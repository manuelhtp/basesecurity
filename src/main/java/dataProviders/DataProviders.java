package dataProviders;

import com.google.gson.Gson;
import entities.*;
import org.testng.annotations.DataProvider;
import utils.FileDataProvider;

public class DataProviders {

    @DataProvider
    public static Object[][] monoapoderado() {
        Gson gson = new Gson();
        MonoApoderado monoApoderado = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\monoapoderado.json")),
                MonoApoderado.class);
        return new Object[][] {{monoApoderado}};
    }

    @DataProvider
    public static Object[][] controller() {
        Gson gson = new Gson();
        Controller controller = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\controller.json")),
                Controller.class);
        return new Object[][] {{controller}};
    }

    @DataProvider
    public static Object[][] apoderadoUno() {
        Gson gson = new Gson();
        ApoderadoUno apoderadoUno = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\apoderadoUno.json")),
                ApoderadoUno.class);
        return new Object[][] {{apoderadoUno}};
    }

    @DataProvider
    public static Object[][] apoderadoDos() {
        Gson gson = new Gson();
        ApoderadoDos apoderadoDos = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/apoderadoDos.json")),
                ApoderadoDos.class);
        return new Object[][] {{apoderadoDos}};
    }

    @DataProvider
    public static Object[][] fullDatos() {
        Gson gson = new Gson();
        Controller controller = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\controller.json")),
                Controller.class);
        ApoderadoUno apoderadoUno = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\apoderadoUno.json")),
                ApoderadoUno.class);
        ApoderadoDos apoderadoDos = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\apoderadoDos.json")),
                ApoderadoDos.class);
       DatosTransNP datosTransNP = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\datosTransferenciaNP.json")),
               DatosTransNP.class);
        return new Object[][] {{controller,apoderadoUno,apoderadoDos,datosTransNP}};
    }

    @DataProvider
    public static Object[][] monoApoderoNP() {
        Gson gson = new Gson();
        MonoApoderado monoApoderado = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\monoapoderado.json")),
                MonoApoderado.class);
               DatosTransNP datosTransNP = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\datosTransferenciaNP.json")),
                DatosTransNP.class);
        return new Object[][] {{monoApoderado,datosTransNP}};
    }

    @DataProvider
    public static Object[][] monoApoderoP() {
        Gson gson = new Gson();
        MonoApoderado monoApoderado = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/monoapoderado.json")),
                MonoApoderado.class);
        DatosTransP datosTransP = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/datosTransferenciaP.json")),
                DatosTransP.class);
        return new Object[][] {{monoApoderado,datosTransP}};
    }

    @DataProvider
    public static Object[][] controllerNP() {
        Gson gson = new Gson();
        Controller controller = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/controller.json")),
                Controller.class);
        DatosTransNP datosTransNP = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/datosTransferenciaNP.json")),
                DatosTransNP.class);
        return new Object[][] {{controller,datosTransNP}};
    }

    @DataProvider
    public static Object[][] controllerP() {
        Gson gson = new Gson();
        Controller controller = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/controller.json")),
                Controller.class);
        DatosTransP datosTransP = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/datosTransferenciaP.json")),
                DatosTransP.class);
        return new Object[][] {{controller,datosTransP}};
    }

    @DataProvider
    public static Object[][] apoderadoUnoP() {
        Gson gson = new Gson();
        ApoderadoUno apoderadoUno = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\apoderadoUno.json")),
                ApoderadoUno.class);
        DatosTransP datosTransP = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\datosTransferenciaP.json")),
                DatosTransP.class);
        return new Object[][] {{apoderadoUno, datosTransP}};
    }

    @DataProvider
    public static Object[][] apoderadoUnoNP() {
        Gson gson = new Gson();
        ApoderadoUno apoderadoUno = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\apoderadoUno.json")),
                ApoderadoUno.class);
        DatosTransNP datosTransNP = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\datosTransferenciaNP.json")),
                DatosTransNP.class);
        return new Object[][] {{apoderadoUno, datosTransNP}};
    }

    @DataProvider
    public static Object[][] apoderadoDosP() {
        Gson gson = new Gson();
        ApoderadoDos apoderadoDos = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\apoderadoDos.json")),
                ApoderadoDos.class);
        DatosTransP datosTransP = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\datosTransferenciaP.json")),
                DatosTransP.class);
        return new Object[][] {{apoderadoDos, datosTransP}};
    }

    @DataProvider
    public static Object[][] apoderadoDosNP() {
        Gson gson = new Gson();
        ApoderadoDos apoderadoDos = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\apoderadoDos.json")),
                ApoderadoDos.class);
        DatosTransNP datosTransNP = gson.fromJson(
                FileDataProvider.asString(String.format("src\\main\\resources\\datosTransferenciaNP.json")),
                DatosTransNP.class);
        return new Object[][] {{apoderadoDos, datosTransNP}};
    }

    @DataProvider
    public static Object[][] monoApoderadoContacto() {
        Gson gson = new Gson();
        MonoApoderado monoApoderado = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/monoapoderado.json")),
                MonoApoderado.class);
        DatosContacto datosContacto = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/datosContacto.json")),
                DatosContacto.class);
        return new Object[][] {{monoApoderado,datosContacto}};
    }

    @DataProvider
    public static Object[][] apoderadoUnoContacto() {
        Gson gson = new Gson();
        ApoderadoUno apoderadoUno = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/apoderadoUno.json")),
                ApoderadoUno.class);
        DatosContacto datosContacto = gson.fromJson(
                FileDataProvider.asString(String.format("src/main/resources/datosContacto.json")),
                DatosContacto.class);
        return new Object[][] {{apoderadoUno,datosContacto}};
    }

}
