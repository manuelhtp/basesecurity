package entities;

public class DatosTransNP {
    private String nombre;
    private String rut;
    private String cuenta;
    private String banco;
    private String nombreMismoBanco;
    private String rutMismoBanco;
    private String cuentaMismoBanco;
    private String bancoMismoBanco;
    private String nombreLT;
    private String rutLT;
    private String cuentaLT;
    private String bancoLT;
    private String correoLT;
    private String comentarioLT;
    private String cuentaAM;
    private String bancoAM;
    private String nombreAM;
    private String rutDestAM;
    private String emailAM;
    private String referenciaAM;
    private String nombreLT2;
    private String rutLT2;
    private String cuentaLT2;
    private String bancoLT2;
    private String correoLT2;
    private String comentarioLT2;

    public String getNombre() {
        return nombre;
    }

    public String getRut() {
        return rut;
    }

    public String getCuenta() {
        return cuenta;
    }

    public String getBanco() {
        return banco;
    }

    public String getNombreMismoBanco() {
        return nombreMismoBanco;
    }

    public String getRutMismoBanco() {
        return rutMismoBanco;
    }

    public String getCuentaMismoBanco() {
        return cuentaMismoBanco;
    }

    public String getBancoMismoBanco() {
        return bancoMismoBanco;
    }

    public String getNombreLT() { return nombreLT; }

    public String getRutLT() { return rutLT; }

    public String getCuentaLT() { return cuentaLT; }

    public String getBancoLT() { return bancoLT; }

    public String getCorreoLT() { return correoLT; }

    public String getComentarioLT() { return comentarioLT; }

    public String getCuentaAM() { return cuentaAM; }

    public String getBancoAM() { return bancoAM; }

    public String getNombreAM() { return nombreAM; }

    public String getRutDestAM() { return rutDestAM; }

    public String getEmailAM() { return emailAM; }

    public String getReferenciaAM() { return referenciaAM; }

    public String getNombreLT2() { return nombreLT2; }

    public String getRutLT2() { return rutLT2; }

    public String getCuentaLT2() { return cuentaLT2; }

    public String getBancoLT2() { return bancoLT2; }

    public String getCorreoLT2() { return correoLT2; }

    public String getComentarioLT2() { return comentarioLT2; }


}
