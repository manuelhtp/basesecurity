package entities;

public class Controller {
    private String nombre;
    private String rut;
    private String pass;
    private String empresa;
    private String usuario;

    public String getNombre() {
        return nombre;
    }

    public String getRut() {
        return rut;
    }

    public String getPass() {
        return pass;
    }

    public String getEmpresa(){
        return empresa;
    }

    public String getUsuario(){ return usuario;}
}
