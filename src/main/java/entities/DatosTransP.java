package entities;

public class DatosTransP {
    private String nombre;
    private String rut;
    private String cuenta;
    private String banco;
    private String tipo;
    private String cuentaMismoBanco;
    private String correo;
    private String tipoCuenta;
    private String cuentaPreestablecidaOtroB;

    public String getNombre() {
        return nombre;
    }

    public String getRut() {
        return rut;
    }

    public String getCuenta() {
        return cuenta;
    }

    public String getBanco() {
        return banco;
    }

    public String getTipo() {
        return tipo;
    }

    public String getCuentaMismoBanco(){
        return cuentaMismoBanco;
    }

    public String getCorreo(){ return correo; }

    public String getTipoCuenta(){ return tipoCuenta; }

    public String getCuentaPreestablecidaOtroB(){ return cuentaPreestablecidaOtroB; }


}

