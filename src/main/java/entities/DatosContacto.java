package entities;

public class DatosContacto {
    private String calle;
    private String numero;
    private String oficinaN;
    private String region;
    private String ciudad;
    private String comuna;
    private String telefono1;
    private String telefono2;

    public String getCalle() { return calle; }
    public String getNumero() { return numero; }
    public String getOficinaN() { return oficinaN; }
    public String getRegion() { return region; }
    public String getCiudad() { return ciudad; }
    public String getComuna() { return comuna; }
    public String getTelefono1() { return telefono1; }
    public String getTelefono2() { return telefono2; }
}
