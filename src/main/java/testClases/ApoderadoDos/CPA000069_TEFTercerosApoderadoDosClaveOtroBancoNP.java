package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000069_TEFTercerosApoderadoDosClaveOtroBancoNP extends BaseConfig {
    @Test(description ="TEF en línea a terceros, Apoderado 2, tarjeta clave dinámica, CPL, Saldo Cta. Cte.," +
            " Otro banco, Cta. NO Pre establecidas",
            dataProvider = "apoderadoDosNP",dataProviderClass = DataProviders.class )
    public void LineaTerceroApoderadoDosNP(ApoderadoDos apoderadoDos , DatosTransNP datosTransNP)  {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(),apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBanco(datosTransNP.getCuenta(), "np");
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoDos(), "Transaccion pendiente aprobada");
    }
}
