package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;
import utils.MetodosGenericos;


public class CPA000111_PagoProveedorNominaApoderadoDosCtaTxt extends BaseConfig {
    @Test(description = " Pago proveedores, Firmar nómina, Apoderado 2, Pago a Cta. Cte.; Cta. Vista, \" +\n" +
            "            \"CPL, FEA, Saldo Cta, Cte. Nómina estándar (TXT)",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void PagoProveedorApoderadoCrearNominaCtaCteTxt(ApoderadoDos apoderadoDos)  {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        MetodosGenericos.waitFor(3);
        opPendientes.clickPagosMasivos();
        opPendientes.pagarNominaPendienteApoderadoDos("cuentaCorriente", "PAGO DE PROVEEDORES");
        Assert.assertTrue(opPendientes.validaPagoMasivoApoderadoDos(),"Transaccion OK");
    }
}
