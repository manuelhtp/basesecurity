package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;
import utils.MetodosGenericos;

import java.awt.*;

public class CPA000075_TEFMasivasApoderadoDosNominaXls extends BaseConfig {
    @Test(description = "TEF masivas, Apoderado 2, FEA, CPL, Saldo Cta. Cte., nómina Excel (XLS)",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void TefApoderadoDosNominaXls(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        MetodosGenericos.waitFor(3);
        opPendientes.clickTrasPendientesMasivas();
        opPendientes.pagarTransMasivaNomina();
        opPendientes.autorizarNominaMasiva();
        Assert.assertTrue(opPendientes.validarTEFMasivasApoderadoDos(), "TEF Masiva excel OK");
    }
}
