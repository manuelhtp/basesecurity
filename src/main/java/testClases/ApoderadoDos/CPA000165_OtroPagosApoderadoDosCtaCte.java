package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;
import utils.MetodosGenericos;

public class CPA000165_OtroPagosApoderadoDosCtaCte extends BaseConfig {
    @Test(description = ".Otros pagos, Firma nómina, Apoderado 2, Pago a Cta. Cte.; " +
            "Cta. Vista, CPL, FEA, Saldo Cta, Cte. Nómina estándar (TXT)",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void PagoProveedorApoderadoCrearNominaCtaCteTxt(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        MetodosGenericos.waitFor(3);
        opPendientes.clickPagosMasivos();
        opPendientes.pagarNominaPendienteApoderadoDos("cuentas", "PAGO DE SUBSIDIOS");
        Assert.assertTrue(opPendientes.validaPagoMasivoApoderadoDos(),"Pago ok");
    }
}
