package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000066_TEFTercerosApoderadoDosClaveOtroBancoP extends BaseConfig {
    @Test(description ="TEF en línea a terceros, Apoderado 2, Tarjeta clave dinámica, CPL, Saldo Cta. Cte., " +
            "Otro banco, Cta. Pre establecidas",
            dataProvider = "apoderadoDosP",dataProviderClass = DataProviders.class )
    public void LineaTerceroApoderadoDosOtroBancoP(ApoderadoDos apoderadoDos, DatosTransP datosTransP)  {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(),apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBanco(datosTransP.getCuenta(), "p");
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoDos(), "Pendiente de Aprobacion no desplegado");
    }
}
