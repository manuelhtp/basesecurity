package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000060_TEFTercerosApoderadoDosClaveMismoBancoP extends BaseConfig {
    @Test(description ="TEF en línea a terceros, Apoderado 2, Tarjeta Clave Dinámica, CPL, Saldo Cta. Cte.," +
            " Mismo banco, Cta. Pre establecidas",
            dataProvider = "apoderadoDosP",dataProviderClass = DataProviders.class )
    public void LineaTerceroApoderadoUnoP(ApoderadoDos apoderadoDos, DatosTransP datosTransP)  {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(),apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBanco(datosTransP.getCuentaPreestablecidaOtroB(), "p");
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoUno(), "Pendiente de Aprobacion no desplegado");
    }


}
