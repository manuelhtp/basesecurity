package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

import java.awt.*;

public class CPA000093__TEFCCLVApoderadoDosPM extends BaseConfig {
    @Test(description = "TEF transferir a CCLV, PM, Apoderado 2, CPL, FEA, Saldo Cta.",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void TefApoderadoUnoNominaTxt(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesCCLV("PM");
        Assert.assertTrue(opPendientes.validaPendientePagoNominaApoderado2(),"Transferencia CCVL-PM pago OK");

    }
}
