package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000123_PagoProveedoresApoderadoDosServipag extends BaseConfig {
    @Test(description = " Pago proveedores, Firmar nómina, Apoderado 2, Pago a Cta. Cte.; Cta. Vista, CPL," +
            " FEA, Saldo Cta, Cte. Nómina EXCEL con detalle (XLS)",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void PagoProveedorApoderadoCrearNominaCtaCteTxt(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickPagosMasivos();
        opPendientes.pagarNominaPendienteApoderadoDos("servipag", "PAGO DE PROVEEDORES");
        Assert.assertTrue(opPendientes.validaPagoMasivoApoderadoDos(),"Transaccion OK");
    }
}
