package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

import java.awt.*;

public class CPA000078_TEFAltosMontosApoderadoDosOtroBancoP extends BaseConfig {
    @Test(description = "TEF altos montos, Apoderado 1, FEA, CPL, Saldo Cta. Cte., Otro banco, Cta. Preestablecida",
            dataProvider = "apoderadoDosNP", dataProviderClass = DataProviders.class)
    public void TefApoderadoUnoNominaTxt(ApoderadoDos apoderadoDos , DatosTransNP datosTransNP)  {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBancoAltosMontos("01272322", "p");
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoDos(), "Transferencia Altos Montos no que queda en estado Transaccion realizada,Pendiente de pago");
    }
}
