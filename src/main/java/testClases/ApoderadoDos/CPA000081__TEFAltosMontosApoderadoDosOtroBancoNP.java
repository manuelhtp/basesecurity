package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;
import utils.MetodosGenericos;

import java.awt.*;

public class CPA000081__TEFAltosMontosApoderadoDosOtroBancoNP extends BaseConfig {
    @Test(description = "TEF altos montos, Apoderado 2, FEA, CPL, Saldo Cta. Cte., Otro banco, Cta. NO Preestablecida",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void TefApoderadoUnoNominaTxt(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        MetodosGenericos.waitFor(3);
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBancoAltosMontos("12123123", "np");
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoDos(), "Transferencia Altos Montos no que queda en estado Transaccion realizada,Pendiente de pago");
    }
}
