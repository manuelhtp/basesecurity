package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

import java.awt.*;

public class CPA000072_TEFMasivasApoderadoDosTxt extends BaseConfig {
    @Test(description = "TEF masivas, Apoderado 2, FEA, CPL, Saldo Cta. Cte., nómina estándar (TXT)",
            dataProvider = "apoderadoDosNP", dataProviderClass = DataProviders.class)
    public void TefApoderadoDosNominaTxt(ApoderadoDos apoderadoDos, DatosTransNP datosTransNP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientesMasivas();
        opPendientes.pagarTransMasivaNomina();
        opPendientes.autorizarNominaMasiva();
        Assert.assertTrue(opPendientes.validarTEFMasivasApoderadoDos(), "TEF Masiva txt OK");
    }
}
