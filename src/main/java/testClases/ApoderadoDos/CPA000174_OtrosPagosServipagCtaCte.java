package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000174_OtrosPagosServipagCtaCte extends BaseConfig {
    @Test(description = "Otros Pagos, Firmar nómina, Apoderado 2, Pago Servipag, CPL, FEA, Saldo Cta, Cte. Nómina estándar (TXT)",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void OtrosPagodServipag(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickPagosMasivos();
        opPendientes.pagarNominaPendienteApoderadoDos("servipag", "PAGO DE SUBSIDIOS");
        Assert.assertTrue(opPendientes.validaPagoMasivoApoderadoDos(),"nomina otros pagos ok");
    }
}
