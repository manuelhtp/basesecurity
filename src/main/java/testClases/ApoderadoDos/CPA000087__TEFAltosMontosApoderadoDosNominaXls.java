package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

import java.awt.*;

public class CPA000087__TEFAltosMontosApoderadoDosNominaXls extends BaseConfig {
    @Test(description = "TEF altos montos, Apoderado 2, FEA, CPL, Saldo Cta. Cte., nómina EXCEL (XLS)",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void TefApoderadoUnoNominaXls(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientesAltosMontosNomina();
        opPendientes.pagarTransPendientesAltosMontosNomina();
        Assert.assertTrue(opPendientes.validaTEFAltosMontosNominasApoderadoDos(), "Transferencia altos montos nomina xls pagada");
    }
}
