package testClases.ApoderadoDos;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;
import utils.MetodosGenericos;

public class CPA000150_PagoRemuneracionesApoderadoDosServipag extends BaseConfig {
    @Test(description = "Pago remuneraciones, Firmar nómina, Apoderado 2, Pago Servipag," +
            " Nómina estándar (TXT)",
            dataProvider = "apoderadoDosNP", dataProviderClass = DataProviders.class)
    public void PagoProveedorApoderadoCrearNominaCtaCteTxt(ApoderadoDos apoderadoDos , DatosTransNP datosTransNP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        MetodosGenericos.waitFor(3);
        opPendientes.clickPagosMasivos();
        opPendientes.pagarNominaPendienteApoderadoDos("servipag", "PAGO DE SUELDOS");
        Assert.assertTrue(opPendientes.validaPagoMasivoApoderadoDos(),"Transaccion OK");
    }
}
