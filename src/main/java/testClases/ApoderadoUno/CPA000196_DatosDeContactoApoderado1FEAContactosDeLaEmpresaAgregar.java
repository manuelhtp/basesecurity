package testClases.ApoderadoUno;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.EditarDatos;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000196_DatosDeContactoApoderado1FEAContactosDeLaEmpresaAgregar extends BaseConfig {
    @Test(description = "Datos de Contacto Apoderado 1: FEA Contactos de la Empresa Agregar",
            dataProvider = "apoderadoUno", dataProviderClass = DataProviders.class, enabled = false)
    public void DatosDeContactoApoderado1FEAContactosDeLaEmpresaAgregar(ApoderadoUno apoderadoUno) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(), apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        header.editarContacto();
        EditarDatos editarDatos = new EditarDatos();
        editarDatos.clickEditContacto();
        editarDatos.agregarEmpresa();
        editarDatos.agregarDatosEmpresa("225674893", "96854396", "prueba@correo.cl");
        String nombreUno = editarDatos.capturaNombre();
        editarDatos.clickGuardarEmpresa();
        Assert.assertTrue(editarDatos.capturaNombreDos(nombreUno), "Emprensa nueva no agregada");


    }
}
