package testClases.ApoderadoUno;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import org.testng.annotations.Test;
import page.EditarDatos;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000198_DatosDeContactoApoderado1FEAContactosDeLaEmpresaBorrar extends BaseConfig {
    @Test(description = "Datos de Contacto Apoderado 1: FEA Contactos de la Empresa Borrar",
            dataProvider = "apoderadoUno", dataProviderClass = DataProviders.class, enabled = false)
    public void DatosDeContactoApoderado1FEAContactosDeLaEmpresaBorrar(ApoderadoUno apoderadoUno) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(),apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        header.editarContacto();
        EditarDatos editarDatos = new EditarDatos();
        editarDatos.clickEditContacto();
        editarDatos.borrarEmpresa();
        editarDatos.borrarContactoEmpresa();
    }
}
