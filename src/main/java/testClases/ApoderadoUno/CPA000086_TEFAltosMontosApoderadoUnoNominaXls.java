package testClases.ApoderadoUno;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

import java.awt.*;

public class CPA000086_TEFAltosMontosApoderadoUnoNominaXls extends BaseConfig {
    @Test(description = "TEF altos montos, Apoderado 1, FEA, CPL, Saldo Cta. Cte., nómina EXCEL (XLS)",
            dataProvider = "apoderadoUnoNP", dataProviderClass = DataProviders.class)
    public void TefApoderadoUnoNominaTxt(ApoderadoUno apoderadoUno, DatosTransNP datosTransNP) throws AWTException {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(), apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientesAltosMontosNomina();
        opPendientes.pagarTransPendientesAltosMontosNomina();
        Assert.assertTrue(opPendientes.validaAltosMontosApoderadoUno(), "Transferencia Altos Montos Nomina" +
                " no que queda en estado Transaccion realizada,Pendiente de pago");
    }
}
