package testClases.ApoderadoUno;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.EditarDatos;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000197_DatosDeContactoApoderado1FEAContactosDeLaEmpresaModificar extends BaseConfig {
    @Test(description = "Datos de Contacto Apoderado 1: FEA Contactos de la Empresa Modificar",
            dataProvider = "apoderadoUno", dataProviderClass = DataProviders.class, enabled = false)
    public void DatosDeContactoApoderado1FEAContactosDeLaEmpresaModificar(ApoderadoUno apoderadoUno) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(),apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        header.editarContacto();
        EditarDatos editarDatos = new EditarDatos();
        editarDatos.clickEditContacto();
        editarDatos.editarEmpresa();
        editarDatos.modificarDatosEmpresa( "229524385", "938475920", "test@correo.cl");
        String nombreUno = editarDatos.capturaNombre();
        editarDatos.clickModificarEmpresa();
        Assert.assertTrue(editarDatos.capturaNombreDos(nombreUno), "Emprensa nueva no agregada");
    }
}
