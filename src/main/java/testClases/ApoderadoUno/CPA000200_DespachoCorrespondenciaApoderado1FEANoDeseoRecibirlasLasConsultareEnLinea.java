package testClases.ApoderadoUno;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.EditarDatos;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000200_DespachoCorrespondenciaApoderado1FEANoDeseoRecibirlasLasConsultareEnLinea extends BaseConfig {
    @Test(description = "Despacho Correspondencia Apoderado 1: FEA Deseo Recibirlas Las Consultare en Linea",
            dataProvider = "apoderadoUno", dataProviderClass = DataProviders.class)
    public void DespachoCorrespondenciaApoderado1FEANoDeseoRecibirlasLasConsultareEnLinea(ApoderadoUno apoderadoUno) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(),apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        header.editarContacto();
        EditarDatos editarDatos = new EditarDatos();
        editarDatos.clickEditCorrespondecia();
        editarDatos.cambiarFormaDespacho();
        editarDatos.despachoCorrespondencia("consultar en linea");
        editarDatos.modificarDespachoCorrespondencia();
        Assert.assertTrue(editarDatos.validaModificarDespacho(), "Modificar despacho de correspondencia no realizado");
    }
}
