package testClases.ApoderadoUno;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.EditarDatos;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000201_DespachoCorrespondenciaApoderado1FEADeseoRecibirlasPorCorreoFisicoRemitente1 extends BaseConfig {
    @Test(description = "Despacho Correspondencia Apoderado 1: FEA Deseo Recibirlas por Correo Fisico Remitente 1",
            dataProvider = "apoderadoUno", dataProviderClass = DataProviders.class)
    public void DespachoCorrespondenciaApoderado1FEADeseoRecibirlasPorCorreoFisicoRemitente1(ApoderadoUno apoderadoUno) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(),apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        header.editarContacto();
        EditarDatos editarDatos = new EditarDatos();
        editarDatos.clickEditCorrespondecia();
        editarDatos.cambiarFormaDespacho();
        editarDatos.despachoCorrespondencia("recibirlas correo fisico");
        editarDatos.modificarDespachoCorrespondencia();
        Assert.assertTrue(editarDatos.validaModificarDespacho(), "Modificar despacho de correspondencia no realizado");
    }
}
