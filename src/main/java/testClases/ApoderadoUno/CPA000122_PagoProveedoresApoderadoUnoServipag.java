package testClases.ApoderadoUno;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000122_PagoProveedoresApoderadoUnoServipag extends BaseConfig {
    @Test(description = " Pago proveedores, Firmar nómina, Apoderado 1, Pago a Cta. Cte.; Cta. Vista, CPL," +
            " FEA, Saldo Cta, Cte. Nómina EXCEL con detalle (XLS)",
            dataProvider = "apoderadoUnoNP", dataProviderClass = DataProviders.class)
    public void PagoProveedorApoderadoCrearNominaCtaCteTxt(ApoderadoUno apoderadoUno, DatosTransNP datosTransNP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(), apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickPagosMasivos();
        opPendientes.pagarNominaPendinte("servipag", "PAGO DE PROVEEDORES");
        Assert.assertTrue(opPendientes.validaPagoMasivoApoderadoUno(),"Transaccion no queda en Nómina Pendiente de Pago o Falta de firma");
    }
}
