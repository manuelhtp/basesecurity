package testClases.ApoderadoUno;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000003_LoginApoderado extends BaseConfig {
    @Test(description ="Validate logIn and LogOut site: Empresas Apoderado",
            dataProvider = "apoderadoUno",dataProviderClass = DataProviders.class )
    public void LoginLogOut(ApoderadoUno apoderadoUno)  {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(),apoderadoUno.getPass());
        Assert.assertTrue(empresas.validaIngreso(), "LogIn private site: empresa OK ");
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        Assert.assertTrue(empresas.validaIngresoComo(), "Access like User OK");
        empresas.logOutEmpresa();
        Assert.assertTrue(header.validaSalir(),"LogOut private site: empresa OK ");
    }
}

