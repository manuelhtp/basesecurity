package testClases.ApoderadoUno;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.DatosContacto;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.EditarDatos;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000195_DatosContactoDireccionTelefono extends BaseConfig {
    @Test(description = "Datos de contacto, Apoderado 1,FEA, Dirección comercial, Teléfonos",
            dataProvider = "apoderadoUnoContacto", dataProviderClass = DataProviders.class, enabled = false)
    public void PagoProveedorApoderadoCrearNominaCtaCteTxt(ApoderadoUno apoderadoUno, DatosContacto datosContacto) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(), apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        EditarDatos editarDatos = new EditarDatos();
        editarDatos.clickEditarMisDatos();
        editarDatos.clickEditContacto();
        editarDatos.modificarDatosContactoFEA(datosContacto.getCalle(), datosContacto.getNumero(), datosContacto.getOficinaN(),
                datosContacto.getRegion(), datosContacto.getCiudad(), datosContacto.getComuna(), datosContacto.getTelefono1(),
                datosContacto.getTelefono2());
        Assert.assertTrue(editarDatos.validarModificarDatos(), "");

    }
}
