package testClases.ApoderadoUno;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000059_TEFTercerosApoderadoUnoMismoBancoP extends BaseConfig {
    @Test(description ="TEF en línea a terceros, Apoderado 1, FEA, CPL, Saldo Cta. Cte., Mismo banco, Cta. Pre establecidas",
            dataProvider = "apoderadoUnoP",dataProviderClass = DataProviders.class )
    public void LineaTerceroApoderadoUnoP(ApoderadoUno apoderadoUno, DatosTransP datosTransP)  {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(),apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBanco(datosTransP.getCuentaPreestablecidaOtroB(), "p");
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoUno(),
                "TEF no que queda en estado Transaccion realizada,Pendiente de pago");
    }
}
