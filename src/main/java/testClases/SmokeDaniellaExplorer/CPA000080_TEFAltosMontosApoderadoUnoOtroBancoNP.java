package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

import java.awt.*;

public class CPA000080_TEFAltosMontosApoderadoUnoOtroBancoNP extends BaseConfig {
    @Test(description = "TEF altos montos, Apoderado 1, FEA, CPL, Saldo Cta. Cte., Otro banco, Cta. NO Preestablecida",
            dataProvider = "apoderadoUnoNP", dataProviderClass = DataProviders.class)
    public void TefApoderadoUnoNominaTxt(ApoderadoUno apoderadoUno, DatosTransNP datosTransNP) throws AWTException {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(), apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBancoAltosMontos("12123123", "np");
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoUno(), "Transferencia Altos Montos no que queda en estado Transaccion realizada,Pendiente de pago");
    }
}
