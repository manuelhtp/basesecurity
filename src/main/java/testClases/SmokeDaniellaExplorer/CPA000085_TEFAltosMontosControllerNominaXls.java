package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

import java.awt.*;

public class CPA000085_TEFAltosMontosControllerNominaXls extends BaseConfig {
    @Test(description = "TEF altos montos, Controller, FEA, CPL, Saldo Cta. Cte., nómina EXCEL (XLS)",
            dataProvider = "controllerNP", dataProviderClass = DataProviders.class)
    public void controllerMasivaTxtXls(Controller controller, DatosTransNP datosTransNP) throws AWTException {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferirAltosMontos();
        transferencias.transferenciaAltosMontosMasivaExcelTxt("xlsController");
        transferencias.clickEnviarAprobacion();
        Assert.assertTrue(transferencias.validaAltosMontosController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
