package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000154_PagoRemuneracionesNominaControllerServipagManual extends BaseConfig {
    @Test(description = "Pago remuneraciones, Crear nueva nómina, Controller, Pago a Servipag, " +
            "CPL, FEA, Saldo Cta, Cte. Ingreso manual",
            dataProvider = "controllerP", dataProviderClass = DataProviders.class)
    public void controllerRemuneracionesServipagManual(Controller controller, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNominaPagoRemuneraciones();
        pagos.creaNominaRemunearciones("PAGO DE SUELDOS", "servipag", "manual", "xls");
        pagos.clickAgregarRegitroNomina();
        pagos.generarRegistroNominaOtrosPagosServipagManual(datosTransP.getRut(), datosTransP.getNombre());
        pagos.validacionNominaCreadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
