package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000074_TEFMasivasApoderadoUnoCtaCteNominaXls extends BaseConfig {
    @Test(description = "TEF masivas, Apoderado 1, FEA, CPL, Saldo Cta. Cte., nómina Excel (XLS)",
            dataProvider = "apoderadoUnoNP", dataProviderClass = DataProviders.class)
    public void TefApoderadoUnoNominaXls(ApoderadoUno apoderadoUno, DatosTransNP datosTransNP) throws FindFailed {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(), apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientesMasivas();
        opPendientes.pagarTransMasivaNomina();
        opPendientes.autorizarNominaMasiva();
        Assert.assertTrue(opPendientes.validaPagoMasivoApoderadoUno(), "");
    }
}
