package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000068_TEFTercerosApoderadoUnoOtroBancoNP extends BaseConfig {
    @Test(description ="TEF en línea a terceros, Apoderado 1, FEA, CPL, Saldo Cta. Cte., Otro banco, Cta. NO Pre establecidas",
            dataProvider = "apoderadoUnoNP",dataProviderClass = DataProviders.class )
    public void LineaTerceroApoderadoUnoNP(ApoderadoUno apoderadoUno, DatosTransNP datosTransNP)  {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(),apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBanco(datosTransNP.getCuenta(), "np");
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoUno(), "TEF no que queda en estado Transaccion realizada,Pendiente de pago");
    }
}
