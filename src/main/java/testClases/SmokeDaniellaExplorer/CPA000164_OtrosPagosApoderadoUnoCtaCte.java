package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000164_OtrosPagosApoderadoUnoCtaCte extends BaseConfig {
    @Test(description = ".Otros pagos, Firma nómina, Apoderado 1, Pago a Cta. Cte.; " +
            "Cta. Vista, CPL, FEA, Saldo Cta, Cte. Nómina estándar (TXT)",
            dataProvider = "apoderadoUnoNP", dataProviderClass = DataProviders.class)
    public void PagoProveedorApoderadoCrearNominaCtaCteTxt(ApoderadoUno apoderadoUno, DatosTransNP datosTransNP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(), apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickPagosMasivos();
        opPendientes.pagarNominaPendinte("cuentas", "PAGO DE SUBSIDIOS");
        Assert.assertTrue(opPendientes.validaPagoMasivoApoderadoUno(),"Transaccion no queda en Nómina Pendiente de Pago o Falta de firma");
    }
}