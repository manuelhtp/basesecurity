package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

import java.awt.*;

public class CPA000088_TEFCclvPhsmController extends BaseConfig {
    @Test(description = "TEF transferir a CCLV, PHSM, Controller, CPL, FEA, Saldo Cta. Cte.",
            dataProvider = "controllerNP", dataProviderClass = DataProviders.class)
    public void controllerMasivaTxtXls(Controller controller, DatosTransNP datosTransNP) throws AWTException {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferirCCLV();
        transferencias.ingresaDatosTransfCCLV("PH-SM", "controller");
        Assert.assertTrue(transferencias.validaAltosMontosController() , "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}