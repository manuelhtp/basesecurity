package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000076_TEFAltosMontosControllerOtroBancoP extends BaseConfig {
    @Test(description = "TEF altos montos, Controller, FEA,CPL, Saldo Cta. Cte., Otro banco, Cta. Preestablecida",
            dataProvider = "controllerP", dataProviderClass = DataProviders.class)
    public void controllerMasivaTxt(Controller controller, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferirAltosMontos();
        transferencias.generarTransferenciaAltosMontosPreestablecidas(controller.getUsuario());
        Assert.assertTrue(transferencias.validaAltosMontosController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
