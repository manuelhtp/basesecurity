package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

import java.awt.*;

public class CPA000092_TEFCCLVApoderadoUnoPM extends BaseConfig {
    @Test(description = "TEF transferir a CCLV, PM, Apoderado 1, CPL, FEA, Saldo Cta.",
            dataProvider = "apoderadoUnoNP", dataProviderClass = DataProviders.class)
    public void TefApoderadoUnoCCVLPM(ApoderadoUno apoderadoUno, DatosTransNP datosTransNP) throws AWTException {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(), apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesCCLV("PM");
        Assert.assertTrue(opPendientes.validaPendientePagoNomina(),"Transferencia CCVL-PM no realizada, pendiente de pago no OK");
    }
}
