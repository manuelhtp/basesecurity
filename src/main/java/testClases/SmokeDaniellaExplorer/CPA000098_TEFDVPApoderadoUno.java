package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000098_TEFDVPApoderadoUno extends BaseConfig {
    @Test(description = "TEF transferencias con DVP, Apoderado Uno, CPL, FEA, Saldo Cta. Cte.", dataProvider = "apoderadoUno", dataProviderClass = DataProviders.class)
    public void TEFDVPController(ApoderadoUno apoderadoUno) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoUno.getRut(), apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.entrarTransPendientesDVP();
        transferencias.pagarTransPendienteDVP(apoderadoUno.getUsuario());
        transferencias.pagarDVPConFEA();
        Assert.assertTrue(transferencias.validaTransDVPApoderado(), "Transferencias aprobadas y pendientes de pago! No realizada");
    }
}
