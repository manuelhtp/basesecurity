package testClases.SmokeDaniellaExplorer;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransNP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000079_TEFAltosMontosControllerOtroBancoNP extends BaseConfig {
    @Test(description = "TEF altos montos, Controller, FEA, CPL, Saldo Cta. Cte., Otro banco, Cta. NO Preestablecida",
         dataProvider = "controllerNP", dataProviderClass =DataProviders.class)
    public void controllerMasivaTxt(Controller controller, DatosTransNP datosTransNP){
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(),controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferirAltosMontos();
        transferencias.generarTransferenciaAltosMontosNoPreestablecidas("controller",datosTransNP.getCuenta(),
                datosTransNP.getBanco(),datosTransNP.getNombre(),datosTransNP.getRut(), "prueba@correo.com", "pago");
        Assert.assertTrue(transferencias.validaAltosMontosController(), "No queda en Pendiente de Pago o Falta de Firma.");
    }
}
