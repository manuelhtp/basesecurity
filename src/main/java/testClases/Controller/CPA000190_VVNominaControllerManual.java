package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import page.ValeVistas;
import utils.BaseConfig;

public class CPA000190_VVNominaControllerManual extends BaseConfig {
    @Test(description ="Vales vista online, Ingresar nómina, Controller, CLP, FEA, Saldo Cta. Cte. Ingreso manual",
            dataProvider ="controllerP", dataProviderClass = DataProviders.class)
    public void controllerVVXlsCon(Controller controller, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        ValeVistas valeVistas = new ValeVistas();
        valeVistas.clickIngresarNomina();
        valeVistas.clickCrearNomina();
        valeVistas.completaDatosNuevaNomina("manual", "xlsSinD");
        valeVistas.agregarRegistroValeVista(datosTransP.getRut(),datosTransP.getNombre(),datosTransP.getCorreo());
        valeVistas.validacionNominaCreada(controller.getUsuario());
        Assert.assertTrue(valeVistas.validaNominaPendientePagoMonoApoderado(),"Nómina no queda en Pendiente de Pago o Falta de Firma.");

    }

}
