package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000109_PagoProveedorControllerCrearNominaCtaCteTxt extends BaseConfig {
    @Test(description = "Pago proveedores, Crear nueva nómina, Controller, Pago a Cta. Cte.; Cta. Vista,CPL, FEA, Saldo Cta, Cte. " +
            "Nómina EXCEL sin detalle (XLS)",dataProvider = "controller", dataProviderClass = DataProviders.class)
    public void PagoProveedorControllerCrearNominaCtaCteTxt(Controller controller) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(),controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNuevaNominaProveedor();
        pagos.creaNominaRemunearciones("PAGO DE PROVEEDORES","cuentas","archivo","txt");
        pagos.cargarArchivoMasivo("txt");
        pagos.validacionNominaCargadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
