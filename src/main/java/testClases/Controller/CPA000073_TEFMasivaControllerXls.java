package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000073_TEFMasivaControllerXls extends BaseConfig {
    @Test(description = "TEF masivas, Controller, FEA, CPL, Saldo Cta. Cte., nómina Excel (XLS)",
    dataProvider = "controller", dataProviderClass = DataProviders.class)
    public void controllerMasivaExcel(Controller controller) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(),controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());;
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferenciasMasivas();
        transferencias.cargarArchivoMasivo("xls");
        Assert.assertTrue(transferencias.validaNominaMasivaPendientePagoController(),"Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
