package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000169_OtrosPagosNominaControllerCtaManual extends BaseConfig {
    @Test(description = " Otros pagos, Crear nueva nómina, Controller, Pago a Cta. Cte.; " +
            "Cta. Vista, CPL, FEA, Saldo Cta, Cte. Ingreso manual",
            dataProvider = "controllerP", dataProviderClass = DataProviders.class)
    public void controllerOtrosPagosManual(Controller controller, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarCrearNominaOtrosPagos();
        pagos.creaNominaRemunearciones("PAGO DE SUBSIDIOS", "cuentas", "manual", "xls");
        pagos.clickAgregarRegitroNomina();
        pagos.generarRegistroNominaOtrosPagosManual(datosTransP.getRut(), datosTransP.getNombre());
        pagos.validacionNominaCreadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
