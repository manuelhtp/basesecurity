package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000127_PagoProveedorNominaControllerServipafXlsSDetalle extends BaseConfig {
    @Test(description = "Pago proveedores, Crear nueva nómina, Controller, PagoServipag, CPL, FEA, Saldo Cta, " +
            "Cte. Nómina EXCEL sin detalle (XLS)",
            dataProvider = "controllerP", dataProviderClass = DataProviders.class)
    public void controllerMasivaTxt(Controller controller, DatosTransP datosTransP){
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNuevaNominaProveedor();
        pagos.creaNominaRemunearciones("PAGO DE PROVEEDORES","servipag","archivo","Excel sin detalle");
        pagos.cargarArchivoMasivo("xls sin detalle servipag");
        pagos.validacionNominaCargadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}