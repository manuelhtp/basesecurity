package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000139_PagoRemuneracionesNominaControllerCtaCteTxt extends BaseConfig {
    @Test(description = "Pago remuneraciones, Crear nueva nómina, Controller, Pago a Cta. Cte.; Cta.Vista, CPL, FEA, " +
            "Saldo Cta, Cte. Nómina estándar (TXT)",
            dataProvider = "controllerP", dataProviderClass = DataProviders.class)
    public void controllerRemuneracionesTxt(Controller controller, DatosTransP datosTransP) throws AWTException {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNominaPagoRemuneraciones();
        pagos.creaNominaRemunearciones("PAGO DE SUELDOS", "cuentas", "archivo", "txt");
        pagos.cargarArchivoMasivo("txtRemuneraciones");
        pagos.validacionNominaCargadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}