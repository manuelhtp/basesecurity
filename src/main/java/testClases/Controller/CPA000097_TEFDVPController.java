package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000097_TEFDVPController extends BaseConfig {
    @Test(description = "TEF transferencias con DVP, Controller, CPL, FEA, Saldo Cta. Cte.", dataProvider = "controller", dataProviderClass = DataProviders.class)
    public void TEFDVPController(Controller controller) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.entrarTransPendientesDVPController();
        transferencias.pagarTransPendienteDVP(controller.getUsuario());
        Assert.assertTrue(transferencias.validaTransDVPController(), "Transferencias aprobadas y pendientes de pago! No realizada");
    }
}