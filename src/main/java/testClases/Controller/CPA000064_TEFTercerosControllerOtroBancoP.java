package testClases.Controller;


import dataProviders.DataProviders;
import entities.Controller;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000064_TEFTercerosControllerOtroBancoP extends BaseConfig {
    @Test(description = "TEF en línea a terceros, Controller, FEA, CPL, Saldo Cta. Cte., Otro banco, Cta. Pre establecidas",
            dataProvider = "controller", dataProviderClass = DataProviders.class)
    public void TransferenciaAltosMontos(Controller controller) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferirLineaTercerosP(controller.getUsuario());
        Assert.assertTrue(transferencias.validaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
