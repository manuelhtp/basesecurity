package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000178_OtrosPagosNominaControllerServipagManual extends BaseConfig {
    @Test(description = "Otros Pagos, Crear nueva nómina, Controller, Pago Servipag, CPL, FEA, Saldo Cta, Cte. Ingreso manual",
            dataProvider = "controllerP", dataProviderClass = DataProviders.class)
    public void controllerOtrosPagosManual(Controller controller, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarCrearNominaOtrosPagos();
        pagos.creaNominaRemunearciones("PAGO DE SUBSIDIOS", "servipag", "manual", "xls");
        pagos.clickAgregarRegitroNomina();
        pagos.generarRegistroNominaOtrosPagosServipagManual(datosTransP.getRut(), datosTransP.getNombre());
        pagos.validacionNominaCreadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
