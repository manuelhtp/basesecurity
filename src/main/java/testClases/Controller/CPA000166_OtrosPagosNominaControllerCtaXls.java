package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000166_OtrosPagosNominaControllerCtaXls extends BaseConfig {
    @Test(description = " Otros pagos, Crear nueva nómina, Controller, Pago a Cta. Cte.; Cta. Vista, " +
            "CPL, FEA, Saldo Cta, Cte. Nómina EXCEL sin detalle (XLS)",
            dataProvider = "controllerP", dataProviderClass = DataProviders.class)
    public void controllerOtrosPagosXLS(Controller controller, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarCrearNominaOtrosPagos();
        pagos.creaNominaRemunearciones("PAGO DE SUBSIDIOS", "cuentas", "archivo", "xls");
        pagos.cargarArchivoMasivo("xls sin detalle");
        pagos.validacionNominaCargadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}

