package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000119_PagoProveedorControllerCrearNominaCtaCteTxt extends BaseConfig {

    @Test(description = "Pago proveedores; Controller; Crear nueva nómina; Cuenta corriente, cuenta vista," +
            " vale vista: subir un archivo con los datos; Estándar (TXT)", dataProvider = "controller", dataProviderClass = DataProviders.class)
    public void PagoProveedorControllerCrearNominaCtaCteTxt(Controller controller) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(),controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNuevaNominaProveedor();
        pagos.creaNominaRemunearciones("PAGO DE PROVEEDORES","cuentas","archivo","txt");
        pagos.cargarArchivoMasivo("txt");
        pagos.validacionNominaCargadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
