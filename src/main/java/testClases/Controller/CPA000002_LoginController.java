package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000002_LoginController extends BaseConfig {

    @Test(description = "Validate logIn and LogOut site: Empresas Controller",
            dataProvider = "controller", dataProviderClass = DataProviders.class )
    public void LoginLogOut(Controller controller) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(),controller.getPass());
        Assert.assertTrue(empresas.validaIngreso(), "LogIn private site: empresa OK ");
        empresas.clickIngresarComo(controller.getEmpresa());
        Assert.assertTrue(empresas.validaIngresoComo(), "Access like User OK");
        empresas.logOutEmpresa();
        Assert.assertTrue(header.validaSalir(), "LogOut private site: empresa OK ");
    }
}
