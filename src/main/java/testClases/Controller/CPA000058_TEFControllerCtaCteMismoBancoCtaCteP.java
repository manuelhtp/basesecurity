package testClases.Controller;

import dataProviders.DataProviders;
import entities.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000058_TEFControllerCtaCteMismoBancoCtaCteP extends BaseConfig {
    @Test(description = "TEF en línea a terceros, Controller, FEA, CPL, Saldo Cta. Cte., Mismo banco, Cta. Pre establecidas",
            dataProvider = "controller", dataProviderClass = DataProviders.class)
    public void TefControllerMismoBancoP(Controller controller) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(),controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.clickTransferirLineaTerceros();
        transferencias.ingresarTransferirLineaTercerosP(controller.getUsuario());
        Assert.assertTrue(transferencias.validaPendientePagoController(),
                "No queda en Pendiente de Pago o Falta de Firma.");

    }
}
