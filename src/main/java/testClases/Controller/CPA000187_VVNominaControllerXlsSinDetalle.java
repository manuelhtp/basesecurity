package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import page.ValeVistas;
import utils.BaseConfig;

public class CPA000187_VVNominaControllerXlsSinDetalle  extends BaseConfig {
    @Test(description =" Vales vista online, Ingresar nómina, Controller, CLP, FEA, Saldo Cta. Cte. Nómina Excel sin detalle (XLS)",
            dataProvider ="controllerP", dataProviderClass = DataProviders.class)
    public void controllerVVXlsSin(Controller controller, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        ValeVistas valeVistas = new ValeVistas();
        valeVistas.clickIngresarNomina();
        valeVistas.clickCrearNomina();
        valeVistas.completaDatosNuevaNomina("archivo", "xlsSinD");
        valeVistas.cargarArchivoMasivo("xlsSinD");
        valeVistas.validacionNominaCargada(controller.getUsuario(), "vv");
        Assert.assertTrue(valeVistas.validaNominaPendientePagoMonoApoderado(),"Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
