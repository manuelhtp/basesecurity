package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000172_OtrosPagosNominaControllerServipagTxt extends BaseConfig {
    @Test(description = "Otros Pagos, Crear nueva nómina, Controller, Pago a Servipag, " +
            "CPL, FEA, Saldo Cta, Cte. Nómina estándar (TXT)",
            dataProvider = "controllerP", dataProviderClass = DataProviders.class)
    public void controllerOtrosPagosTxt(Controller controller, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarCrearNominaOtrosPagos();
        pagos.creaNominaRemunearciones("PAGO DE SUBSIDIOS", "servipag", "archivo", "txt");
        pagos.cargarArchivoMasivo("txtRemuneracionesServipag");
        pagos.validacionNominaCargadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
