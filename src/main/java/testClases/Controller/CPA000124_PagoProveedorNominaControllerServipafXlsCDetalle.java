package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000124_PagoProveedorNominaControllerServipafXlsCDetalle extends BaseConfig {
    @Test(description = "Pago proveedores, Crear nueva nómina, Controller, PagoServipag, CPL, FEA, Saldo Cta, Cte. " +
            "Nómina EXCEL con detalle (XLS)", dataProvider = "controller", dataProviderClass = DataProviders.class)
    public void PagoProveedorControllerCrearNominaCtaCteXLSCDetalle(Controller controller) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(),controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNuevaNominaProveedor();
        pagos.creaNominaRemunearciones("PAGO DE PROVEEDORES","servipag","archivo","Excel con detalle");
        pagos.cargarArchivoMasivo("xls con detalle servipag");
        pagos.validacionNominaCargadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
