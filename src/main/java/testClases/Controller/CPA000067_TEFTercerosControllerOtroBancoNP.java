package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransNP;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

import java.awt.*;

public class CPA000067_TEFTercerosControllerOtroBancoNP extends BaseConfig {
    @Test(description = "TEF en línea a terceros, Controller, FEA, CPL, Saldo Cta. Cte., Otro banco, Cta. NO Pre establecidas",
    dataProvider = "controllerNP", dataProviderClass = DataProviders.class)
    public void controllerOtroBancoNP(Controller controller, DatosTransNP datosTransNP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(),controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.clickTransferirLineaTerceros();
        transferencias.ingresarTransferirLineaTercerosNP(controller.getUsuario(),
                datosTransNP.getNombre(),datosTransNP.getRut(),datosTransNP.getCuenta(),
                datosTransNP.getBanco(), "prueba@correo.com", "pago", "Corriente");
        Assert.assertTrue(transferencias.validaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");

    }
}
