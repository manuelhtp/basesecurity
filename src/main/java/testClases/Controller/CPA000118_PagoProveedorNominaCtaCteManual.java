package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import entities.DatosTransNP;
import entities.DatosTransP;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000118_PagoProveedorNominaCtaCteManual extends BaseConfig {
    @Test(description = "Pago proveedores, Crear nueva nómina, Controller, Pago a Cta. Cte.; Cta. Vista,CPL, FEA," +
            " Saldo Cta, Cte. Ingreso manual",
            dataProvider = "controllerP", dataProviderClass = DataProviders.class)
    public void controllerMasivaTxt(Controller controller, DatosTransP datosTransP) throws AWTException {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(), controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNuevaNominaProveedor();
        pagos.creaNominaRemunearciones("PAGO DE PROVEEDORES","cuentas","manual","txt");
        pagos.clickAgregarRegitroNomina();
        pagos.generarRegistroNominaOtrosPagosManual(datosTransP.getRut(), datosTransP.getNombre());
        pagos.validacionNominaCreadaController();
        Assert.assertTrue(pagos.validaNominaPendientePagoController(), "Nómina no queda en Pendiente de Pago o Falta de Firma.");



        
    }
}
