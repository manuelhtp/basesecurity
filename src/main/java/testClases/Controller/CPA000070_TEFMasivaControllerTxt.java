package testClases.Controller;

import dataProviders.DataProviders;
import entities.Controller;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000070_TEFMasivaControllerTxt extends BaseConfig {
    @Test(description = "TEF Masivas, Controller en CLP; FEA; Saldo Cta. Cte. Estándar (TXT);",
            dataProvider = "controller", dataProviderClass = DataProviders.class)
    public void controllerMasivaTxt(Controller controller) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(),controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferenciasMasivas();
        transferencias.cargarArchivoMasivo("txt");
       Assert.assertTrue(transferencias.validaNominaMasivaPendientePagoController(),"Nómina no queda en Pendiente de Pago o Falta de Firma.");
    }
}
