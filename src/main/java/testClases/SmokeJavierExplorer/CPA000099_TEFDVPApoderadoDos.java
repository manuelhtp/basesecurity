package testClases.SmokeJavierExplorer;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000099_TEFDVPApoderadoDos extends BaseConfig {
    @Test(description = "TEF transferencias con DVP, Apoderado Dos, CPL, FEA, Saldo Cta. Cte.",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void TEFDVPController(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.entrarTransPendientesDVP();
        transferencias.pagarTransPendienteDVP(apoderadoDos.getUsuario());
        transferencias.pagarDVPConFEA();
        Assert.assertTrue(transferencias.validaTransDVPApoderadoDos(), "");
    }
}
