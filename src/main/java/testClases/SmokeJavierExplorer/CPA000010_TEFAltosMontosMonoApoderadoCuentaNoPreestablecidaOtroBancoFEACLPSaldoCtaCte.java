package testClases.SmokeJavierExplorer;

import dataProviders.DataProviders;
import entities.DatosTransNP;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000010_TEFAltosMontosMonoApoderadoCuentaNoPreestablecidaOtroBancoFEACLPSaldoCtaCte extends BaseConfig {
    @Test(description ="Transferencia altos montos No Preestablecidas mono apoderado",
            dataProvider = "monoApoderoNP", dataProviderClass = DataProviders.class)
    public void TEFAltosMontosMonoApoderadoCuentaNoPreestablecidaOtroBancoFEACLPSaldoCtaCte(MonoApoderado monoApoderado, DatosTransNP datosTransNP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferirAltosMontos();
        transferencias.generarTransferenciaAltosMontosNoPreestablecidas("monoapoderado",datosTransNP.getCuentaAM(),
                datosTransNP.getBancoAM(), datosTransNP.getNombreAM(), datosTransNP.getRutDestAM(), datosTransNP.getEmailAM(), datosTransNP.getReferenciaAM());
        Assert.assertTrue(transferencias.validarTEFAltosMontos(), "Transferencia Altos Montos No Pre: OK");
    }
}
