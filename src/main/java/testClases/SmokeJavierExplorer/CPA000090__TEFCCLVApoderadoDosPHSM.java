package testClases.SmokeJavierExplorer;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000090__TEFCCLVApoderadoDosPHSM extends BaseConfig {
    @Test(description = "TEF transferir a CCLV, PHSM, Apoderado 1, CPL, FEA, Saldo Cta. Cte.",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void TefApoderadoDosCCLVPhsm(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesCCLV("PH-SM");
        Assert.assertTrue(opPendientes.validaPendientePagoNominaApoderado2(),"Transferencia CCVL-PH-MS pago OK");
    }
}
