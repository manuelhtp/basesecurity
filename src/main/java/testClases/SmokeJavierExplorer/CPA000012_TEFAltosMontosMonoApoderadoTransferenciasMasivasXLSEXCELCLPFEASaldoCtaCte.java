package testClases.SmokeJavierExplorer;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000012_TEFAltosMontosMonoApoderadoTransferenciasMasivasXLSEXCELCLPFEASaldoCtaCte extends BaseConfig {
    @Test(description ="Transferencia altos montos mono apoderado: Transferencias masivas",
            dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void TEFAltosMontosMonoApoderadoTransferenciasMasivasXLSEXCELCLPFEASaldoCtaCte(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        Transferencias transferencias = new Transferencias();
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferirAltosMontos();
        transferencias.transferenciaAltosMontosMasivaExcelTxt("EXCEL");
    }
}
