package testClases.SmokeJavierExplorer;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000016_TransaccionesPendientesMonoApoderadoCLPFEASaldoCtaCte extends BaseConfig {
    @Test(description ="Transacciones Pendientes Monoapoderado CLP FEA Saldo Cta Cte",
            dataProvider = "monoapoderado",dataProviderClass = DataProviders.class )
    public void TransaccionesPendientesMonoApoderadoCLPFEASaldoCtaCte(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(), monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.entrarTransPendientesDVP();
        transferencias.pagarTransPendienteDVP(monoApoderado.getUsuario());
        transferencias.pagarDVPConFEA();
        Assert.assertTrue(transferencias.validarTransDVP(), "Transacciones pendientes DVP: OK");
    }
}
