package testClases.SmokeJavierExplorer;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000081__TEFAltosMontosApoderadoDosOtroBancoNP extends BaseConfig {
    @Test(description = "TEF altos montos, Apoderado 2, FEA, CPL, Saldo Cta. Cte., Otro banco, Cta. NO Preestablecida",
            dataProvider = "apoderadoDos", dataProviderClass = DataProviders.class)
    public void TefApoderadoUnoNominaTxt(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBancoAltosMontos("12123123", "np");
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoDos(), "Transferencia Altos Montos no que queda en estado Transaccion realizada,Pendiente de pago");
    }
}
