package testClases.SmokeJavierExplorer;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.OpPendientes;
import utils.BaseConfig;

public class CPA000087__TEFAltosMontosApoderadoDosNominaXls extends BaseConfig {
    @Test(description = "TEF altos montos, Apoderado 2, FEA, CPL, Saldo Cta. Cte., nómina EXCEL (XLS)",
            dataProvider = "apoderadoUnoNP", dataProviderClass = DataProviders.class)
    public void TefApoderadoUnoNominaXls(ApoderadoDos apoderadoDos) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(apoderadoDos.getRut(), apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientesAltosMontosNomina();
        opPendientes.pagarTransPendientesAltosMontosNomina();
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoDos(), "Transferencia altos montos nomina txt pagada");
    }
}
