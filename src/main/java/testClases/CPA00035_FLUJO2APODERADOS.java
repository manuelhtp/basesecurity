package testClases;

import dataProviders.DataProviders;
import entities.ApoderadoDos;
import entities.ApoderadoUno;
import entities.Controller;
import entities.DatosTransNP;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.*;
import utils.BaseConfig;

import java.awt.*;

public class CPA00035_FLUJO2APODERADOS extends BaseConfig {
    @Test(description = "Transferencia a terceros 2 apoderados",dataProvider = "fullDatos",
            dataProviderClass = DataProviders.class)
    public void flujoCompleto(Controller controller, ApoderadoUno apoderadoUno,
                              ApoderadoDos apoderadoDos, DatosTransNP datosTransNP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(controller.getRut(),controller.getPass());
        empresas.clickIngresarComo(controller.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.clickTransferirLineaTerceros();
        transferencias.ingresarTransferirLineaTercerosNP(controller.getUsuario(),
                datosTransNP.getNombre(),datosTransNP.getRut(),datosTransNP.getCuenta(),
                datosTransNP.getBanco(), "prueba@correo.com", "pago", "Corriente");
        Assert.assertTrue(transferencias.validaNominaPendientePagoController(), "Pendiente de Aprobacion no desplegado");
        empresas.logOutEmpresa();
        header.clickIngresoClientes();
        empresas.logInEmpresa(apoderadoUno.getRut(),apoderadoUno.getPass());
        empresas.clickIngresarComo(apoderadoUno.getEmpresa());
        OpPendientes opPendientes = new OpPendientes();
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBanco("12345678", "np");
        Assert.assertTrue(opPendientes.validaTEFPendientePagoApoderadoUno(), "Pendiente de Aprobacion no desplegado");
        empresas.logOutEmpresa();
        header.clickIngresoClientes();
        empresas.logInEmpresa(apoderadoDos.getRut(),apoderadoDos.getPass());
        empresas.clickIngresarComo(apoderadoDos.getEmpresa());
        opPendientes.clickTrasPendientes();
        opPendientes.pagarTransPendientesBanco("12345678", "np");
        Assert.assertTrue(opPendientes.validaTransRealizada(), "Transferencia aprobada no desplegado");
    }
}
