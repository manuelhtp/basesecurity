package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000036_OtrosPagosBuscarNominasMonoApoderadoCPLFEACtaCteCtaVista extends BaseConfig {
    @Test(description = "Otros Pagos Monoapoderado: Buscar Nominas CLP FEA Saldo cta cte"
            , dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void OtrosPagosBuscarNominasMonoApoderadoCPLFEACtaCteCtaVista(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarBuscarNominasOtrosPagos();
        pagos.buscarNominaOtrosPagos("Cuenta Corriente Cuenta Vista Vale Vista");

    }
}
