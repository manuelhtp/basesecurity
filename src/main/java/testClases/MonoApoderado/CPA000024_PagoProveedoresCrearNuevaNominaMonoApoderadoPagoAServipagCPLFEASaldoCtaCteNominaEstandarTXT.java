package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000024_PagoProveedoresCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteNominaEstandarTXT extends BaseConfig {
    @Test(description = "Pagos Proveedores Monoapoderado: Crear Nueva Nomina Pago a Servipag CLP FEA Saldo cta cte" +
            " subir archivo con datos standart txt", dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void PagoProveedoresCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteNominaEstandarTXT(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNuevaNominaProveedor();
        pagos.generarNuevaNominaProveedoresServipag("txt");
        pagos.cargarArchivoMasivo("txt");
        pagos.validarNominaPago();
        pagos.realizarOperacionPagos();
        Assert.assertTrue(pagos.validarPagos(), "Pago Proveedores Servipag nomina standar txt: OK");
    }
}
