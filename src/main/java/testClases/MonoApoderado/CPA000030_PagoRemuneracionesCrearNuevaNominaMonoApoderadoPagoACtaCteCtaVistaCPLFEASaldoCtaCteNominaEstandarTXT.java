package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000030_PagoRemuneracionesCrearNuevaNominaMonoApoderadoPagoACtaCteCtaVistaCPLFEASaldoCtaCteNominaEstandarTXT extends BaseConfig {
    @Test(description = "Pago remuneraciones, Crear nueva nómina, Mono apoderado, Pago a Cta. Cte.;" +
            " Cta. Vista, CPL, FEA, Saldo Cta, Cte. Nómina estándar (TXT)",
            dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void PagoRemuneracionesCrearNuevaNominaMonoApoderadoPagoACtaCteCtaVistaCPLFEASaldoCtaCteNominaEstandarTXT(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(), monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNominaPagoRemuneraciones();
        pagos.creaNominaRemunearciones("PAGO DE SUELDOS", "cuentas", "archivo", "TXT");
        pagos.cargarArchivoMasivo("txtRemuneraciones");
        pagos.validacionNominaCreada();
        pagos.realizarOperacionPagos();
        Assert.assertTrue(pagos.validarPagos(), "Pago Remuneraciones cta cte cta vista nomina estandar txt: OK");

    }
 }
