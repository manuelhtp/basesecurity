package testClases.MonoApoderado;


import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import utils.BaseConfig;
import dataProviders.DataProviders;


public class CPA0001_LoginEmpresaMono extends BaseConfig {

    @Test(description = "Validate logIn and LogOut site: Empresas MonoApoderado",
            dataProvider = "monoapoderado",dataProviderClass = DataProviders.class )
    public void LoginLogOut(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        Assert.assertTrue(empresas.validaIngreso(), "LogIn private site: empresa OK ");
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Assert.assertTrue(empresas.validaIngresoComo(), "Access like User OK");
        empresas.logOutEmpresa();
        Assert.assertTrue(header.validaSalir(), "LogOut private site: empresa OK ");
    }
}
