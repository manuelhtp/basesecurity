package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000018_PagoProveedoresBuscarNominasMonoApoderadoCPLFEACtaCteCtaVista extends BaseConfig {
    @Test(description = "Pagos: Proveedores Buscar Nominas monoapoderado CLP FEA Cta Cte Cta Vista",
            dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void PagoProveedoresBuscarNominasMonoApoderadoCPLFEACtaCteCtaVista(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarBuscarNominasProveedores();
        pagos.generarBuscarNominaProveedores("cta Corriente Vista Vale Vista", "PAGO DE PROVEEDORES");
    }
}
