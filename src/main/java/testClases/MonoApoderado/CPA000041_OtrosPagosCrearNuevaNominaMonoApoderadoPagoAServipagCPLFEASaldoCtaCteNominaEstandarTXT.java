package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000041_OtrosPagosCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteNominaEstandarTXT extends BaseConfig {
    @Test(description = "Otros Pagos Monoapoderado: Crear Nueva Nomina Pago a Servipag CLP FEA Saldo cta cte" +
            " subir archivo con datos standart txt", dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void OtrosPagosCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteNominaEstandarTXT(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarCrearNominaOtrosPagos();
        pagos.generarNuevaNominaProveedoresServipag("txt");
        pagos.cargarArchivoMasivo("txtRemuneracionesServipag");
        pagos.validarNominaPago();
        pagos.realizarOperacionPagos();
        Assert.assertTrue(pagos.validarPagos(), "Otros Pagos cta cte cta vista nomina estandar: OK");
    }
}
