package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000019_PagoProveedoresBuscarNominasMonoApoderadoCPLFEAServipag extends BaseConfig {
    @Test(description = "Pagos: Proveedores Buscar Nominas monoapoderado CLP FEA Servipag",
            dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void PagoProveedoresBuscarNominasMonoApoderadoCPLFEAServipag(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarBuscarNominasProveedores();
        pagos.generarBuscarNominaProveedores("efectivo servipag", "PAGO DE PROVEEDORES");
    }
}
