package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000029_PagoRemuneracionesBuscarNominasMonoApoderadoCPLFEAServipag extends BaseConfig {
    @Test(description = "Pago remuneraciones, Buscar nóminas, Mono apoderado, CPL, FEA, Servipag",
            dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void PagoRemuneracionesBuscarNominasMonoApoderadoCPLFEAServipag(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(), monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickBuscarNominaPagoRemuneraciones();
        pagos.seleccionarMediodePago("servipag");
        pagos.seleccionarMotivoAbono("PAGO DE SUELDOS");
        pagos.clickBuscar();
        Assert.assertTrue(pagos.validaCopiarEliminar(), "Opciones copiar y eliminar no desplegado");
    }
}
