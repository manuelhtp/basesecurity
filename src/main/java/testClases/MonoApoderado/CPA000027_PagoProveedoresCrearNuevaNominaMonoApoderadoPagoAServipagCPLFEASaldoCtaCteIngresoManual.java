package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.DatosTransP;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000027_PagoProveedoresCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteIngresoManual extends BaseConfig {
    @Test(description = "Pagos Proveedores Monoapoderado: Crear Nueva Nomina Pago a cuenta Servipag CLP FEA Saldo cta cte" +
            " ingreso manual", dataProvider = "monoApoderoP", dataProviderClass = DataProviders.class)
    public void PagoProveedoresCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteIngresoManual(MonoApoderado monoApoderado, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(), monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNuevaNominaProveedor();
        pagos.generarNuevaNominaProveedoresServipagManual();
        pagos.clickAgregarRegitroNomina();
        pagos.generarRegistroNominaProveedoresServipagManual(datosTransP.getRut(), datosTransP.getNombre(), datosTransP.getCorreo());
        pagos.validarNominaPago();
        pagos.realizarOperacionPagos();
        Assert.assertTrue(pagos.validarPagos(), "Pago Proveedores Servipag ingreso manual: OK");
    }
}
