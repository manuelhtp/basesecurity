package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.DatosTransNP;
import entities.MonoApoderado;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

import java.awt.*;

public class CPA00004_TEFEnLineaATercerosMonoApoderadoCLPFEASaldoCtaCteOtroBancoCtaNOPrestablecida extends BaseConfig {
    @Test(description ="Transferencia: Linea a Terceros mono apoderado CLP FEA Cta Cte Otro Banco No Preestablecida"
            , dataProvider = "monoApoderoNP", dataProviderClass = DataProviders.class)
    public void TEFEnLineaATercerosMonoApoderadoCLPFEASaldoCtaCteOtroBancoCtaNOPrestablecida(MonoApoderado monoApoderado, DatosTransNP datosTransNP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.clickTransferirLineaTerceros();
        transferencias.ingresarTransferirLineaTercerosNP(monoApoderado.getUsuario(),
                datosTransNP.getNombreLT2(),datosTransNP.getRutLT2(),datosTransNP.getCuentaLT2(),
                datosTransNP.getBancoLT2(), datosTransNP.getCorreoLT2(), datosTransNP.getComentarioLT2(), "Vista");
        Assert.assertTrue(transferencias.validarTEFLineaTerceros(), "Transferencia Linea Terceros No Pre: OK");
    }
}
