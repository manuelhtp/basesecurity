package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

public class CPA000037_OtrosPagosBuscarNominasMonoApoderadoCPLFEAServipag extends BaseConfig {
    @Test(description = "Otros Pagos Monoapoderado: Buscar Nominas CLP FEA Servipag"
            , dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void OtrosPagosBuscarNominasMonoApoderadoCPLFEAServipag(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarBuscarNominasOtrosPagos();
        pagos.buscarNominaOtrosPagos("Pago En Efectivo Servipag");

    }
}
