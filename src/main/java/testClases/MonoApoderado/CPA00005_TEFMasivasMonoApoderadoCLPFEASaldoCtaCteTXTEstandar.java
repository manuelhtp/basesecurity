package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA00005_TEFMasivasMonoApoderadoCLPFEASaldoCtaCteTXTEstandar extends BaseConfig {
    @Test(description = "TEF; Masivas; Mono apoderado; CLP; FEA; Saldo Cta. Cte. TXT (Estándar)",
            dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void TEFMasivasMonoApoderadoCLPFEASaldoCtaCteTXTEstandar(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferenciasMasivas();
        transferencias.cargarArchivoMasivo("txt");
        transferencias.pagarTransferenciasMasivas();
        Assert.assertTrue(transferencias.validarTransferenciaMasiva(), "Transferencia Masiva Excel: OK");
    }
}
