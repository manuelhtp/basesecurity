package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA000015_TEFTransferirACCLVRVMonoApoderadoCLPFEASaldoCtaCte extends BaseConfig {
    @Test(description = "TEF Transferir a CCLV; RV; Mono apoderado; CLP; FEA; Saldo Cta Cte.",
    dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void TEFTransferirACCLVRVMonoApoderadoCLPFEASaldoCtaCte(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferirCCLV();
        transferencias.cerrarModal();
        transferencias.ingresaDatosTransfCCLV("RV", "");
        transferencias.seleccionaFE();
        Assert.assertTrue(transferencias.validarCCLV(), "TEF CCLV RV: OK");
    }
}