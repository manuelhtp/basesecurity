package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

import java.awt.*;

public class CPA00009_TEFAltosMontosMonoApoderadoCuentasPrestablecidas extends BaseConfig {
    @Test(description ="Transferencia altos montos preestablecidas mono apoderado",
            dataProvider = "monoapoderado",dataProviderClass = DataProviders.class )
    public void TEFAltosMontosMonoApoderadoCuentasPrestablecidas(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(), monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferirAltosMontos();
        transferencias.generarTransferenciaAltosMontosPreestablecidas(monoApoderado.getUsuario());
        Assert.assertTrue(transferencias.validarTEFAltosMontos(), "Transferencia Altos Montos Pre: OK");
    }
}
