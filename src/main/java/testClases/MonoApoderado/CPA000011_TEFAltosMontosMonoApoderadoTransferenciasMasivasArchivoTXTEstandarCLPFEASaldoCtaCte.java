package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

import java.awt.*;

public class CPA000011_TEFAltosMontosMonoApoderadoTransferenciasMasivasArchivoTXTEstandarCLPFEASaldoCtaCte extends BaseConfig {
    @Test(description =". TEF Altos montos Mono Apoderado, Transferencias Masivas Archivo TXT CLP; FEA; Saldo Cta. Cte.(Estándar)",
    dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void TEFAltosMontosMonoApoderadoTransferenciasMasivasArchivoTXTEstandarCLPFEASaldoCtaCte(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferirAltosMontos();
        transferencias.transferenciaAltosMontosMasivaExcelTxt("TXT");
        transferencias.pagarTEFAltosMontosNominas();
        Assert.assertTrue(transferencias.validarTEFAltosMontosNominas(), "TEF Altos Montos Nomina: OK");
    }
}
