package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.EditarDatos;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000055_DespachoCorrespondenciaMonoApoderadoFEANoDeseoRecibirlasLasConsultareEnLinea extends BaseConfig {
    @Test(description = "Despacho Correspondencia MonoApoderado: FEA No Deseo Recibirlas Las Consultare en Linea",
            dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void DespachoCorrespondenciaMonoApoderadoFEANoDeseoRecibirlasLasConsultareEnLinea(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        header.editarContacto();
        EditarDatos editarDatos = new EditarDatos();
        editarDatos.clickEditCorrespondecia();
        editarDatos.cambiarFormaDespacho();
        editarDatos.despachoCorrespondencia("consultar en linea");
        editarDatos.modificarDespachoCorrespondencia();
        Assert.assertTrue(editarDatos.validaModificarDespacho(), "Modificar despacho de correspondencia no realizado");
    }
}
