package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

public class CPA00006_TEFMasivasMonoApoderadoCLPFEASaldoCtaCteXLSEXCEL extends BaseConfig {
    @Test(description ="Transferencias Masivas: FEA Saldo cta cte mono apoderado excel",
            dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void TEFMasivasMonoApoderadoCLPFEASaldoCtaCteXLSEXCEL(MonoApoderado monoApoderado)  {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.ingresarTransferenciasMasivas();
        transferencias.cargarArchivoMasivo("xls");
        transferencias.pagarTransferenciasMasivas();
        Assert.assertTrue(transferencias.validarTransferenciaMasiva(), "Transferencia Masiva Excel: OK");
    }
}
