package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.DatosTransP;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000043_OtrosPagosCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteIngresoManual extends BaseConfig {
    @Test(description = "Otros Pagos Monoapoderado: Crear Nueva Nomina Pago a cuenta Servipag CLP FEA Saldo cta cte" +
            " Ingreso manual", dataProvider = "monoApoderoP", dataProviderClass = DataProviders.class)
    public void OtrosPagosCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteIngresoManual(MonoApoderado monoApoderado, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(), monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarCrearNominaOtrosPagos();
        pagos.generarNuevaNominaProveedoresServipagManual();
        pagos.clickAgregarRegitroNomina();
        pagos.generarRegistroNominaOtrosPagosServipagManual(datosTransP.getRut(), datosTransP.getNombre());
        pagos.validarNominaPago();
        pagos.realizarOperacionPagos();
        Assert.assertTrue(pagos.validarPagos(), "Otros Pagos servipag ingreso manual: OK");
    }
}
