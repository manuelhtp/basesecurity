package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.DatosTransNP;
import entities.MonoApoderado;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

import java.awt.*;

public class CPA00002_TEFEnLineaa3rosMonoApoderadoCLPFEASaldoCtaCteMismoBancoCtaNoPrestablecida extends BaseConfig {
    @Test(description ="Transferencia: Linea a Terceros", dataProvider = "monoApoderoNP",
    dataProviderClass = DataProviders.class)
    public void TEFEnLineaa3rosMonoApoderadoCLPFEASaldoCtaCteMismoBancoCtaNoPrestablecida(MonoApoderado monoApoderado, DatosTransNP datosTransNP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.clickTransferirLineaTerceros();
        transferencias.ingresarTransferirLineaTercerosNP(monoApoderado.getUsuario(),
                datosTransNP.getNombreLT(), datosTransNP.getRutLT(), datosTransNP.getCuentaLT(), datosTransNP.getBancoLT(),
                datosTransNP.getCorreoLT(), datosTransNP.getComentarioLT(), "Corriente");
        Assert.assertTrue(transferencias.validarTEFLineaTerceros(), "Transferencia Linea Terceros No Pre: OK");
    }
}
