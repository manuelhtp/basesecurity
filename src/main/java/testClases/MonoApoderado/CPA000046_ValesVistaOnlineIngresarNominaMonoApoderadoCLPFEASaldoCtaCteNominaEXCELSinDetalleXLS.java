package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.DatosTransP;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import page.ValeVistas;
import utils.BaseConfig;

import java.awt.*;

public class CPA000046_ValesVistaOnlineIngresarNominaMonoApoderadoCLPFEASaldoCtaCteNominaEXCELSinDetalleXLS extends BaseConfig {
    @Test(description = "Vales vista online, Ingresar nómina, Mono apoderado, CLP, FEA, " +
            "Saldo Cta. Cte. Nómina EXCEL sin detalle (XLS)",
            dataProvider = "monoApoderoP", dataProviderClass = DataProviders.class)
    public void ValesVistaOnlineIngresarNominaMonoApoderadoCLPFEASaldoCtaCteNominaEXCELSinDetalleXLS(MonoApoderado monoApoderado, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(), monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        ValeVistas valeVistas = new ValeVistas();
        valeVistas.clickIngresarNomina();
        valeVistas.clickCrearNomina();
        valeVistas.completaDatosNuevaNomina("archivo", "xlsSinD");
        valeVistas.cargarArchivoMasivo("xlsSinD");
        valeVistas.validacionNominaCargada(monoApoderado.getUsuario(), "");
        valeVistas.realizarPagoConFea();
        Assert.assertTrue(pagos.validarValeVista(), "Vale Vista Online Ingresar Nomina nomina Excel sin detalle: OK");
    }
}
