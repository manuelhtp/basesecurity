package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.ApoderadoUno;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.EditarDatos;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000056_DespachoCorrespondenciaMonoApoderadoFEADeseoRecibirlasPorCorreoFisicoRemitente1 extends BaseConfig {
    @Test(description = "Despacho Correspondencia MonoApoderado: FEA Deseo Recibirlas por Correo Fisico Remitente 1",
            dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void DespachoCorrespondenciaApoderado1FEADeseoRecibirlasPorCorreoFisicoRemitente1(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        header.editarContacto();
        EditarDatos editarDatos = new EditarDatos();
        editarDatos.clickEditCorrespondecia();
        editarDatos.cambiarFormaDespacho();
        editarDatos.despachoCorrespondencia("recibirlas correo fisico");
        editarDatos.modificarDespachoCorrespondencia();
        Assert.assertTrue(editarDatos.validaModificarDespacho(), "Modificar despacho de correspondencia no realizado");
    }
}
