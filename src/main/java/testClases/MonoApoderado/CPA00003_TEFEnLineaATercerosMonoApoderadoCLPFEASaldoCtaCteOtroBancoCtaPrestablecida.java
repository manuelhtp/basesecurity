package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.DatosTransP;
import entities.MonoApoderado;
import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Transferencias;
import utils.BaseConfig;

import java.awt.*;

public class CPA00003_TEFEnLineaATercerosMonoApoderadoCLPFEASaldoCtaCteOtroBancoCtaPrestablecida extends BaseConfig {
    @Test(description = "Transferir en línea a terceros Mono apoderado; CLP; FEA; Saldo Cta Cte; Otro banco; Cta Prestablecida",
            dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void TEFEnLineaATercerosMonoApoderadoCLPFEASaldoCtaCteOtroBancoCtaPrestablecida(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(), monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Transferencias transferencias = new Transferencias();
        transferencias.ingresarTranferencias();
        transferencias.clickTransferirLineaTerceros();
        transferencias.transfTercerosOtroBancoPaso1("ange - 123");
        Assert.assertTrue(transferencias.validarTEFLineaTerceros(), "Transferencia Linea Terceros Pre: OK");
    }
}
