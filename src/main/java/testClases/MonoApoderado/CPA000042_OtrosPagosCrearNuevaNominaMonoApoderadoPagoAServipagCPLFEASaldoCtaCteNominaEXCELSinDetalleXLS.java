package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000042_OtrosPagosCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteNominaEXCELSinDetalleXLS extends BaseConfig {
    @Test(description = "Otros Pagos Monoapoderado: Crear Nueva Nomina Pago a Servipag CLP FEA Saldo cta cte" +
            " subir archivo con datos excel sin detalle", dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void OtrosPagosCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteNominaEXCELSinDetalleXLS(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarCrearNominaOtrosPagos();
        pagos.generarNuevaNominaProveedoresServipag("Excel sin detalle");
        pagos.cargarArchivoMasivo("xls sin detalle servipag");
        pagos.validarNominaPago();
        pagos.realizarOperacionPagos();
        Assert.assertTrue(pagos.validarPagos(), "Otros Pagos servipag nomina excel sin detalle: OK");
    }
}
