package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.DatosTransP;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000035_PagoRemuneracionesCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteIngresoManual extends BaseConfig {
    @Test(description = "Pago remuneraciones, Crear nueva nómina, Mono apoderado, " +
            "Pago a Servipag, CPL, FEA, Saldo Cta, Cte. Ingreso manual",
            dataProvider = "monoApoderoP", dataProviderClass = DataProviders.class)
    public void PagoRemuneracionesCrearNuevaNominaMonoApoderadoPagoAServipagCPLFEASaldoCtaCteIngresoManual(MonoApoderado monoApoderado, DatosTransP datosTransP) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(), monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNominaPagoRemuneraciones();
        pagos.creaNominaRemunearciones("PAGO DE SUELDOS", "servipag", "manual", "XLS");
        pagos.agregarRegistroRemuneraciones(datosTransP.getRut(), datosTransP.getNombre(),
                datosTransP.getCuenta(), "servipag");
        pagos.validacionNominaCreada();
        pagos.realizarOperacionPagos();
        Assert.assertTrue(pagos.validarPagos(), "Pago Remuneraciones Servipag manual: OK");
    }
}