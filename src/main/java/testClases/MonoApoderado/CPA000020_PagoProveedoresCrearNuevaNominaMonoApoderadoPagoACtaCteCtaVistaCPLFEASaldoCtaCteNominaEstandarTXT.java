package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.Controller;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000020_PagoProveedoresCrearNuevaNominaMonoApoderadoPagoACtaCteCtaVistaCPLFEASaldoCtaCteNominaEstandarTXT extends BaseConfig {
    @Test(description = "Pagos Proveedores Monoapoderado: Crear Nueva Nomina Pago a cuenta corriente cuenta vista vale vista CLP FEA Saldo cta cte" +
            " subir archivo con datos standart txt", dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void PagoProveedoresCrearNuevaNominaMonoApoderadoPagoACtaCteCtaVistaCPLFEASaldoCtaCteNominaEstandarTXT(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNuevaNominaProveedor();
        pagos.generarNuevaNominaProveedor("txt");
        pagos.cargarArchivoMasivo("txt");
        pagos.validarNominaPago();
        pagos.realizarOperacionPagos();
        Assert.assertTrue(pagos.validarPagos(), "Pago Proveedores Crear Nueva nomina cta cte cta vista nomina estandar txt: OK");
    }
}
