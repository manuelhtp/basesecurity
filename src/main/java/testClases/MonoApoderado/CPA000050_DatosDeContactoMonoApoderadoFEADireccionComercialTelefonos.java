package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.DatosContacto;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.EditarDatos;
import page.Empresas;
import page.Header;
import utils.BaseConfig;

public class CPA000050_DatosDeContactoMonoApoderadoFEADireccionComercialTelefonos extends BaseConfig {
    @Test(description = "Datos de Contacto monoapoderado: FEA Direccion Comercial Telefonos",
            dataProvider = "monoApoderadoContacto", dataProviderClass = DataProviders.class)
    public void DatosDeContactoMonoApoderadoFEADireccionComercialTelefonos(MonoApoderado monoApoderado,
                                                                           DatosContacto datosContacto) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        header.editarContacto();
        EditarDatos editarDatos = new EditarDatos();
        editarDatos.clickEditContacto();
        editarDatos.modificarDatosContactoFEA(datosContacto.getCalle(), datosContacto.getNumero(), datosContacto.getOficinaN(),
                datosContacto.getRegion(), datosContacto.getCiudad(), datosContacto.getComuna(), datosContacto.getTelefono1(),
                datosContacto.getTelefono2());
        editarDatos.guardarCambiosModificarDatos();
        Assert.assertTrue(editarDatos.validarModificarDatos(), "Modificación de Datos: OK");
    }
}
