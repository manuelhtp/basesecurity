package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000022_PagoProveedoresCrearNuevaNominaMonoApoderadoPagoACtaCteCtaVistaCPLFEASaldoCtaCteNominaEXCELSinDetalleXLS extends BaseConfig {
    @Test(description = "Pagos Proveedores Monoapoderado: Crear Nueva Nomina Pago a cuenta corriente cuenta vista vale vista CLP FEA Saldo cta cte" +
            " subir archivo con datos excel con detalle", dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void PagoProveedoresCrearNuevaNominaMonoApoderadoPagoACtaCteCtaVistaCPLFEASaldoCtaCteNominaEXCELSinDetalleXLS(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.clickCrearNuevaNominaProveedor();
        pagos.generarNuevaNominaProveedor("Excel sin detalle");
        pagos.cargarArchivoMasivo("xls sin detalle");
        pagos.validarNominaPago();
        pagos.realizarOperacionPagos();
        Assert.assertTrue(pagos.validarPagos(), "Pago Proveedores Crear Nueva nomina cta cte cta vista Excel sin detalle: OK");
    }
}
