package testClases.MonoApoderado;

import dataProviders.DataProviders;
import entities.MonoApoderado;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.Empresas;
import page.Header;
import page.Pagos;
import utils.BaseConfig;

import java.awt.*;

public class CPA000039_OtrosPagosCrearNuevaNominaMonoApoderadoPagoACtaCteCtaVistaCPLFEASaldoCtaCteNominaEXCELSinDetalleXLS extends BaseConfig {
    @Test(description = "Otros Pagos Monoapoderado: Crear Nueva Nomina pago a Cta Cte Cta Vista CLP FEA Saldo cta cte" +
            "Nomina Excel sin detalle" , dataProvider = "monoapoderado", dataProviderClass = DataProviders.class)
    public void OtrosPagosCrearNuevaNominaMonoApoderadoPagoACtaCteCtaVistaCPLFEASaldoCtaCteNominaEXCELSinDetalleXLS(MonoApoderado monoApoderado) {
        Header header = new Header();
        header.clickEmpresas();
        header.clickIngresoClientes();
        Empresas empresas = new Empresas();
        empresas.logInEmpresa(monoApoderado.getRut(),monoApoderado.getPass());
        empresas.clickIngresarComo(monoApoderado.getEmpresa());
        Pagos pagos = new Pagos();
        pagos.ingresarAPagos();
        pagos.ingresarCrearNominaOtrosPagos();
        pagos.generarNuevaNominaOtrosPagos("Excel sin detalle");
        pagos.cargarArchivoMasivo("xls sin detalle");
        pagos.validarNominaPago();
        pagos.realizarOperacionPagos();
        Assert.assertTrue(pagos.validarPagos(), "Otros Pagos cta cte cta vista nomina Excel sin detalle: OK");
    }
}
