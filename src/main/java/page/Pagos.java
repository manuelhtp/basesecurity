package page;

import driver.DriverContext;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import utils.MetodosGenericos;

import java.awt.*;
import java.io.File;

/**
 * The type Pagos.
 */
public class Pagos {
    private WebDriver driver;

    /**
     * Instantiates a new Pagos.
     */
    public Pagos() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "//frame[contains(@id, 'topFrame')]")
    private WebElement topFrame;

    @FindBy(xpath = "//frame[@id='leftFrame']")
    private WebElement leftFrame;

    @FindBy(id = "mainFrame")
    private WebElement mainFrame;

    @FindBy(name = "Arriba")
    private WebElement arriba;

    @FindBy(id = "central")
    private WebElement central;

    @FindBy(xpath = "//li[@class='estilo1']//a[contains(text(), 'Pagos')]")
    private WebElement pagosBtn;

    @FindBy(xpath = "(//a[text()='Crear nueva nómina'])[1]")
    private WebElement nuevaNominaPagosProveedoresLnk;

    @FindBy(xpath = "//a[text()='Pagos Previsionales']")
    private WebElement pagosPrevisionalesLnk;

    @FindBy(name = "cta")
    private WebElement cuentaCorrienteACargar;

    @FindBy(xpath = "//a[text()='Continuar']")
    private WebElement continuarBtn;

    @FindBy(xpath = "//a[text()='Pago de Remuneraciones']")
    private WebElement pagoRemuneracionesLnk;

    @FindBy(xpath = "//a[text()='Otros pagos']")
    private WebElement otrosPagosLnk;

    @FindBy(xpath = "(//a[text()='Buscar nóminas'])[1]")
    private WebElement buscarNominaProveedoresLnk;

    @FindBy(xpath = "(//input[@id='MedioPago'])[2]")
    private WebElement medioPagoEfectivoServipagBuscarNomina;

    @FindBy(xpath = "//div[@id='btn-subir']//button[text()='Crear nómina y continuar']")
    private WebElement crearNominayContinuarBtn;

    @FindBy(xpath = "//div[@id='btn-digitar']//button[text()='Crear nómina y continuar']")
    private WebElement crearNominayContinuarManualBtn;

    @FindBy(id = "optionsRadios4")
    private WebElement formatoArchivoExcelConDetalleRd;

    @FindBy(id = "optionsRadios5")
    private WebElement formatoArchivoExcelSinDetalleRd;

    @FindBy(xpath = "(//a[contains(@href,'javascript:Copiar')])[1]")
    private WebElement copiarAccionBtn;

    @FindBy(xpath = "(//tr//a[contains(@href,'javascript:Eliminar')])[1]")
    private WebElement eliminarAccionBtn;

    @FindBy(name = "Archivo")
    private WebElement archivoDeDatosFile;

    @FindBy(xpath = "(//a[text()='Validar'])[1]")
    private WebElement validarProveedoresBtn;

    @FindBy(xpath = "(//a[text()='Confirmar'])[1]")
    private WebElement confirmarEnviarValidacionBtn;

    @FindBy(xpath = "(//a[text()='Pagar'])[1]")
    private WebElement pagarProveedoresBtn;

    @FindBy(xpath = "(//a[text()='Confirmar'])[3]")
    private WebElement confirmarEnviarPagoBtn;

    @FindBy(id = "clave-dinamica-radio4")
    private WebElement feaInput;

    @FindBy(id = "btnDePago")
    private WebElement realizarOperacionPagoProveBtn;

    @FindBy(xpath = "//input[@id='TipoIngreso' and @value='1']")
    private WebElement tipoIngresoNominaInpt;

    @FindBy(xpath = "//a[text()=' Agregar registro']")
    private WebElement agregarRegistroNominaBtn;

    @FindBy(id = "Rut")
    private WebElement rutNominaInpt;

    @FindBy(id = "Nombre")
    private WebElement nombreNominaInpt;

    @FindBy(id = "Email_Proveedor")
    private WebElement emailNominaInpt;

    @FindBy(id = "optionsRadios1")
    private WebElement medioPagoNominaInpt;

    @FindBy(id = "optionsRadios2")
    private WebElement medioPagoNominaSecurityInpt;

    @FindBy(name = "IdBanco")
    private WebElement bancoDestinoNominaSel;

    @FindBy(name = "FormaAbono")
    private WebElement tipoCuentaDestinoComboNomina;

    @FindBy(id = "NumeroCuenta")
    private WebElement numeroCuentaNominaInpt;

    @FindBy(id = "Monto")
    private WebElement montoNominaInpt;

    @FindBy(id = "button1")
    private WebElement guardarCambiosBtn;

    @FindBy(xpath = "//input[@value='N']")
    private WebElement cuentasRd;

    @FindBy(xpath = "//input[@value='S']")
    private WebElement previredRd;

    @FindBy(name = "MotivoAbono")
    private WebElement motivoAbonoSel;

    @FindBy(xpath = "//a[text()=' Buscar']")
    private WebElement buscarBtn;

    @FindBy(xpath = "//a[text()='copiar']")
    private WebElement copiarBtn;

    @FindBy(xpath = "//a[text()='eliminar']")
    private WebElement eliminarBtn;

    @FindBy(xpath = "(//a[text()='Crear nueva nómina'])[2]")
    private WebElement crearNominaRemuneracionBtn;

    @FindBy(name = "FechaAbono")
    private WebElement fechaAbonoInpt;

    @FindBy(xpath = "//input[@value='2']")
    private WebElement archivoRd;

    @FindBy(xpath = "//input[@name='TipoIngreso'and @value='1']")
    private WebElement manualRd;

    @FindBy(id = "optionsRadios3")
    private WebElement txtRd;

    @FindBy(id = "optionsRadios5")
    private WebElement xlsRd;

    @FindBy(xpath = "//a[text()='Confirmar']")
    private WebElement confirmarBtn;

    @FindBy(xpath = "(//a[text()='Pagar'])[1]")
    private WebElement pagarNominaBtn;

    @FindBy(xpath = "(//a[text()='Pagar'])[2]")
    private WebElement pagarRegistroBtn;

    @FindBy(xpath = "(//a[text()='Buscar nóminas'])[2]")
    private WebElement buscarNominaPagoRemuneracionBtn;

    @FindBy(xpath = "//div[@id='btn-digitar']//button[text()='Crear nómina y continuar']")
    private WebElement crearNominaYContinuarBtnNomina;

    @FindBy(name = "FechaInicio")
    private WebElement fechaInicioServipag;

    @FindBy(name = "FechaTermino")
    private WebElement fechaTerminoServipag;
    
    @FindBy(id = "optionsRadios4")
    private WebElement tipoCuentaVistaInpt;

    @FindBy(id = "optionsRadios3")
    private WebElement tipoCuentaCteInpt;

    @FindBy(name = "FechaTermino")
    private WebElement fechaAbonoTerminoInpt;

    @FindBy(name = "FechaInicio")
    private WebElement fechaAbonoInicioInpt;

    @FindBy(xpath = "(//a[text()='Buscar nóminas'])[3]")
    private WebElement buscarNominaOtrosPagosLnk;

    @FindBy(xpath = "(//a[text()='Crear nueva nómina'])[3]")
    private WebElement crearNuevaNominaOtrosPagos;

    @FindBy(xpath = "//input[@name='MedioAbono' and @value='3']")
    private WebElement medioPagoNominaOtrosPagosVV;

    @FindBy(xpath = "(//a[contains(text(),'Enviar a aprobación')])[2]")
    private WebElement enviarAprobacionNominaBtn;

    @FindBy(xpath = "//img[contains(@src,'timbre-ok-icon')]")
    private WebElement timbreOkPagos;

    @FindBy(name = "Indexa")
    private WebElement indexaFrm;

    /**
     * Ingresar a pagos.
     */
    public void ingresarAPagos() {
        driver.switchTo().frame("topFrame");
        MetodosGenericos.visualizarObjeto(pagosBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", pagosBtn);
    }

    /**
     * Ingresar pagos proveedores.
     */
    public void clickCrearNuevaNominaProveedor() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", nuevaNominaPagosProveedoresLnk);
    }

    /**
     * Ingresar pagos previsionales.
     */
    public void ingresarPagosPrevisionales() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", pagosPrevisionalesLnk);
    }

    /**
     * Generar pagos previsionales.
     */
    public void generarPagosPrevisionales() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        Select sel = new Select(cuentaCorrienteACargar);
        sel.selectByIndex(0);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", continuarBtn);
    }

    /**
     * Ingresar pagos remuneraciones.
     */
    public void ingresarPagosRemuneraciones() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", pagoRemuneracionesLnk);
    }

    /**
     * Generar buscar nomina proveedores.
     *
     * @param medio       the medio
     * @param motivoAbono the motivo abono
     */
    public void generarBuscarNominaProveedores(String medio, String motivoAbono) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        switch (medio) {
            case "cta Corriente Vista Vale Vista":
                Select sel = new Select(motivoAbonoSel);
                sel.selectByVisibleText(motivoAbono);
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", buscarBtn);
                MetodosGenericos.waitFor(5);
                Assert.assertTrue(MetodosGenericos.visualizarObjeto(copiarAccionBtn, 5));
                Assert.assertTrue(MetodosGenericos.visualizarObjeto(eliminarAccionBtn, 5));
                break;
            case "efectivo servipag":
                Select sel2 = new Select(motivoAbonoSel);
                sel2.selectByVisibleText(motivoAbono);
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", medioPagoEfectivoServipagBuscarNomina);
                JavascriptExecutor executor3 = (JavascriptExecutor) driver;
                executor3.executeScript("arguments[0].click();", buscarBtn);
                MetodosGenericos.waitFor(5);
                Assert.assertTrue(MetodosGenericos.visualizarObjeto(copiarAccionBtn, 5));
                Assert.assertTrue(MetodosGenericos.visualizarObjeto(eliminarAccionBtn, 5));
                break;
            default:
                break;
        }
    }

    /**
     * Generar nueva nomina.
     *
     * @param formato String
     */
    public void generarNuevaNominaProveedor(String formato) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        fechaAbonoInpt.sendKeys(MetodosGenericos.devolverDiaSiguiente());
        switch (formato) {
            case "TXT":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", txtRd);
                break;
            case "XLS":
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", xlsRd);
                break;
            case "Excel con detalle":
                JavascriptExecutor executor3 = (JavascriptExecutor) driver;
                executor3.executeScript("arguments[0].click();", formatoArchivoExcelConDetalleRd);
                break;
            case "Excel sin detalle":
                JavascriptExecutor executor4 = (JavascriptExecutor) driver;
                executor4.executeScript("arguments[0].click();", formatoArchivoExcelSinDetalleRd);
                break;
        }
        JavascriptExecutor executor5 = (JavascriptExecutor) driver;
        executor5.executeScript("arguments[0].click();", crearNominayContinuarBtn);
    }

    /**
     * Generar nueva nomina proveedores manual.
     */
    public void generarNuevaNominaProveedoresManual() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        fechaAbonoInpt.sendKeys(MetodosGenericos.devolverDiaSiguiente());
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", tipoIngresoNominaInpt);
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", crearNominaYContinuarBtnNomina);
    }

    /**
     * Cargar archivo masivo.
     *
     * @param tipo the tipo
     */
    public void cargarArchivoMasivo(String tipo) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        MetodosGenericos.visualizarObjeto(archivoDeDatosFile, 5);
        switch (tipo) {
            case "xls con detalle":
                File file = new File("src/main/java/recursos/Nomina P.Prov C.Detalle.xlsx");
                driver.findElement(By.xpath("//input[@name='Archivo']")).
                        sendKeys(file.getAbsolutePath());
                break;
            case "txt":
                File file2 = new File("src/main/java/recursos/Nomina P.Prov S.Detalle.txt");
                driver.findElement(By.xpath("//input[@type ='File']")).
                        sendKeys(file2.getAbsolutePath());
                break;
            case "xls sin detalle":
                File file3 = new File("src/main/java/recursos/Nomina P.Prov S.Detalle.xlsx");
                driver.findElement(By.xpath("//input[@name='Archivo']")).
                        sendKeys(file3.getAbsolutePath());
                break;
            case "xlsRemuneraciones":
                File file4 = new File("src/main/java/recursos/Nomina P.Remuneraciones.xlsx");
                driver.findElement(By.xpath("//input[@type ='File']")).
                        sendKeys(file4.getAbsolutePath());
                break;
            case "txtRemuneraciones":
                File file5 = new File("src/main/java/recursos/Nomina Pago Remuneraciones2.txt");
                driver.findElement(By.xpath("//input[@type ='File']")).
                        sendKeys(file5.getAbsolutePath());
                break;
            case "txtRemuneracionesServipag":
                File file6 = new File("src/main/java/recursos/Nomina Pago Remuneraciones.txt");
                driver.findElement(By.xpath("//input[@type ='File']")).
                        sendKeys(file6.getAbsolutePath());
                break;
            case "xls sin detalle servipag":
                File file7 = new File("src/main/java/recursos/Nomina P.Prov S.Detalle - Servipag.xlsx");
                driver.findElement(By.xpath("//input[@type ='File']")).
                        sendKeys(file7.getAbsolutePath());
                break;
            case "xls con detalle servipag":
                File file8 = new File("src/main/java/recursos/Nomina P.Prov C.Detalle - Servipag.xlsx");
                driver.findElement(By.xpath("//input[@type ='File']")).
                        sendKeys(file8.getAbsolutePath());
                break;
        }
        MetodosGenericos.waitClickable(driver, continuarBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", continuarBtn);
    }

    /**
     * Validar nomina pago proveedores.
     */
    public void validarNominaPago() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", validarProveedoresBtn);
        MetodosGenericos.waitVisibility(driver, confirmarEnviarValidacionBtn, 5);
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", confirmarEnviarValidacionBtn);
        MetodosGenericos.waitFor(3);
        JavascriptExecutor executor3 = (JavascriptExecutor) driver;
        executor3.executeScript("arguments[0].click();", pagarProveedoresBtn);
        MetodosGenericos.waitVisibility(driver, confirmarEnviarPagoBtn, 5);
        JavascriptExecutor executor4 = (JavascriptExecutor) driver;
        executor4.executeScript("arguments[0].click();", confirmarEnviarPagoBtn);
    }

    /**
     * Realizar operacion pago proveedores.
     *
     * @throws AWTException the awt exception
     */
    public void realizarOperacionPagos() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        driver.switchTo().frame("central");
        MetodosGenericos.waitFor(20);
        MetodosGenericos.waitClickable(driver, feaInput, 25);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", feaInput);
        MetodosGenericos.waitFor(3);
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", realizarOperacionPagoProveBtn);
        MetodosGenericos.waitFor(3);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(2);
        MetodosGenericos.ingresarPinMonoapoderadoFEA();
        MetodosGenericos.waitFor(10);
    }

    /**
     * Click buscar nomina pago remuneraciones.
     */
    public void clickBuscarNominaPagoRemuneraciones() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        MetodosGenericos.waitClickable(driver, buscarNominaPagoRemuneracionBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", buscarNominaPagoRemuneracionBtn);
    }

    /**
     * Seleccionar mediode pago.
     *
     * @param tipo String tipo
     */
    public void seleccionarMediodePago(String tipo) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        switch (tipo) {
            case "servipag":
                MetodosGenericos.waitClickable(driver, previredRd, 5);
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", previredRd);
                break;
            case "cuentas":
                break;
            default:
        }
    }

    /**
     * Seleccionar motivo abono.
     *
     * @param motivo String motivo
     */
    public void seleccionarMotivoAbono(String motivo) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        Toolkit.getDefaultToolkit().beep();
        MetodosGenericos.waitClickable(driver, motivoAbonoSel, 5);
        Select sel = new Select(motivoAbonoSel);
        sel.selectByVisibleText(motivo);

    }

    /**
     * Click buscar.
     */
    public void clickBuscar() {
        MetodosGenericos.waitClickable(driver, buscarBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", buscarBtn);
    }

    /**
     * Ingresar otros pagos.
     */
    public void ingresarOtrosPagos() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", otrosPagosLnk);
    }

    /**
     * Ingresar buscar nominas proveedores.
     */
    public void ingresarBuscarNominasProveedores() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", buscarNominaProveedoresLnk);
    }

     /**
     * Valida copiar eliminar boolean.
     *
     * @return the boolean
     */
    public boolean validaCopiarEliminar() {
        boolean aux;
        if (copiarBtn.isDisplayed() || eliminarBtn.isDisplayed()) {
            aux = true;
        } else
            aux = false;
        return aux;
    }

    /**
     * Click crear nomina pago remuneraciones.
     */
    public void clickCrearNominaPagoRemuneraciones() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        MetodosGenericos.waitClickable(driver, crearNominaRemuneracionBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", crearNominaRemuneracionBtn);
    }

    /**
     * Crea nomina remunearciones.
     *
     * @param motivo      the motivo
     * @param tipo        the tipo
     * @param tipoIngreso the tipo ingreso
     * @param formato     the formato
     */
    public void creaNominaRemunearciones(String motivo, String tipo, String tipoIngreso, String formato) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        Select sel = new Select(motivoAbonoSel);
        sel.selectByVisibleText(motivo);
        seleccionarMediodePago(tipo);
        if (tipo.equals("cuentas")) {
            fechaAbonoInpt.sendKeys(MetodosGenericos.devolverSubSiguienteDia());
        } else if (tipo.equals("servipag")) {
            fechaAbonoInicioInpt.sendKeys( MetodosGenericos.devolverSubSiguienteDia());
            fechaAbonoTerminoInpt.sendKeys(MetodosGenericos.devolver15DiasDespues());
        }
        seleccionarTipoIngreso(tipoIngreso);
        if (tipoIngreso.equals("archivo")) {
            generarNuevaNomina(formato);
        } else {
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", crearNominayContinuarManualBtn);
        }
    }

    private void generarNuevaNomina(String formato) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        MetodosGenericos.waitFor(3);
        switch (formato) {
            case "txt":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", txtRd);
                break;
            case "xls":
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", xlsRd);
                break;
            case "Excel con detalle":
                JavascriptExecutor executor3 = (JavascriptExecutor) driver;
                executor3.executeScript("arguments[0].click();", formatoArchivoExcelConDetalleRd);
                break;
            case "Excel sin detalle":
                JavascriptExecutor executor4 = (JavascriptExecutor) driver;
                executor4.executeScript("arguments[0].click();", formatoArchivoExcelSinDetalleRd);
                break;
        }
        JavascriptExecutor executor5 = (JavascriptExecutor) driver;
        executor5.executeScript("arguments[0].click();", crearNominayContinuarBtn);
    }

    /**
     * Seleccionar tipo ingreso.
     *
     * @param tipoIngreso the tipo ingreso
     */
    public void seleccionarTipoIngreso(String tipoIngreso) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        MetodosGenericos.waitClickable(driver, manualRd, 10);
        switch (tipoIngreso) {
            case "archivo":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", archivoRd);
                break;
            case "manual":
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", manualRd);
                break;
            default:
                break;
        }
    }

      /**
     * Validacion nomina creada.
     */
    public void validacionNominaCreada() {
        JavascriptExecutor executor4 = (JavascriptExecutor) driver;
        executor4.executeScript("arguments[0].click();", driver.findElement(By.xpath("(//a[contains(text(),'Validar')])[2]")));
        MetodosGenericos.waitVisibility(driver, confirmarBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", confirmarBtn);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", pagarNominaBtn);
        MetodosGenericos.waitFor(10);
        JavascriptExecutor executor3 = (JavascriptExecutor) driver;
        executor3.executeScript("arguments[0].click();", driver.findElement(By.xpath("(//a[text()='Confirmar'])[3]")));
    }

    /**
     * Generar registro nomina proveedores manual.
     *
     * @param rut        the rut
     * @param nombre     the nombre
     * @param correo     the correo
     * @param banco      the banco
     * @param tipoCuenta the tipo cuenta
     * @param cuenta     the cuenta
     */
    public void generarRegistroNominaProveedoresManual(String rut, String nombre, String correo, String banco
            , String tipoCuenta, String cuenta) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        rut = rut.replace("-", "");
        rutNominaInpt.sendKeys(rut);
        nombreNominaInpt.sendKeys(nombre);
        emailNominaInpt.sendKeys(correo);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", medioPagoNominaInpt);
        MetodosGenericos.waitFor(3);
        Select sel = new Select(bancoDestinoNominaSel);
        sel.selectByVisibleText(banco);
        Select sel2 = new Select(tipoCuentaDestinoComboNomina);
        sel2.selectByVisibleText(tipoCuenta);
        numeroCuentaNominaInpt.sendKeys(cuenta);
        montoNominaInpt.sendKeys("1");
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", guardarCambiosBtn);
    }

    /**
     * Agregar registro remuneraciones.
     *
     * @param rut    the rut
     * @param nombre the nombre
     * @param cuenta the cuenta
     * @param modo   the modo
     */
    public void agregarRegistroRemuneraciones(String rut, String nombre, String cuenta, String modo) {
        clickAgregarRegitroNomina();
        switch (modo){
            case("manual"):
                rut = rut.replace("-", "");
                rutNominaInpt.sendKeys(rut);
                nombreNominaInpt.sendKeys(nombre);
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", medioPagoNominaSecurityInpt);
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", tipoCuentaVistaInpt);
                numeroCuentaNominaInpt.sendKeys(cuenta);
                montoNominaInpt.sendKeys("1");
                JavascriptExecutor executor3 = (JavascriptExecutor) driver;
                executor3.executeScript("arguments[0].click();", guardarCambiosBtn);
                break;
            case("servipag"):
                rut = rut.replace("-", "");
                rutNominaInpt.sendKeys(rut);
                nombreNominaInpt.sendKeys(nombre);
                montoNominaInpt.sendKeys("1");
                JavascriptExecutor executor4 = (JavascriptExecutor) driver;
                executor4.executeScript("arguments[0].click();", guardarCambiosBtn);
        }
    }

    /**
     * Click agregar regitro nomina.
     */
    public void clickAgregarRegitroNomina() {
        MetodosGenericos.waitClickable(driver, agregarRegistroNominaBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", agregarRegistroNominaBtn);
    }

    /**
     * Valida comprobante pago boolean.
     *
     * @return the boolean
     */
    public boolean validaComprobantePago() {//falta terminar este metodo con el timbre de aprobado
        return pagosBtn.isDisplayed();
    }

    /**
     * Generar nueva nomina proveedores servipag.
     *
     * @param formato the formato
     */
    public void generarNuevaNominaProveedoresServipag(String formato){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", medioPagoEfectivoServipagBuscarNomina);
        MetodosGenericos.waitFor(3);
        fechaInicioServipag.sendKeys(MetodosGenericos.devolverSubSiguienteDia());
        fechaTerminoServipag.sendKeys(MetodosGenericos.devolver15DiasDespues());
        switch (formato) {
            case "txt":
                break;
            case "Excel con detalle":
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", formatoArchivoExcelConDetalleRd);
                break;
            case "Excel sin detalle":
                JavascriptExecutor executor3 = (JavascriptExecutor) driver;
                executor3.executeScript("arguments[0].click();", formatoArchivoExcelSinDetalleRd);
                break;
        }
        JavascriptExecutor executor4 = (JavascriptExecutor) driver;
        executor4.executeScript("arguments[0].click();", crearNominayContinuarBtn);
    }

    /**
     * Generar registro nomina proveedores servipag manual.
     *
     * @param rut    the rut
     * @param nombre the nombre
     * @param correo the correo
     */
    public void generarRegistroNominaProveedoresServipagManual(String rut, String nombre, String correo){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        rut = rut.replace("-", "");
        rutNominaInpt.sendKeys(rut);
        nombreNominaInpt.sendKeys(nombre);
        emailNominaInpt.sendKeys(correo);
        montoNominaInpt.sendKeys("1");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", guardarCambiosBtn);
    }

    /**
     * Generar nueva nomina proveedores servipag manual.
     */
    public void generarNuevaNominaProveedoresServipagManual() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", medioPagoEfectivoServipagBuscarNomina);
        MetodosGenericos.waitFor(3);
        fechaInicioServipag.sendKeys(MetodosGenericos.devolverSubSiguienteDia());
        fechaTerminoServipag.sendKeys(MetodosGenericos.devolver15DiasDespues());
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", tipoIngresoNominaInpt);
        JavascriptExecutor executor3 = (JavascriptExecutor) driver;
        executor3.executeScript("arguments[0].click();", crearNominaYContinuarBtnNomina);
    }

    /**
     * Ingresar buscar nominas otros pagos.
     */
    public void ingresarBuscarNominasOtrosPagos() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", buscarNominaOtrosPagosLnk);
    }

    /**
     * Buscar nomina otros pagos.
     *
     * @param medio the medio
     */
    public void buscarNominaOtrosPagos(String medio){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        switch (medio){
            case "Cuenta Corriente Cuenta Vista Vale Vista":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", buscarBtn);
                Assert.assertTrue(MetodosGenericos.visualizarObjeto(copiarAccionBtn, 5));
                Assert.assertTrue(MetodosGenericos.visualizarObjeto(eliminarAccionBtn, 5));
                break;
            case "Pago En Efectivo Servipag":
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", medioPagoEfectivoServipagBuscarNomina);
                JavascriptExecutor executor3 = (JavascriptExecutor) driver;
                executor3.executeScript("arguments[0].click();", buscarBtn);
                Assert.assertTrue(MetodosGenericos.visualizarObjeto(copiarAccionBtn, 5));
                Assert.assertTrue(MetodosGenericos.visualizarObjeto(eliminarAccionBtn, 5));
                break;
        }
    }

    /**
     * Ingresar crear nomina otros pagos.
     */
    public void ingresarCrearNominaOtrosPagos() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", crearNuevaNominaOtrosPagos);
    }

    /**
     * Generar nueva nomina otros pagos.
     *
     * @param formato the formato
     */
    public void generarNuevaNominaOtrosPagos(String formato){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        fechaAbonoInpt.sendKeys(MetodosGenericos.devolverDiaSiguiente());
        switch (formato) {
            case "txt":
                break;
            case "Excel con detalle":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", formatoArchivoExcelConDetalleRd);
                break;
            case "Excel sin detalle":
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", formatoArchivoExcelSinDetalleRd);
                break;
        }
        JavascriptExecutor executor3 = (JavascriptExecutor) driver;
        executor3.executeScript("arguments[0].click();", crearNominayContinuarBtn);
    }

    /**
     * Generar registro nomina otros pagos manual.
     *
     * @param rut    the rut
     * @param nombre the nombre
     */
    public void generarRegistroNominaOtrosPagosManual(String rut, String nombre) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        rut = rut.replace("-", "");
        rutNominaInpt.sendKeys(rut);
        nombreNominaInpt.sendKeys(nombre);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", medioPagoNominaOtrosPagosVV);
        MetodosGenericos.waitFor(3);
        montoNominaInpt.sendKeys("1");
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", guardarCambiosBtn);
    }

    /**
     * Generar registro nomina otros pagos servipag manual.
     *
     * @param rut    the rut
     * @param nombre the nombre
     */
    public void generarRegistroNominaOtrosPagosServipagManual(String rut, String nombre){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        rut = rut.replace("-", "");
        rutNominaInpt.sendKeys(rut);
        nombreNominaInpt.sendKeys(nombre);
        montoNominaInpt.sendKeys("1");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", guardarCambiosBtn);
    }

    /**
     * Pagar pagos masivos previsionales.
     */
    public void pagarPagosMasivosPrevisionales(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", pagarNominaBtn);
    }

    /**
     * Validacion nomina creada controller.
     */
    public void validacionNominaCreadaController() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("(//a[contains(text(),'Validar')])[2]")));
        MetodosGenericos.waitVisibility(driver, confirmarBtn, 5);
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", confirmarBtn);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        JavascriptExecutor executor3 = (JavascriptExecutor) driver;
        executor3.executeScript("arguments[0].click();", enviarAprobacionNominaBtn);
        MetodosGenericos.waitFor(10);
        JavascriptExecutor executor4 = (JavascriptExecutor) driver;
        executor4.executeScript("arguments[0].click();", driver.findElement(By.xpath("(//a[text()='Confirmar'])[2]")));
    }

    public void validacionNominaCargadaController() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("(//a[contains(text(),'Validar')])[1]")));
        MetodosGenericos.waitVisibility(driver, confirmarBtn, 5);
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", confirmarBtn);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        JavascriptExecutor executor3 = (JavascriptExecutor) driver;
        executor3.executeScript("arguments[0].click();", enviarAprobacionNominaBtn);
        MetodosGenericos.waitFor(10);
        JavascriptExecutor executor4 = (JavascriptExecutor) driver;
        executor4.executeScript("arguments[0].click();", driver.findElement(By.xpath("(//a[text()='Confirmar'])[2]")));
    }

    /**
     * Valida nomina pendiente pago controller boolean.
     *
     * @return the boolean
     */
    public boolean validaNominaPendientePagoController() {
        boolean textOk = false;
        MetodosGenericos.waitFor(60);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        WebElement textAutorizado = driver.findElement(By.xpath("//*[contains(text(),'Autorizada por:')]"));
        WebElement textController = driver.findElement(By.xpath("//*[contains(text(),'Angelica Olmedo Leiva (Controller)')]"));
        if(textAutorizado.isDisplayed() || textController.isDisplayed()){
            textOk= true;
        }
        return textOk;
    }

    public boolean validarPagos(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame("Arriba");
        driver.switchTo().frame("central");
        MetodosGenericos.visualizarObjeto(timbreOkPagos, 30);

        return timbreOkPagos.isDisplayed();
    }

    public boolean validarValeVista(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        driver.switchTo().frame(indexaFrm);
        driver.switchTo().frame("central");
        MetodosGenericos.visualizarObjeto(timbreOkPagos, 30);

        return timbreOkPagos.isDisplayed();
    }
}

