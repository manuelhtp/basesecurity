package page;

import driver.DriverContext;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.MetodosGenericos;

import java.util.List;

/**
 * The type Empresas.
 */
public class Empresas {
    private WebDriver driver;

    /**
     * Instantiates a new Empresas.
     */
    public Empresas() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(this.driver, this);

    }

    @FindAll({@FindBy( id = "lrut"), @FindBy( name = "frut")})
    private WebElement rutInpt;

    @FindAll({@FindBy( id = "lpass"), @FindBy( id = "clave")})
    private WebElement claveInpt;

    @FindBy(id = "btnIngresar")
    private WebElement ingresarBtn;

    @FindBy(xpath = "//a[contains(@href,'/empresas/login/salir_menu.asp')]")
    private WebElement salirBtn;

    @FindBy(xpath = "//a[contains(text(),'editar mis datos')]")
    private WebElement editarDatosBtn;

    @FindBy(xpath = "//li[contains(@class, 'saludo')]")
    private WebElement saludoTxt;

    @FindBy(id = "menu-izquierdo]")
    private WebElement menuIzquierdoDiv;

    @FindBy(id = "mainFrame")
    private WebElement frame;

    @FindBy(name = "central")
    private WebElement iframe;

    @FindBy(id = "topFrame")
    private WebElement topFrame;

    @FindBy(xpath = "//*[contains(text(),'No ver más este aviso')]")
    private WebElement modalNPSBtn;

    @FindBy(id = "other-account")
    private WebElement otraCuentaLnk;

    /**
     * Log in empresa.
     *
     * @param rut  String
     * @param pass String
     */
    public void logInEmpresa(String rut, String pass) {
        MetodosGenericos.waitFor(5);
        if (otraCuentaLnk.isDisplayed()) {
            MetodosGenericos.waitClickable(driver, otraCuentaLnk, 30);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", otraCuentaLnk);
            MetodosGenericos.waitFor(5);
        }
        rutInpt.sendKeys(rut);
        claveInpt.sendKeys(pass);
        MetodosGenericos.waitFor(5);
        MetodosGenericos.waitClickable(driver, ingresarBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", ingresarBtn);

    }

    /**
     * Valida ingreso boolean.
     *
     * @return the boolean
     */
    public boolean validaIngreso() {
        driver.switchTo().frame(0);
        MetodosGenericos.visualizarObjeto(saludoTxt, 5);
        return saludoTxt.isDisplayed();
    }

    /**
     * Log out empresa.
     */
    public void logOutEmpresa() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(0);
        MetodosGenericos.waitClickable(driver, salirBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", salirBtn);
    }

    /**
     * Click ingresar como.
     *
     * @param empresa the empresa
     */
    public void clickIngresarComo(String empresa) {
        int indice = buscaIndice(empresa);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(frame);
        driver.switchTo().frame(iframe);
        WebElement boton = driver.findElement(By.xpath("//*[@id='container']/section/div[1]/table/tbody/tr[" + indice + "]/td[3]/a"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", boton);
        MetodosGenericos.waitFor(5);
    }

    /**
     * Valida ingreso como boolean.
     *
     * @return the boolean
     */
    public boolean validaIngresoComo() {
        driver.switchTo().frame(topFrame);
        MetodosGenericos.visualizarObjeto(editarDatosBtn, 5);
        return editarDatosBtn.isDisplayed();
    }

    /**
     * Busca indice int.
     *
     * @param empresa String
     * @return indice  int
     */
    public int buscaIndice(String empresa) {
        MetodosGenericos.waitFor(15);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(frame);
        driver.switchTo().frame(iframe);
        String xpath = "//*[@id='container']/section/div[1]/table/tbody/tr[posicion]/td[1]";
        List<WebElement> trElements = driver.findElements(By.xpath("//*[@id='container']/section/div[1]/table/tbody/tr"));

        for (int i = 1; i <= trElements.size(); i++) {
            String pos = Integer.toString(i);
            String xpath_2 = xpath.replace("posicion", pos);
            String name = driver.findElement(By.xpath(xpath_2)).getText();
            if (name.equals(empresa)) {
                return i;
            }
        }
        return -1;
    }

}
