package page;

import driver.DriverContext;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import utils.MetodosGenericos;

import java.io.File;
import java.lang.reflect.Method;
import java.util.List;

/**
 * The type Op pendientes.
 */
public class OpPendientes {
    private WebDriver driver;

    /**
     * Instantiates a new Op pendientes.
     */
    public OpPendientes() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "//a[contains(text(),'Transferencias masivas')]")
    private WebElement transferenciaMasivaLkn;

    @FindBy(xpath = "//a[contains(text(),'Transacciones pendientes')]")
    private WebElement transaccionPendienteLkn;

    @FindBy(xpath = "//frame[contains(@id, 'topFrame')]")
    private WebElement topFrame;

    @FindBy(xpath = "//frame[@id='leftFrame']")
    private WebElement leftFrame;

    @FindBy(xpath = "//frame[@id='mainFrame']")
    private WebElement mainFrame;

    @FindBy(id = "Mecanismo_TC")
    private WebElement claveDinamicaRd;

    @FindBy(id = "btnPagar")
    private WebElement pagarBtn;

    @FindBy(id = "txtMatriz1")
    private WebElement coordenadaUnoInpt;

    @FindBy(id = "txtMatriz2")
    private WebElement coordenadaDosInpt;

    @FindBy(id = "txtMatriz3")
    private WebElement coordenadaTresInpt;

    @FindBy(xpath = "//a[contains(@href,'javascript:_Validar')]")
    private WebElement confirmarBtnLnk;

    @FindBy(name = "central")
    private WebElement iframe;

    @FindBy(xpath = "//a[contains(text(),'Transferencias masivas')]")
    private WebElement transaccionPendienteMasivaLkn;

    @FindBy(xpath = "//frame[@name='Indexa']")
    private WebElement indexaFrm;

    @FindBy(xpath = "//a[contains(text(),'Confirmar')]")
    private WebElement confirmarBtn;

    @FindBy(xpath = "//iframe[@name='central']")
    private WebElement centralFrm;

    @FindBy(xpath = "//input[@id='clave-dinamica-radio4' and @value='firma-electronica']")
    private WebElement feaRd;

    @FindBy(id = "btnDePago")
    private WebElement realizarOpBtn;

    @FindBy(xpath = "//a[contains(text(),'Nóminas Transferencias de Altos Montos')]")
    private WebElement transaccionPendienteAltosMontosLkn;

    @FindBy(xpath = "(//*[@name='pendientes-pago'])[1]")
    private WebElement pendientesPagoAltosMontosRd;

    @FindBy(id = "idFE")
    private WebElement feaAltosMontosRd;

    @FindBy(xpath = "//button[contains(text(),' Pagar seleccionada')]")
    private WebElement pagarSeleccionadaBtn;

    @FindBy(xpath = "//input[@id='Mecanismo_' and @value='FE']")
    private WebElement feaPagosPendientesRd;

    @FindBy(xpath = "//a[contains(text(),'Pagos masivos')]")
    private WebElement pagosMasivosLnk;

    @FindBy(xpath = "//input[@id='MedioPago' and @value='N']")
    private WebElement medioPagoCtaRd;

    @FindBy(xpath = "//input[@id='MedioPago' and @value='S']")
    private WebElement medioPagoServipagRd;

    @FindBy(xpath = "//input[@id='MedioPago' and @value='I']")
    private WebElement medioPagoInstantaneoRd;

    @FindBy(xpath = "//a[contains(@href,'javascript:Autorizar')]")
    private WebElement confirmarPagosMasivosBtnLnk;

    @FindBy(xpath = "//img[contains(@src,'timbre-ok-icon')]")
    private WebElement timbreOkImg;

    /**
     * Click trasferencias masiva.
     */
    public void clickTrasMasiva() {
        transferenciaMasivaLkn.click();
    }

    /**
     * Click trasferencias pendientes.
     */
    public void clickTrasPendientes() {
        MetodosGenericos.waitFor(10);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        MetodosGenericos.waitClickable(driver, transaccionPendienteLkn, 20);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transaccionPendienteLkn);
    }

    /**
     * Valida pendiente pago boolean.
     *
     * @return boolean boolean
     */
    public boolean validaTEFPendientePagoApoderadoUno() {
        boolean textOk = false;
        MetodosGenericos.waitFor(60);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement textPagadaPor =
                driver.findElement(By.xpath("//*[contains(text(),'Pagada por')]"));
        WebElement textUsuario =
                driver.findElement(By.xpath("//span[contains(text(),'Daniella')]"));
        if (textPagadaPor.isDisplayed() && textUsuario.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    /**
     * Valida trans realizada boolean.
     *
     * @return boolean boolean
     */
    public boolean validaTransRealizada() {
        MetodosGenericos.waitFor(30);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement timbreOK = driver.findElement(By.xpath("//img[@src='img/timbre_trans_realizada.jpg']"));
        return timbreOK.isDisplayed();
    }

    /**
     * Pagar trans pendientes banco cuenta Preestablecida o No Preestablecida.
     *
     * @param cuenta String cuenta
     * @param tipo   the tipo
     */
    public void pagarTransPendientesBanco(String cuenta, String tipo) {
        int indice = 0;
        switch (tipo) {
            case "np":
                indice = buscaIndiceBancoNP(cuenta);
                break;
            case "p":
                indice = buscaIndiceBancoP(cuenta);
                break;
        }
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        if (tipo.equals("p")) {
            WebElement uno = driver.findElement(By.xpath("/html/body/div/form[2]/div[2]/div/div[2]/div[" + indice + "]/div[1]/div/input"));
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", uno);
        } else {
            WebElement dos = driver.findElement(By.xpath("(//input[@class='input-checkbox-radio'])[" + indice + "]"));
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", dos);
        }
        MetodosGenericos.waitFor(6);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", feaPagosPendientesRd);
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", pagarBtn);
        MetodosGenericos.waitClickable(driver, confirmarBtnLnk, 5);
        JavascriptExecutor xecutor = (JavascriptExecutor) driver;
        xecutor.executeScript("arguments[0].click();", confirmarBtnLnk);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
    }

    /**
     * Busca indice cuenta de banco.
     *
     * @param cuenta String cuenta
     * @return int i
     */
    public int buscaIndiceBancoNP(String cuenta) {
        MetodosGenericos.waitFor(10);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        String xpath = "//*[@id='Formulario2']/div[1]/div/div[2]/div[posicion]/div[5]";
        List<WebElement> trElements = driver.findElements(By.cssSelector("#Formulario2>div:nth-child(1)>div>div.table>div"));
        for (int i = 2; i <= trElements.size(); i++) {
            String pos = Integer.toString(i);
            String xpath_2 = xpath.replace("posicion", pos);
            String name = driver.findElement(By.xpath(xpath_2)).getText();
            if (name.equals(cuenta)) {
                return --i;

            }
        }
        return -1;
    }

    /**
     * Click tras pendientes masivas.
     */
    public void clickTrasPendientesMasivas() {
        MetodosGenericos.waitFor(6);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(leftFrame);
        MetodosGenericos.waitClickable(driver, transaccionPendienteMasivaLkn, 20);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transaccionPendienteMasivaLkn);
    }

    /**
     * Pagar trans masiva nomina.
     */
    public void pagarTransMasivaNomina() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        MetodosGenericos.waitFor(5);
        WebElement uno = driver.findElement(By.xpath("(//*[contains(text(),'Pagar') and @class='btn btn-mini'])[1]"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", uno);
        MetodosGenericos.waitFor(5);
        WebElement dos = driver.findElement(By.xpath("(//a[contains(text(),'Confirmar')])[1]"));
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", dos);

    }

    /**
     * Autorizar nomina masiva.
     */
    public void autorizarNominaMasiva() {
        MetodosGenericos.waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.waitClickable(driver, feaRd, 15);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", feaRd);
        MetodosGenericos.waitClickable(driver, realizarOpBtn, 5);
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", realizarOpBtn);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(15);
    }

    /**
     * Click tras pendientes altos montos nominas.
     */
    public void clickTrasPendientesAltosMontosNomina() {
       // driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        MetodosGenericos.waitClickable(driver, transaccionPendienteAltosMontosLkn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transaccionPendienteAltosMontosLkn);
    }

    /**
     * Pagar trans pendientes altos montos nominas.
     */
    public void pagarTransPendientesAltosMontosNomina() {
        MetodosGenericos.waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        MetodosGenericos.waitClickable(driver, pendientesPagoAltosMontosRd, 10);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", pendientesPagoAltosMontosRd);
        MetodosGenericos.waitFor(3);
        MetodosGenericos.waitClickable(driver, feaAltosMontosRd, 8);
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", feaAltosMontosRd);
        MetodosGenericos.waitFor(5);
        MetodosGenericos.waitClickable(driver, pagarSeleccionadaBtn, 8);
        JavascriptExecutor xecutor = (JavascriptExecutor) driver;
        xecutor.executeScript("arguments[0].click();", pagarSeleccionadaBtn);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
    }

    /**
     * Busca indice cuenta de banco.
     *
     * @param cuenta String cuenta
     * @return int i
     */
    public int buscaIndiceBancoAltosMontosP(String cuenta) {
        MetodosGenericos.waitFor(10);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        String xpath = "//*[@id='Formulario2']/div[3]/div/div[2]/div[posicion]/div[5]";
        List<WebElement> trElements = driver.findElements(By.cssSelector("#Formulario2>div:nth-child(3)>div>div.table>div"));
        for (int i = 2; i <= trElements.size(); i++) {
            String pos = Integer.toString(i);
            String xpath_2 = xpath.replace("posicion", pos);
            String name = driver.findElement(By.xpath(xpath_2)).getText();
            if (name.equals(cuenta)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Pagar trans pendientes banco altos montos np.
     *
     * @param cuenta the cuenta
     * @param tipo   the tipo
     * @return
     */
    public void pagarTransPendientesBancoAltosMontos(String cuenta, String tipo) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        int indice = 0;
        switch (tipo) {
            case "np":
                indice = buscaIndiceBancoAltosMontosNP(cuenta);
                break;
            case "p":
                indice = buscaIndiceBancoAltosMontosP(cuenta);
                break;
        }
        if (tipo.equals("np")) {
            WebElement altosMontosNP = driver.findElement(By.xpath
                    ("//h4[contains(text(),'Transferencias Preestablecidas Altos Montos')]"));
            if (altosMontosNP.isDisplayed()) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame("mainFrame");
                WebElement uno = driver.findElement(By.xpath("/html/body/div/form[2]/div[4]/div/div[2]/div[" + indice + "]/div[1]/div/input"));
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", uno);
                MetodosGenericos.waitClickable(driver, feaPagosPendientesRd, 5);
                JavascriptExecutor executo = (JavascriptExecutor) driver;
                executo.executeScript("arguments[0].click();", feaPagosPendientesRd);
            } else {
                System.out.println("Transferencias No Preestablecidas Altos Montos, NO DISPONIBLES");
            }
        } else if (tipo.equals("p")) {
            WebElement altosMontosP = driver.findElement(By.xpath
                    ("//h4[contains(text(),'Transferencias Preestablecidas Altos Montos')]"));
            if (altosMontosP.isDisplayed()) {
                driver.switchTo().defaultContent();
                driver.switchTo().frame("mainFrame");
                WebElement dos = driver.findElement(By.xpath("/html/body/div/form[2]/div[3]/div/div[2]/div[" + indice + "]/div[1]/div/input"));
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", dos);
                MetodosGenericos.waitClickable(driver, feaPagosPendientesRd, 5);
                JavascriptExecutor executo = (JavascriptExecutor) driver;
                executo.executeScript("arguments[0].click();", feaPagosPendientesRd);
            } else {
                System.out.println("Transferencias Preestablecidas Altos Montos, NO DISPONIBLES");
            }
        }
        MetodosGenericos.waitClickable(driver, pagarBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", pagarBtn);
        MetodosGenericos.waitVisibility(driver, confirmarBtnLnk, 10);
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", confirmarBtnLnk);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
    }

    /**
     * Busca indice cuenta de banco.
     *
     * @param cuenta String cuenta
     * @return int i
     */
    public int buscaIndiceBancoAltosMontosNP(String cuenta) {
        MetodosGenericos.waitFor(10);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        String xpath = "//*[@id='Formulario2']/div[4]/div/div[2]/div[posicion]/div[5]";
        List<WebElement> trElements = driver.findElements(By.cssSelector("#Formulario2>div:nth-child(4)>div>div.table>div"));
        for (int i = 2; i <= trElements.size(); i++) {
            String pos = Integer.toString(i);
            String xpath_2 = xpath.replace("posicion", pos);
            String name = driver.findElement(By.xpath(xpath_2)).getText();
            if (name.equals(cuenta)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Pagar trans pendientes cclv.
     *
     * @param tipo the tipo
     */
    public void pagarTransPendientesCCLV(String tipo) {
        MetodosGenericos.waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement cclv = driver.findElement(By.xpath
                ("//h4[contains(text(),'Transferencia CCLV')]"));
        if (cclv.isDisplayed()) {
            int indice = buscaIndiceBancoCCLV(tipo);
            driver.switchTo().defaultContent();
            driver.switchTo().frame(mainFrame);
            WebElement uno = driver.findElement(By.xpath("/html/body/div/form[2]/div[5]/div/div[2]/div[" + indice + "]/div[1]/div/input"));
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", uno);
            MetodosGenericos.waitClickable(driver, feaPagosPendientesRd, 5);
            JavascriptExecutor executo = (JavascriptExecutor) driver;
            executo.executeScript("arguments[0].click();", feaPagosPendientesRd);
        } else {
            System.out.println("Transferencia CCLV, NO DISPONIBLES");
        }
        MetodosGenericos.waitFor(3);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        MetodosGenericos.waitClickable(driver, pagarBtn, 5);
        executor.executeScript("arguments[0].click();", pagarBtn);
        MetodosGenericos.waitFor(3);
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", confirmarBtnLnk);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
    }

    /**
     * Busca indice cuenta de banco.
     *
     * @param tipo the tipo
     * @return int i
     */
    public int buscaIndiceBancoCCLV(String tipo) {
        MetodosGenericos.waitFor(10);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        String xpath = "//*[@id='Formulario2']/div[5]/div/div[2]/div[posicion]/div[5]";
        List<WebElement> trElements = driver.findElements(By.cssSelector("#Formulario2>div:nth-child(5)>div>div.table>div"));
        for (int i = 2; i <= trElements.size(); i++) {
            String pos = Integer.toString(i);
            String xpath_2 = xpath.replace("posicion", pos);
            String name = driver.findElement(By.xpath(xpath_2)).getText();
            if (name.equals(tipo)) {
                return i;
            }
        }
        return -1;
    }


    /**
     * Valida pendiente pago nomina boolean.
     *
     * @return the boolean
     */
    public boolean validaPendientePagoNomina() {
        MetodosGenericos.waitFor(60);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement textOk = driver.findElement(By.xpath("//img[@src='/security/estilos_generales/img/timbre-ok-pendiente.png']"));
        return textOk.isDisplayed();
    }

    /**
     * Click pagos masivos.
     */
    public void clickPagosMasivos() {
        MetodosGenericos.waitFor(3);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(leftFrame);
        MetodosGenericos.waitClickable(driver, pagosMasivosLnk, 15);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", pagosMasivosLnk);
    }

    /**
     * Pagar nomina pendinte.
     *
     * @param tipo   the tipo
     * @param motivo the motivo
     */
    public void pagarNominaPendinte(String tipo, String motivo) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        MetodosGenericos.waitFor(5);
        switch (tipo) {
            case "cuentaCorriente":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", medioPagoCtaRd);
                break;
            case "servipag":
                JavascriptExecutor xecutor = (JavascriptExecutor) driver;
                xecutor.executeScript("arguments[0].click();", medioPagoServipagRd);
                break;
            case "instantaneo":
                JavascriptExecutor executo = (JavascriptExecutor) driver;
                executo.executeScript("arguments[0].click();", medioPagoInstantaneoRd);
                break;
            default:
                break;
        }
        clickNominaPagoMasivo(tipo, motivo);
        Screen screen = new Screen();
        File file = new File("src/main/java/recursos/");
        Pattern confirmarBtn = new Pattern(file + "/confirmarBtn.PNG");
        try {
            screen.click(confirmarBtn);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.waitVisibility(driver, feaRd, 20);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", feaRd);
        MetodosGenericos.waitFor(5);
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", realizarOpBtn);
        MetodosGenericos.aprobarFirmaSikuli();
    }

    /**
     * Busca indice motivo int.
     *
     * @param tipo   the tipo
     * @param motivo the motivo
     * @return the int
     */
    public int buscaIndiceMotivo(String tipo, String motivo) {
        MetodosGenericos.waitFor(10);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        String xpath = null;
        if (tipo.equals("servipag")) {
            xpath = "//*[@id='cuadro-buscar']/table/tbody/tr[posicion]/td[3]";
        } else {
            xpath = "//*[@id='cuadro-buscar']/table/tbody/tr[posicion]/td[2]";
        }
        List<WebElement> trElements = driver.findElements(By.cssSelector("#cuadro-buscar>table>tbody>tr"));
        for (int i = 2; i <= trElements.size(); i++) {
            String pos = Integer.toString(i);

            String xpath_2 = xpath.replace("posicion", pos);
            String name = driver.findElement(By.xpath(xpath_2)).getText();
            if (name.equals(motivo)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Click nomina pago masivo.
     *
     * @param tipo   the tipo
     * @param motivo the motivo
     */
    public void clickNominaPagoMasivo(String tipo, String motivo) {
        int indice = buscaIndiceMotivo(tipo, motivo);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        if (tipo.equals("servipag")) {
           WebElement uno = driver.findElement(By.xpath("//*[@id='cuadro-buscar']/table/tbody/tr[" + indice + "]/td[7]/a"));
            JavascriptExecutor executor = (JavascriptExecutor) driver;
           executor.executeScript("arguments[0].click();", uno);
        } else {
            WebElement dos = driver.findElement(By.xpath("//*[@id='cuadro-buscar']/table/tbody/tr[" + indice + "]/td[6]/a"));
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", dos);
        }
    }

    /**
     * Busca indice banco p int.
     *
     * @param cuenta the cuenta
     * @return the int
     */
    public int buscaIndiceBancoP(String cuenta) {
        MetodosGenericos.waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        String xpath = "//*[@id='Formulario2']/div[2]/div/div[2]/div[posicion]/div[5]";
        List<WebElement> trElements = driver.findElements(By.cssSelector("#Formulario2>div:nth-child(2)>div>div.table>div"));
        for (int i = 2; i <= trElements.size(); i++) {
            String pos = Integer.toString(i);
            String xpath_2 = xpath.replace("posicion", pos);
            String name = driver.findElement(By.xpath(xpath_2)).getText();
            if (name.equals(cuenta)) {
                return i;
            }
        }
        return -1;
    }

    public boolean validaPagoMasivoApoderadoUno() {
        boolean textOk = false;
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        driver.switchTo().frame(centralFrm);
        WebElement textPagadaPor =
                driver.findElement(By.xpath("//*[contains(text(),'Pagada por')]"));
        WebElement textUsuario =
                driver.findElement(By.xpath("//*[contains(text(),'Daniella')]"));
        if (textPagadaPor.isDisplayed() && textUsuario.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    public boolean validaPagoMasivoApoderadoDos() {//falta modificar los textos del xpath
        boolean textOk = false;
        MetodosGenericos.waitFor(8);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        driver.switchTo().frame(centralFrm);
        WebElement textPagadaPor =
                driver.findElement(By.xpath("//*[contains(text(),'Pagada por')]"));
        WebElement textUsuario =
                driver.findElement(By.xpath("//*[contains(text(),'Javier')]"));
        if (textPagadaPor.isDisplayed() && textUsuario.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    public boolean validaTEFPendientePagoApoderadoDos() {
        boolean textOk = false;
        MetodosGenericos.waitFor(60);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement textPagadaPor =
                driver.findElement(By.xpath("//*[contains(text(),'Pagada por')]"));
        WebElement textUsuario =
                driver.findElement(By.xpath("//span[contains(text(),'Javier')]"));
        if (textPagadaPor.isDisplayed() && textUsuario.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    //No se pueden generar CCLV de momento
    public boolean validaPendientePagoNominaApoderado2() {
        MetodosGenericos.waitFor(60);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement textOk = driver.findElement(By.xpath("//img[@src='/security/estilos_generales/img/timbre-ok-icon.png']"));
        return textOk.isDisplayed();
    }

    public void pagarNominaPendienteApoderadoDos(String tipo, String motivo) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        MetodosGenericos.waitFor(5);
        switch (tipo) {
            case "cuentaCorriente":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", medioPagoCtaRd);
                break;
            case "servipag":
                JavascriptExecutor xecutor = (JavascriptExecutor) driver;
                xecutor.executeScript("arguments[0].click();", medioPagoServipagRd);
                break;
            case "instantaneo":
                JavascriptExecutor executo = (JavascriptExecutor) driver;
                executo.executeScript("arguments[0].click();", medioPagoInstantaneoRd);
                break;
            default:
                break;
        }
        clickNominaPagoMasivo(tipo, motivo);
        MetodosGenericos.waitFor(5);
        Screen screen = new Screen();
        File file = new File("src/main/java/recursos/");
        Pattern confirmarBtn = new Pattern(file + "/confirmarBtn2.PNG");
        try {
            screen.click(confirmarBtn);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
        MetodosGenericos.waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.waitVisibility(driver, feaRd, 20);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", feaRd);
        MetodosGenericos.waitFor(5);
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", realizarOpBtn);
        MetodosGenericos.waitFor(10);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
    }
    public boolean validaAltosMontosApoderadoUno() {
        boolean textOk = false;
        MetodosGenericos.waitFor(30);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement textPagadaPor =
                driver.findElement(By.xpath("//*[contains(text(),'Pagada por')]"));
        WebElement textUsuario =
                driver.findElement(By.xpath("//p[contains(text(),'Daniella')]"));
        if (textPagadaPor.isDisplayed() && textUsuario.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    public boolean validarTEFMasivasApoderadoDos() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.visualizarObjeto(timbreOkImg, 60);
        return timbreOkImg.isDisplayed();
    }

    public boolean validaTEFAltosMontosNominasApoderadoDos() {
        boolean textOk = false;
        MetodosGenericos.waitFor(120);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement textPagadaPor =
                driver.findElement(By.xpath("//*[contains(text(),'Pagada por')]"));
        WebElement textUsuario =
                driver.findElement(By.xpath("//div[contains(text(),'Javier')]"));
        if (textPagadaPor.isDisplayed() && textUsuario.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

}

