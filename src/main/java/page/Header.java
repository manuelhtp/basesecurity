package page;

import driver.DriverContext;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.MetodosGenericos;


/**
 * The type Header.
 */
public class Header {
    private WebDriver driver;

    /**
     * Instantiates a new Header.
     */
    public Header() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(id = "homeEmpresas")
    private WebElement empresaBtn;

    @FindBy(id = "homePersonas")
    private WebElement personasBtn;

    @FindBy(xpath = "(//*[@class='btn-login'])[2]")
    private WebElement ingresoClientesBtn;

    @FindBy(xpath = "//a[contains(text(),'editar mis datos')]")
    private WebElement editarDatosBtn;

    @FindBy(id = "topFrame")
    private WebElement topFrame;

    /**
     * Click empresas.
     */
    public void clickEmpresas() {
        MetodosGenericos.waitFor(5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", empresaBtn);
        MetodosGenericos.waitFor(5);
    }

    /**
     * Click personas.
     */
    public void clickPersonas() {
        personasBtn.click();
    }

    /**
     * Click ingreso clientes.
     */
    public void clickIngresoClientes() {
        MetodosGenericos.waitClickable(driver, ingresoClientesBtn, 10);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", ingresoClientesBtn);

    }

    /**
     * Valida salir boolean.
     *
     * @return boolean
     */
    public boolean validaSalir() {
        MetodosGenericos.visualizarObjeto(ingresoClientesBtn, 5);
        return ingresoClientesBtn.isDisplayed();
    }

    /**
     * Click editar datos.
     */
    public void clickEditarDatos() {
        MetodosGenericos.visualizarObjeto(editarDatosBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", editarDatosBtn);
    }

    /**
     * Editar contacto.
     */
    public void editarContacto() {
        driver.switchTo().frame("topFrame");
        clickEditarDatos();
    }
}