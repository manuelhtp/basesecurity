package page;

import driver.DriverContext;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import utils.MetodosGenericos;

import java.util.List;

/**
 * The type Editar datos.
 */
public class EditarDatos {
    private WebDriver driver;

    /**
     * Instantiates a new Editar datos.
     */
    public EditarDatos() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "(//a[@class='button'])[2]")
    private WebElement editCorrespondeciaBtn;

    @FindBy(xpath = "(//a[@class='button'])[1]")
    private WebElement editContactoBtn;

    @FindBy(id = "mainFrame")
    private WebElement frame;

    @FindBy(name = "central")
    private WebElement iframe;

    @FindBy(id = "topFrame")
    private WebElement topFrame;

    @FindBy(name = "calle_")
    private WebElement calleInpt;

    @FindBy(name = "nume_")
    private WebElement numeroInpt;

    @FindBy(name = "ofi_")
    private WebElement numeroOfInpt;

    @FindBy(name = "region_Comercial")
    private WebElement regionSel;

    @FindBy(name = "ciudad_Comercial")
    private WebElement ciudadSel;

    @FindBy(name = "comuna_Comercial")
    private WebElement comunaSel;

    @FindBy(name = "num_leg_")
    private WebElement telUnoInpt;

    @FindBy(name = "num_com_")
    private WebElement telDosInpt;

    @FindBy(xpath = "//input[@value='FE'] ")
    private WebElement firmaElectronicaRd;

    @FindBy(xpath = "//*[@class='buttonround1']")
    private WebElement modifDirTelBtn;

    @FindBy(xpath = "//*[contains(text(),'Conecte su Firma Electrónica Avanzada')]")
    private WebElement firmaElectronicaTxt;

    @FindBy(xpath = "//*[@class='buttonround2']")
    private WebElement continuarBtn;

    @FindBy(xpath = "//a[contains(text(),'editar mis datos')]")
    private WebElement editarMisDatosBtn;

    @FindBy(xpath = "//a[contains(@href,'javascript:Valida()')]")
    private WebElement guardarCambiosBtnLnk;

    @FindBy(xpath = "//span[text()='Guardar cambios']")
    private WebElement guardarCambioBtn;

    @FindBy(xpath = "//img[contains(@src,'transaccion_realizada')]")
    private WebElement modificarDatosOkImg;

    @FindBy(xpath = "//a[@href='agregar_contacto_de_la_empresa.asp']")
    private WebElement agregarEmpresaBtn;

    @FindBy(name = "nombrecontacto")
    private WebElement nombreEmpresaInpt;

    @FindBy(name = "telefonocontacto")
    private WebElement telefonoEmpresaInpt;

    @FindBy(name = "celularcontacto")
    private WebElement celularEmpresaInpt;

    @FindBy(name = "email_1")
    private WebElement emailEmpresaInpt;

    @FindBy(name = "email_2")
    private WebElement emailConfirmEmpresaInpt;

    @FindBy(xpath = "//a[@href='javascript:AgregarDatosContacto();']")
    private WebElement guardarNuevaEmpresa;

    @FindBy(xpath = "(//a[text()='Borrar'])[1]")
    private WebElement borrarEmpresaLnk;

    @FindBy(xpath = "//a[@href='javascript:Borrar();']")
    private WebElement borrarElContactoBtn;

    @FindBy(xpath = "(//a[text()='Editar'])[1]")
    private WebElement editarEmpresaLnk;

    @FindBy(xpath = "//a[@href='javascript:ModificarDatosContacto();']")
    private WebElement modificarDatosEmpresaBtn;

    @FindBy(xpath = "//span[contains(text(), 'Cambiar forma de despacho')]")
    private WebElement cambiarFormaDespachoBtn;

    @FindBy(xpath = "(//input[@id='RadioGroup'])[1]")
    private WebElement deseoRecibirEmail;

    @FindBy(xpath = "(//input[@id='RadioGroup'])[2]")
    private WebElement consultarEnLinea;

    @FindBy(xpath = "(//input[@id='RadioGroup'])[3]")
    private WebElement deseoRecibirCorreoFisico;

    @FindBy(xpath = "//input[@id='chkProductos']")
    private WebElement empresaCheck;

    /**
     * Click edit correspondecia.
     */
    public void clickEditCorrespondecia() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        MetodosGenericos.visualizarObjeto(editCorrespondeciaBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", editCorrespondeciaBtn);
    }

    /**
     * Click edit contacto.
     */
    public void clickEditContacto() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        MetodosGenericos.visualizarObjeto(editContactoBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", editContactoBtn);
    }

    /**
     * Modif datos contacto.
     */
    public void modifDatosContacto() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        MetodosGenericos.visualizarObjeto(modifDirTelBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", modifDirTelBtn);
        MetodosGenericos.waitFor(10);
        calleInpt.clear();
        calleInpt.sendKeys(MetodosGenericos.devuelveTextoMinusculas());
        numeroInpt.clear();
        numeroInpt.sendKeys(MetodosGenericos.devuelveNumeroTelefono());
        numeroOfInpt.clear();
        numeroOfInpt.sendKeys(MetodosGenericos.devuelveNumeroDosDigitos());
        Select sel = new Select(regionSel);
        sel.selectByVisibleText("REGION METROPOLITANA DE SANTIAGO");
        Select sel1 = new Select(ciudadSel);
        sel1.selectByVisibleText("SANTIAGO");
        Select sel2 = new Select(comunaSel);
        sel2.selectByVisibleText("SANTIAGO");
        telUnoInpt.sendKeys(MetodosGenericos.devuelveNumeroTelefono());
        telDosInpt.sendKeys(MetodosGenericos.devuelveNumeroTelefono());
        MetodosGenericos.waitVisibility(driver, firmaElectronicaRd, 10);
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", firmaElectronicaRd);
        JavascriptExecutor executor3 = (JavascriptExecutor) driver;
        executor3.executeScript("arguments[0].click();", continuarBtn);
    }

    /**
     * Valida solicita fe boolean.
     *
     * @return the boolean
     */
    public boolean validaSolicitaFE() {
        MetodosGenericos.visualizarObjeto(firmaElectronicaTxt, 5);
        return firmaElectronicaTxt.isDisplayed();
    }

    public void modificarDatosContactoFEA(String calle, String numero, String oficina, String region, String ciudad,
                                          String comuna, String telefono1, String telefono2) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        MetodosGenericos.visualizarObjeto(modifDirTelBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", modifDirTelBtn);
        MetodosGenericos.waitFor(10);
        calleInpt.clear();
        calleInpt.sendKeys(calle);
        numeroInpt.clear();
        numeroInpt.sendKeys(numero);
        numeroOfInpt.clear();
        numeroOfInpt.sendKeys(oficina);
        Select sel = new Select(regionSel);
        sel.selectByVisibleText(region);
        Select sel1 = new Select(ciudadSel);
        sel1.selectByVisibleText(ciudad);
        Select sel2 = new Select(comunaSel);
        sel2.selectByVisibleText(comuna);
        telUnoInpt.sendKeys(telefono1);
        telDosInpt.sendKeys(telefono2);
        MetodosGenericos.waitVisibility(driver, firmaElectronicaRd, 15);
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", firmaElectronicaRd);
        JavascriptExecutor executor3 = (JavascriptExecutor) driver;
        executor3.executeScript("arguments[0].click();", continuarBtn);
        MetodosGenericos.waitFor(5);
        JavascriptExecutor executor4 = (JavascriptExecutor) driver;
        executor4.executeScript("arguments[0].click();", guardarCambiosBtnLnk);
        MetodosGenericos.aprobarFirmaSikuli();
    }

    public void clickEditarMisDatos(){
        driver.switchTo().frame("topFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", editarMisDatosBtn);
    }

    public void guardarCambiosModificarDatos(){
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(2);
        MetodosGenericos.ingresarPinMonoapoderadoFEA();
    }

    public boolean validarModificarDatos(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        MetodosGenericos.visualizarObjeto(modificarDatosOkImg, 20);

        return modificarDatosOkImg.isDisplayed();
    }

    public void agregarEmpresa(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", agregarEmpresaBtn);
    }

    public void agregarDatosEmpresa( String telefono, String celular, String email){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        nombreEmpresaInpt.sendKeys(MetodosGenericos.devuelveTextoMinusculas());
        telefonoEmpresaInpt.sendKeys(telefono);
        celularEmpresaInpt.sendKeys(celular);
        emailEmpresaInpt.sendKeys(email);
        emailConfirmEmpresaInpt.sendKeys(email);

    }

    public void borrarEmpresa(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", borrarEmpresaLnk);
    }

    public void borrarContactoEmpresa(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", borrarElContactoBtn);
    }

    public void editarEmpresa(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", editarEmpresaLnk);
    }

    public void modificarDatosEmpresa(String telefono, String celular, String email){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        nombreEmpresaInpt.clear();
        telefonoEmpresaInpt.clear();
        celularEmpresaInpt.clear();
        emailEmpresaInpt.clear();
        MetodosGenericos.waitFor(2);
        nombreEmpresaInpt.sendKeys(MetodosGenericos.devuelveTextoMinusculas());
        telefonoEmpresaInpt.sendKeys(telefono);
        celularEmpresaInpt.sendKeys(celular);
        emailEmpresaInpt.sendKeys(email);
        emailConfirmEmpresaInpt.sendKeys(email);
    }

    public String capturaNombre() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
       String nombre = nombreEmpresaInpt.getText();
        return nombre;
    }

    public void clickGuardarEmpresa(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", guardarNuevaEmpresa);
    }

    public boolean capturaNombreDos(String nombreUno) {
        boolean nomb = false;
        List<WebElement> trElements = driver.findElements(By.xpath("/html/body/form/div/div[8]/div/div/div/div/div[2]/table/tbody/tr"));
        for (int i = 2; i <= trElements.size(); i++) {
            if(nombreUno.equals(nombreUno)){
                nomb = true;
            }
        }
        return nomb;
    }

    public void clickModificarEmpresa(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", modificarDatosEmpresaBtn);
    }

    public void cambiarFormaDespacho(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", cambiarFormaDespachoBtn);
    }

    public void despachoCorrespondencia(String formato){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        switch (formato){
            case "recibirlas email":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", deseoRecibirEmail);
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", empresaCheck);
                MetodosGenericos.waitVisibility(driver, firmaElectronicaRd, 10);
                JavascriptExecutor executor3 = (JavascriptExecutor) driver;
                executor3.executeScript("arguments[0].click();", firmaElectronicaRd);
                JavascriptExecutor executor4 = (JavascriptExecutor) driver;
                executor4.executeScript("arguments[0].click();", continuarBtn);
                JavascriptExecutor executor5 = (JavascriptExecutor) driver;
                executor5.executeScript("arguments[0].click();", guardarCambiosBtnLnk);
                break;
            case "consultar en linea":
                JavascriptExecutor executor6 = (JavascriptExecutor) driver;
                executor6.executeScript("arguments[0].click();", consultarEnLinea);
                MetodosGenericos.waitClickable(driver, firmaElectronicaRd, 10);
                JavascriptExecutor executor7 = (JavascriptExecutor) driver;
                executor7.executeScript("arguments[0].click();", firmaElectronicaRd);
                JavascriptExecutor executor8 = (JavascriptExecutor) driver;
                executor8.executeScript("arguments[0].click();", continuarBtn);
                JavascriptExecutor executor9 = (JavascriptExecutor) driver;
                executor9.executeScript("arguments[0].click();", guardarCambiosBtnLnk);
                break;
            case "recibirlas correo fisico":
                JavascriptExecutor executor10 = (JavascriptExecutor) driver;
                executor10.executeScript("arguments[0].click();", deseoRecibirCorreoFisico);
                MetodosGenericos.waitClickable(driver, firmaElectronicaRd, 10);
                JavascriptExecutor executor11 = (JavascriptExecutor) driver;
                executor11.executeScript("arguments[0].click();", firmaElectronicaRd);
                JavascriptExecutor executor12 = (JavascriptExecutor) driver;
                executor12.executeScript("arguments[0].click();", continuarBtn);
                JavascriptExecutor executor13 = (JavascriptExecutor) driver;
                executor13.executeScript("arguments[0].click();", guardarCambiosBtnLnk);
                break;
        }
    }

    public void modificarDespachoCorrespondencia(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        MetodosGenericos.waitFor(2);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(10);
    }


    public boolean validaModificarDespacho() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        WebElement textOk = driver.findElement(By.xpath("//*[text()='Los cambios han sido realizados.']"));
        return textOk.isDisplayed();
    }
}
