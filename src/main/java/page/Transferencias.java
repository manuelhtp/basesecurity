package page;

import driver.DriverContext;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import utils.MetodosGenericos;

import java.awt.*;
import java.io.File;

/**
 * The type Transferencias.
 */
public class Transferencias {
    private WebDriver driver;

    /**
     * Instantiates a new Transferencias.
     */
    public Transferencias() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(id = "topFrame")
    private WebElement topFrame;

    @FindBy(xpath = "//frame[@id='leftFrame']")
    private WebElement leftFrame;

    @FindBy(xpath = "//frame[@id='mainFrame']")
    private WebElement mainFrame;

    @FindBy(xpath = "//iframe[@name='central']")
    private WebElement centralFrm;

    @FindBy(xpath = "//li[@class='estilo2']//a[contains(text(), 'Transferencias')]")
    private WebElement transferenciasLnk;

    @FindBy(xpath = "//div[@class='PanelClosed']//a[contains(text(), 'Transferencias Masivas')]")
    private WebElement transferenciasMasivasLnk;

    @FindBy(xpath = "//div[@class='PanelClosed']//a[contains(text(), 'Transferir altos montos')]")
    private WebElement transferirAltosMontosLnk;

    @FindBy(xpath = "//div[@class='PanelClosed']//a[contains(text(), 'Transferir en línea a terceros')]")
    private WebElement transferirLineaTercerosLnk;

    //Transferencias a terceros en linea
    @FindBy(xpath = "//a[@href='#preestablecidas-tab']")
    private WebElement preestablecidasTercerosLinea;

    //Transferencias de altos montos
    @FindBy(id = "tab-bg:0")
    private WebElement preestablecidasAltosMontos;

    @FindBy(id = "tab-bg:2")
    private WebElement nominaDeTransferencia;

    @FindBy(id = "ctactesNuevo")
    private WebElement cuentaOrigenNominaTransferenciaSel;

    @FindBy(id = "opTexto")
    private WebElement tipoNominaTxt;

    @FindBy(id = "opExcel")
    private WebElement tipoNominaExcel;

    @FindBy(id = "subir")
    private WebElement btnSubirNomina;

    @FindBy(xpath = "//div[@class='PanelClosed']//a[contains(text(), 'Transferir CCLV')]")
    private WebElement transferirCCLVLnk;

    //Trans Altos Montos Datos Cuentas Preestablecidas

    @FindBy(id = "ctactes")
    private WebElement CuentaSel;

    @FindBy(id = "cbDestino")
    private WebElement nombreDestinatarioSel;

    @FindBy(id = "txtEmail")
    private WebElement email;

    @FindBy(id = "txtReferencia")
    private WebElement referencia;

    @FindBy(id = "txtMonto")
    private WebElement monto;

    //Cuentas No Preestablecidas
    @FindBy(id = "Select2")
    private WebElement cuentaGuardada;

    @FindBy(id = "txtNumeroCuenta")
    private WebElement numeroCuentaTxt;

    @FindBy(id = "cbBanco")
    private WebElement bancoDestinoSel;

    @FindBy(id = "txtNombre")
    private WebElement nombreDestiNoPreInpt;

    @FindBy(id = "txtRut")
    private WebElement rutDestinatario;

    @FindBy(xpath = "//a[contains(@href, 'javascript:_Validar')]")
    private WebElement confirmarBtnAltosMontos;

    @FindBy(id = "GuardarDestinatario")
    private WebElement guardarDestinatarioBtn;

    //Transferir a linea a terceros

    @FindBy(id = "cuentas")
    private WebElement cuentaOrigelLTSel;

    @FindBy(id = "destinatario-nombre")
    private WebElement nombreDestLineaTerceros;

    @FindBy(id = "destinatario-rut")
    private WebElement rutDestLineaTerceros;

    @FindBy(id = "destinatario-cuenta")
    private WebElement numeroCuentaLineaTerceros;

    @FindBy(id = "destinatario-banco")
    private WebElement bancoDestLineaTerceros;

    @FindBy(xpath = "//form[@class='no-preestablecidas secure-form form-horizontal']//input[@id='guardar-como']")
    private WebElement guardarCuentaLineaTerceros;

    @FindBy(id = "clave-dinamica-radio4")
    private WebElement firmaElectronicaLineaTerceros;

    //Tipos de cuenta
    @FindBy(xpath = "//input[@data-nombre='Cuenta Ahorro']")
    private WebElement tipoCuentaAhorroRd;

    @FindBy(xpath = "//input[@data-nombre='Cuenta Corriente']")
    private WebElement tipoCuentaCorrienteRd;

    @FindBy(xpath = "//input[@data-nombre='Cuenta Vista']")
    private WebElement tipoCuentaVistaRd;

    @FindBy(id = "modal-horalimite")
    private WebElement modalDiv;

    @FindBy(xpath = "//button[@class='btn btn-default']")
    private WebElement aceptarModalBtn;

    @FindBy(id = "ctactes")
    private WebElement cuentaOrigenSel;

    @FindBy(id = "tipoCamara")
    private WebElement camaraSel;

    @FindBy(id = "tipoFondo")
    private WebElement tipoFondoSel;

    @FindBy(id = "tipoSaldo")
    private WebElement tipoSaldoSel;

    @FindBy(id = "txtMonto")
    private WebElement montoInpt;

    @FindBy(xpath = "//*[contains(text(),'Continuar')]")
    private WebElement continuarBtn;

    @FindBy(xpath = "//a[contains(@href,'javascript:validaFormNoEstablecidas();')]")
    private WebElement continuarBtnLnk;

    @FindBy(xpath = "//a[contains(@href,'javascript:validaFormEstablecidas();')]")
    private WebElement continuarPBtnLnk;

    @FindBy(xpath = "//input[@value='FE']")
    private WebElement firmaElectronicaRd;

    @FindBy(xpath = "//*[contains(text(),'Conecte su Firma Electrónica Avanzada')]")
    private WebElement firmaElectronicaTxt;

    @FindBy(xpath = "//input[@value='1']")
    private WebElement excelRd;

    @FindBy(xpath = "//div[@class='PanelClosed']//a[contains(text(), 'Transferir entre cuentas corrientes mismo rut')]")
    private WebElement transferirMismoRut;

    @FindBy(id = "CuentaCorrienteId")
    private WebElement cuentaCorrientOrigenSel;

    @FindBy(id = "CuentaDestinoId")
    private WebElement cuentaCorrientDestinoSel;

    @FindBy(id = "Monto")
    private WebElement montoCuentaInpt;

    @FindBy(xpath = "//input[@value='2']")
    private WebElement txtlRd;

    @FindBy(xpath = "//frame[@name='Arriba']")
    private WebElement arribaFrm;

    @FindBy(xpath = "//a[contains(text(), ' Crear nómina y continuar')]")
    private WebElement crearNominaBtn;

    @FindBy(id = "destinatarios")
    private WebElement cuentasGuardadSel;

    @FindBy(id = "Email")
    private WebElement emailInpt;

    @FindBy(id = "Comentario")
    private WebElement comentarioInpt;

    @FindBy(xpath = "//*[contains(text(), 'Confirmar')]")
    private WebElement confirmarBtn;

    @FindBy(xpath = "//*[@id='enviar-paso-2']")
    private WebElement enviarAprobacionBtn;

    @FindBy(xpath = "//a[contains(@href,'javascript:aprobar();')]")
    private WebElement enviarAprobacionBtnLnk;

    @FindBy(xpath = "//a[contains(@href,'javascript:validaFormEstablecidas();')]")
    private WebElement continuarBtnAltosMontosP;

    @FindBy(xpath = "//img[contains(@src,'timbre-ok-icon')]")
    private WebElement timbreOkLineaTercerosP;

    @FindBy(id = "confirmar")
    private WebElement confirmarBtnAltosMontosMasiva;

    @FindBy(id = "Realizar")
    private WebElement realizarTEFAltosMontosMasiva;

    @FindBy(xpath = "(//input[@id='Monto'])[2]")
    private WebElement montoLineaTercerosNP;

    @FindBy(xpath = "(//input[@id='Email'])[2]")
    private WebElement emailLineaTercerosNP;

    @FindBy(xpath = "(//input[@id='Comentario'])[2]")
    private WebElement comentarioLineaTercerosNP;

    @FindBy(xpath = "(//a[@id='enviar-paso-1'])[2]")
    private WebElement continuarBtnLineaTercerosNP;

    @FindBy(xpath = "//img[@src='img/timbre_trans_realizada.jpg']")
    private WebElement transAprobadaImgAltosMontosNP;

    @FindBy(xpath = "//button[contains(text(), ' Enviar a aprobación')]")
    private WebElement enviarAprobacionTxtBtn;

    @FindBy(xpath = "//a[text()='Pagar']")
    private WebElement pagarBtnTransferenciasMasivas;

    @FindBy(id = "btnDePago")
    private WebElement pagoBtnTransMasivas;

    @FindBy(xpath = "(//a[contains(@href,'javascript:Continuar')])[5]")
    private WebElement continuarBtnTransMasivas;

    @FindBy(xpath = "(//a[text()='Transacciones pendientes'])[2]")
    private WebElement transPendientesDVP;

    @FindBy(xpath = "(//div[@id='tableDVP']//input[@name='checkbox'])[1]")
    private WebElement dvpPendienteChk;

    @FindBy(id = "btnPagar")
    private WebElement pagarDVPBtn;

    @FindBy(id = "action-continue")
    private WebElement continuarDVPBtn;

    @FindBy(xpath = "//img[contains(@src,'success')]")
    private WebElement dvpAprobadaImg;

    @FindBy(xpath = "//a[text()='Transacciones pendientes']")
    private WebElement transPendientesDVPControllerBtn;

    @FindBy(xpath = "//a[contains(@href,'javascript:ApruebaValores()')]")
    private WebElement aprobarDVPControllerBtn;

    /**
     * Ingresar tranferencias.
     */
    public void ingresarTranferencias() {
        driver.switchTo().frame("topFrame");
        MetodosGenericos.waitClickable(driver, transferenciasLnk, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transferenciasLnk);
    }

    /**
     * Click Ingresar transferencias masivas.
     */
    public void ingresarTransferenciasMasivas() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(leftFrame);
        MetodosGenericos.waitClickable(driver, transferenciasMasivasLnk, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transferenciasMasivasLnk);
    }

    /**
     * Ingresar transferir altos montos.
     */
    public void ingresarTransferirAltosMontos() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        MetodosGenericos.visualizarObjeto(transferirAltosMontosLnk, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transferirAltosMontosLnk);
    }

    /**
     * Ingresar transferir altos montos.
     * <p>
     * /* @param cuenta String cuenta
     * /* @param rut    String rut
     *
     * @param usuario   the usuario
     * @param nroCuenta the nro cuenta
     * @param bancoDes  the banco des
     * @param nombreDes the nombre des
     * @param rutDes    the rut des
     * @param Email     the email
     * @param refe      the refe
     * @throws AWTException the awt exception
     */
    public void generarTransferenciaAltosMontosNoPreestablecidas(String usuario, String nroCuenta, String bancoDes,
                                                                 String nombreDes, String rutDes, String Email,
                                                                 String refe) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        MetodosGenericos.waitFor(5);
        Select sel = new Select(CuentaSel);
        sel.selectByIndex(1);
        numeroCuentaTxt.sendKeys(nroCuenta);
        Select sel2 = new Select(bancoDestinoSel);
        sel2.selectByVisibleText(bancoDes.toUpperCase());
        nombreDestiNoPreInpt.sendKeys(nombreDes);
        rutDestinatario.sendKeys(rutDes);
        email.sendKeys(Email);
        referencia.sendKeys(refe);
        monto.sendKeys("1");
        if (usuario.equals("controller")) {
            MetodosGenericos.waitClickable(driver, continuarBtnLnk, 5);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", continuarBtnLnk);
            MetodosGenericos.waitClickable(driver, enviarAprobacionBtnLnk, 5);
            JavascriptExecutor executor1 = (JavascriptExecutor) driver;
            executor1.executeScript("arguments[0].click();", enviarAprobacionBtnLnk);
        } else {
            MetodosGenericos.waitVisibility(driver, firmaElectronicaRd, 5);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", firmaElectronicaRd);
            JavascriptExecutor executor1 = (JavascriptExecutor) driver;
            executor1.executeScript("arguments[0].click();", continuarBtnLnk);
            MetodosGenericos.waitFor(3);
            driver.switchTo().defaultContent();
            driver.switchTo().frame(mainFrame);
            JavascriptExecutor executor2 = (JavascriptExecutor) driver;
            executor2.executeScript("arguments[0].click();", confirmarBtnAltosMontos);
            MetodosGenericos.waitFor(3);
            MetodosGenericos.aprobarFirmaSikuli();
            MetodosGenericos.aprobarFirmaSikuli2();
            MetodosGenericos.waitFor(2);
            MetodosGenericos.ingresarPinMonoapoderadoFEA();
            MetodosGenericos.waitFor(10);
        }
    }

    /**
     * Generar transferencia altos montos no preestablecidas mismo banco.
     *
     * @param nroCuenta the nro cuenta
     * @param bancoDes  the banco des
     * @param nombreDes the nombre des
     * @param rutDes    the rut des
     * @throws AWTException the awt exception
     */
    public void generarTransferenciaAltosMontosNoPreestablecidasMismoBanco(String nroCuenta, String bancoDes, String nombreDes
            , String rutDes) throws AWTException {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        Select sel = new Select(CuentaSel);
        sel.selectByIndex(1);
        numeroCuentaTxt.sendKeys(nroCuenta);
        Select sel2 = new Select(bancoDestinoSel);
        sel2.selectByVisibleText(bancoDes);
        nombreDestiNoPreInpt.sendKeys(nombreDes);
        rutDestinatario.sendKeys(rutDes);
        email.sendKeys(MetodosGenericos.devuelveTextoMinusculas() + "@gmail.com");
        referencia.sendKeys(MetodosGenericos.devuelveTextoMinusculas());
        monto.sendKeys("1");
        MetodosGenericos.waitVisibility(driver, firmaElectronicaRd, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", firmaElectronicaRd);
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", continuarBtnLnk);
        MetodosGenericos.waitFor(3);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", confirmarBtnAltosMontos);
        MetodosGenericos.waitFor(3);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(2);
        MetodosGenericos.ingresarPinMonoapoderadoFEA();
        MetodosGenericos.waitFor(10);
    }

    /**
     * Generar transferencia altos montos preestablecidas.
     *
     * @param usuario the usuario
     * @throws AWTException the awt exception
     */
    public void generarTransferenciaAltosMontosPreestablecidas(String usuario) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        MetodosGenericos.visualizarObjeto(preestablecidasAltosMontos, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", preestablecidasAltosMontos);
        MetodosGenericos.waitFor(10);
        Select sele1 = new Select(CuentaSel);
        sele1.selectByIndex(1);
        MetodosGenericos.waitFor(5);
        Select sele2 = new Select(nombreDestinatarioSel);
        sele2.selectByIndex(1);
        MetodosGenericos.waitFor(5);
        email.sendKeys(MetodosGenericos.devuelveTextoMinusculas() + "@gmail.com");
        referencia.sendKeys(MetodosGenericos.devuelveTextoMinusculas());
        monto.sendKeys("1");
        if (usuario.equals("controller")) {
            MetodosGenericos.waitClickable(driver, continuarBtnLnk, 5);
            JavascriptExecutor executor1 = (JavascriptExecutor) driver;
            executor1.executeScript("arguments[0].click();", continuarPBtnLnk);
            MetodosGenericos.waitClickable(driver, enviarAprobacionBtnLnk, 5);
            JavascriptExecutor executor2 = (JavascriptExecutor) driver;
            executor2.executeScript("arguments[0].click();", enviarAprobacionBtnLnk);
        } else {
            MetodosGenericos.waitVisibility(driver, firmaElectronicaRd, 5);
            JavascriptExecutor executor3 = (JavascriptExecutor) driver;
            executor3.executeScript("arguments[0].click();", firmaElectronicaRd);
            JavascriptExecutor executor4 = (JavascriptExecutor) driver;
            executor4.executeScript("arguments[0].click();", continuarBtnAltosMontosP);
            MetodosGenericos.waitFor(3);
            driver.switchTo().defaultContent();
            driver.switchTo().frame("mainFrame");
            JavascriptExecutor executor5 = (JavascriptExecutor) driver;
            executor5.executeScript("arguments[0].click();", confirmarBtnAltosMontos);
            MetodosGenericos.waitFor(3);
            MetodosGenericos.aprobarFirmaSikuli();
            MetodosGenericos.aprobarFirmaSikuli2();
            MetodosGenericos.waitFor(2);
            MetodosGenericos.ingresarPinMonoapoderadoFEA();
        }
    }

    /**
     * Click Ingresar transferir linea terceros.
     */
    public void clickTransferirLineaTerceros() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(leftFrame);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transferirLineaTercerosLnk);
    }

    /**
     * Ingresar transferir linea terceros np.
     *
     * @param usuario            String usuario
     * @param nombreDestinatario String nombre destinatario
     * @param rut                String rut
     * @param cuenta             String cuenta
     * @param banco              String banco
     * @param correo             the correo
     * @param comentario         the comentario
     * @param tipoCuenta         the tipo cuenta
     * @throws AWTException the awt exception
     */
    public void ingresarTransferirLineaTercerosNP(String usuario, String nombreDestinatario,
                                                  String rut, String cuenta, String banco, String correo,
                                                  String comentario, String tipoCuenta) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.waitFor(15);
        try {
            if (driver.findElement(By.xpath("//input[@id='Monto']")).isDisplayed()) {
                driver.findElement(By.xpath("//input[@id='Monto']")).sendKeys("1");

            } else {
                montoLineaTercerosNP.sendKeys("1");
            }
        } catch (Exception e) {
        }
        nombreDestLineaTerceros.sendKeys(nombreDestinatario);
        rutDestLineaTerceros.sendKeys(rut);
        numeroCuentaLineaTerceros.sendKeys(cuenta);
        Select sel = new Select(bancoDestLineaTerceros);
        sel.selectByVisibleText(banco);
        MetodosGenericos.waitFor(5);
        switch (tipoCuenta) {
            case "Ahorro":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", tipoCuentaAhorroRd);
                break;
            case "Corriente":
                JavascriptExecutor executor1 = (JavascriptExecutor) driver;
                executor1.executeScript("arguments[0].click();", tipoCuentaCorrienteRd);
                break;
            case "Vista":
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", tipoCuentaVistaRd);
                break;
        }
        try {
            if (driver.findElement(By.xpath("//input[@id='Email']")).isDisplayed()) {
                driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(correo);
            } else {
                emailLineaTercerosNP.sendKeys(correo);
            }
        } catch (Exception e) {
        }
        ;
        try {
            if (driver.findElement(By.xpath("//input[@id='Comentario']")).isDisplayed()) {
                driver.findElement(By.xpath("//input[@id='Comentario']")).sendKeys(comentario);
            } else {
                comentarioLineaTercerosNP.sendKeys(comentario);
            }
        } catch (Exception e) {
        }
        ;
        try {
            if (continuarBtn.isDisplayed()) {
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", continuarBtn);
            } else {
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", continuarBtnLineaTercerosNP);
            }
        } catch (Exception e) {
        }
        ;
        //Paso 2 Transferencia a terceros
        if (usuario.equals("controller")) {
            driver.switchTo().defaultContent();
            driver.switchTo().frame(mainFrame);
            driver.switchTo().frame(centralFrm);
            MetodosGenericos.waitClickable(driver, enviarAprobacionBtn, 5);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", enviarAprobacionBtn);
        } else {
            driver.switchTo().defaultContent();
            driver.switchTo().frame(mainFrame);
            driver.switchTo().frame(centralFrm);
            MetodosGenericos.waitClickable(driver, firmaElectronicaLineaTerceros, 10);
            MetodosGenericos.waitFor(3);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", firmaElectronicaLineaTerceros);
            MetodosGenericos.waitFor(2);
            JavascriptExecutor executor1 = (JavascriptExecutor) driver;
            executor1.executeScript("arguments[0].click();", enviarAprobacionBtn);
            MetodosGenericos.waitFor(2);
            MetodosGenericos.aprobarFirmaSikuli();
            MetodosGenericos.aprobarFirmaSikuli2();
            MetodosGenericos.waitFor(2);
            MetodosGenericos.ingresarPinMonoapoderadoFEA();
            MetodosGenericos.waitFor(10);
        }
    }

    /**
     * Ingresar transferir linea terceros Preestablecida.
     *
     * @param usuario String usuario
     */
    public void ingresarTransferirLineaTercerosP(String usuario) {
        clickTransferirLineaTerceros();
        MetodosGenericos.waitFor(3);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.waitFor(5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", preestablecidasTercerosLinea);
        MetodosGenericos.waitFor(5);
        montoCuentaInpt.sendKeys("1");
        Select sel = new Select(cuentasGuardadSel);
        sel.selectByIndex(1);
        emailInpt.sendKeys("correo@correo.cl");
        comentarioInpt.sendKeys("comentario");
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", continuarBtn);
        if (usuario.equals("controller")) {
            driver.switchTo().defaultContent();
            driver.switchTo().frame(mainFrame);
            driver.switchTo().frame(centralFrm);
            JavascriptExecutor execut = (JavascriptExecutor) driver;
            execut.executeScript("arguments[0].click();", enviarAprobacionBtn);
        } else {
            driver.switchTo().defaultContent();
            driver.switchTo().frame(mainFrame);
            driver.switchTo().frame(centralFrm);
            JavascriptExecutor xecutor = (JavascriptExecutor) driver;
            xecutor.executeScript("arguments[0].click();", firmaElectronicaLineaTerceros);
            MetodosGenericos.waitClickable(driver, firmaElectronicaLineaTerceros, 5);
        }
    }

    /**
     * Ingresar transferir cclv.
     */
    public void ingresarTransferirCCLV() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(leftFrame);
        MetodosGenericos.visualizarObjeto(transferirCCLVLnk, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transferirCCLVLnk);
        MetodosGenericos.waitFor(5);
    }

    /**
     * Cerrar modal.
     */
    public void cerrarModal() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        MetodosGenericos.visualizarObjeto(modalDiv, 5);
        if (modalDiv.isDisplayed()) {
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", aceptarModalBtn);
        }
    }

    /**
     * Ingresa datos transf cclv.
     *
     * @param tipo the tipo
     */
    public void ingresaDatosTransfCCLV(String tipo, String usuario) {
        cerrarModal();
        MetodosGenericos.waitFor(5);
        Select sel = new Select(cuentaOrigenSel);
        sel.selectByIndex(1);
        String transferencia = null;
        switch (tipo) {
            case "PH-SM":
                transferencia = "PH-SM - Pagadero Hoy";
                break;
            case "PM":
                transferencia = "PM - Pagadero Mañana";
                break;
            case "RV":
                transferencia = "RV - Renta Variable";
                break;
            default:
                break;
        }
        Select sel1 = new Select(camaraSel);
        sel1.selectByVisibleText(transferencia);
        MetodosGenericos.waitFor(5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", aceptarModalBtn);
        MetodosGenericos.waitFor(5);
        Select sel2 = new Select(tipoFondoSel);
        sel2.selectByVisibleText("A");
        Select sel3 = new Select(tipoSaldoSel);
        sel3.selectByVisibleText("OT - Otras Transacciones");
        MetodosGenericos.waitFor(5);
        montoInpt.sendKeys("1");
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", continuarBtn);
        MetodosGenericos.waitFor(5);
        switch (usuario){
            case "controller":
                JavascriptExecutor xecutor = (JavascriptExecutor) driver;
                xecutor.executeScript("arguments[0].click();", enviarAprobacionTxtBtn);
                break;
            default:
                break;
        }
    }

    /**
     * Selecciona fe.
     */
    public void seleccionaFE() {
        MetodosGenericos.waitFor(15);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", firmaElectronicaRd);
        MetodosGenericos.waitFor(3);
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", realizarTEFAltosMontosMasiva);
        MetodosGenericos.waitFor(2);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(2);
        MetodosGenericos.ingresarPinMonoapoderadoFEA();
        MetodosGenericos.waitFor(10);
    }

    /**
     * Valida solicita fe boolean.
     *
     * @return the boolean
     */
    public boolean validaSolicitaFE() {
        MetodosGenericos.visualizarObjeto(firmaElectronicaTxt, 5);
        return firmaElectronicaTxt.isDisplayed();
    }

    /**
     * Cargar archivo masivo.
     *
     * @param tipo the tipo
     */
    public void cargarArchivoMasivo(String tipo) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(arribaFrm);
        MetodosGenericos.visualizarObjeto(excelRd, 5);
        switch (tipo) {
            case "xls":
                JavascriptExecutor executor1 = (JavascriptExecutor) driver;
                executor1.executeScript("arguments[0].click();", excelRd);
                File file = new File("src/main/java/recursos/masivo.xls");
                driver.findElement(By.xpath("//input[@type ='File']")).sendKeys(file.getAbsolutePath());
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", crearNominaBtn);
                MetodosGenericos.waitFor(5);
                break;
            case "txt":
                MetodosGenericos.visualizarObjeto(txtlRd, 5);
                JavascriptExecutor executo = (JavascriptExecutor) driver;
                executo.executeScript("arguments[0].click();", txtlRd);
                File file2 = new File("src/main/java/recursos/masivo.txt");
                driver.findElement(By.xpath("//input[@type ='File']")).sendKeys(file2.getAbsolutePath());
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", crearNominaBtn);
                break;
        }
        aceptarNominaRepetida();
        MetodosGenericos.waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(arribaFrm);
        WebElement  continuarBtn = driver.findElement(By.xpath("(//a[contains(@href,'javascript:Continuar')])[5]"));
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", continuarBtn);

    }

    /**
     * Transferir mismo rut.
     */
    public void transferirMismoRut() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(leftFrame);
        MetodosGenericos.visualizarObjeto(transferirMismoRut, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transferirMismoRut);
        MetodosGenericos.waitFor(5);
    }

    /**
     * Click continuar.
     */
    public void clickContinuar() {
        MetodosGenericos.waitClickable(driver, continuarBtn, 15);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", continuarBtn);
    }

    /**
     * Ingresa datos transf mismo rut.
     *
     * @param origen  the origen
     * @param destino the destino
     */
    public void ingresaDatosTransfMismoRut(String origen, String destino) {
        driver.switchTo().alert().accept();
        MetodosGenericos.waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(centralFrm);
        Select sel = new Select(cuentaCorrientOrigenSel);
        sel.selectByVisibleText(origen);
        Select sel1 = new Select(cuentaCorrientDestinoSel);
        sel1.selectByVisibleText(destino);
        montoCuentaInpt.sendKeys("1");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", continuarBtn);
    }

    /**
     * Transf terceros otro banco paso 1.
     *
     * @param cuenta String
     * @throws AWTException the awt exception
     */
    public void transfTercerosOtroBancoPaso1(String cuenta) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.waitFor(5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", preestablecidasTercerosLinea);
        MetodosGenericos.waitFor(3);
        montoCuentaInpt.sendKeys("1");
        Select sel = new Select(cuentasGuardadSel);
        sel.selectByVisibleText(cuenta);
        emailInpt.sendKeys("test@test.cl");
        comentarioInpt.sendKeys("pago");
        MetodosGenericos.waitClickable(driver, continuarBtn, 5);
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", continuarBtn);
        MetodosGenericos.waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.waitClickable(driver, firmaElectronicaLineaTerceros, 10);
        JavascriptExecutor xecutor = (JavascriptExecutor) driver;
        xecutor.executeScript("arguments[0].click();", firmaElectronicaLineaTerceros);
        MetodosGenericos.waitFor(3);
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", enviarAprobacionBtn);
        MetodosGenericos.waitFor(3);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(3);
        MetodosGenericos.ingresarPinMonoapoderadoFEA();
        MetodosGenericos.waitFor(10);
    }

    /**
     * Transferencia altos montos masiva excel txt.
     *
     * @param tipo String
     */
    public void transferenciaAltosMontosMasivaExcelTxt(String tipo) {
        MetodosGenericos.waitFor(8);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", nominaDeTransferencia);
        MetodosGenericos.waitFor(5);
        Select sel = new Select(cuentaOrigenNominaTransferenciaSel);
        sel.selectByIndex(1);
        switch (tipo) {
            case "TXT":
                JavascriptExecutor executo = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", tipoNominaTxt);
                MetodosGenericos.waitFor(30);
                JavascriptExecutor xecutor = (JavascriptExecutor) driver;
                xecutor.executeScript("arguments[0].click();", btnSubirNomina);
                MetodosGenericos.waitFor(5);
                String winHandleBefore = driver.getWindowHandle();
                for (String winHandle : driver.getWindowHandles()) {
                    driver.switchTo().window(winHandle);
                }
                MetodosGenericos.waitFor(10);
                File file = new File("src/main/java/recursos/Nomina TEF Altos Montos.txt");
                MetodosGenericos.waitFor(10);
                driver.findElement(By.xpath("//input[@type ='file']")).sendKeys(file.getAbsolutePath());
                MetodosGenericos.waitFor(30);
                JavascriptExecutor executor1 = (JavascriptExecutor) driver;
                executor1.executeScript("arguments[0].click();", btnSubirNomina);
                MetodosGenericos.waitFor(10);
                driver.switchTo().window(winHandleBefore);
                MetodosGenericos.waitFor(10);
                break;
            case "EXCEL":
                JavascriptExecutor executor5 = (JavascriptExecutor) driver;
                executor5.executeScript("arguments[0].click();", tipoNominaExcel);
                JavascriptExecutor executor6 = (JavascriptExecutor) driver;
                executor6.executeScript("arguments[0].click();", btnSubirNomina);
                String winHandleBefore2 = driver.getWindowHandle();
                for (String winHandle : driver.getWindowHandles()) {
                    driver.switchTo().window(winHandle);
                }
                File file2 = new File("src/main/java/recursos/Nomina TEF Altos Montos.xls");
                driver.findElement(By.xpath("//input[@type ='file']")).sendKeys(file2.getAbsolutePath());
                JavascriptExecutor executor7 = (JavascriptExecutor) driver;
                executor7.executeScript("arguments[0].click();", btnSubirNomina);
                MetodosGenericos.waitFor(5);
                driver.switchTo().window(winHandleBefore2);
                break;
            case "xlsController":
                JavascriptExecutor executor11 = (JavascriptExecutor) driver;
                executor11.executeScript("arguments[0].click();", tipoNominaExcel);
                JavascriptExecutor executor12 = (JavascriptExecutor) driver;
                executor12.executeScript("arguments[0].click();", btnSubirNomina);
                String winHandleBefore3 = driver.getWindowHandle();
                for (String winHandle : driver.getWindowHandles()) {
                    driver.switchTo().window(winHandle);
                }
                File file3 = new File("src/main/java/recursos/NominaMasivaController.xls");
                driver.findElement(By.xpath("//input[@type ='file']")).sendKeys(file3.getAbsolutePath());
                JavascriptExecutor executor13 = (JavascriptExecutor) driver;
                executor13.executeScript("arguments[0].click();", btnSubirNomina);
                MetodosGenericos.waitFor(5);
                driver.switchTo().window(winHandleBefore3);
                break;
            case "txtController":
                JavascriptExecutor executor14 = (JavascriptExecutor) driver;
                executor14.executeScript("arguments[0].click();", tipoNominaTxt);
                JavascriptExecutor executor15 = (JavascriptExecutor) driver;
                executor15.executeScript("arguments[0].click();", btnSubirNomina);
                String winHandleBefore4 = driver.getWindowHandle();
                for (String winHandle : driver.getWindowHandles()) {
                    driver.switchTo().window(winHandle);
                }
                File file4 = new File("src/main/java/recursos/NominaMasivaController.txt");
                driver.findElement(By.xpath("//input[@type ='file']")).sendKeys(file4.getAbsolutePath());
                JavascriptExecutor executor16 = (JavascriptExecutor) driver;
                executor16.executeScript("arguments[0].click();", btnSubirNomina);
                MetodosGenericos.waitFor(5);
                driver.switchTo().window(winHandleBefore4);
                break;
        }
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement continuarBtn = driver.findElement(By.xpath("//button[contains(text(),'Continuar')]"));
        JavascriptExecutor executor17 = (JavascriptExecutor) driver;
        executor17.executeScript("arguments[0].click();", continuarBtn);
    }


    /**
     * Confirmar transf mismo rut.
     */
    public void confirmarTransfMismoRut() {
        MetodosGenericos.waitFor(5);
        driver.switchTo().alert().accept();
        MetodosGenericos.waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.waitClickable(driver, confirmarBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", confirmarBtn);
    }

    /**
     * Valida transferencia pendiente boolean.
     *
     * @return the boolean
     */
    public boolean validaTransferenciaPendiente() {
        MetodosGenericos.waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(centralFrm);
        WebElement timbreOK = driver.findElement(By.xpath("//*[@class='timbre-comprobante']"));
        return timbreOK.isDisplayed();
    }

    /**
     * Valida altos montos controller boolean.
     *
     * @return the boolean
     */
    public boolean validaAltosMontosController() {
        MetodosGenericos.waitFor(30);
        boolean textOk = false;
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        WebElement textAutorizado =
                driver.findElement(By.xpath("//*[contains(text(),'Aprobada por')]"));
        WebElement textController =
                driver.findElement(By.xpath("(//*[contains(text(),'Angelica                                           " +
                        "Olmedo                                             Leiva                                             ')])[2]"));
        if (textAutorizado.isDisplayed() && textController.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    /**
     * Validar tef altos montos boolean.
     *
     * @return the boolean
     */
    public boolean validarTEFAltosMontos() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        MetodosGenericos.visualizarObjeto(transAprobadaImgAltosMontosNP, 10);
        return transAprobadaImgAltosMontosNP.isDisplayed();
    }

    /**
     * Validar tef linea terceros boolean.
     *
     * @return the boolean
     */
    public boolean validarTEFLineaTerceros() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.visualizarObjeto(timbreOkLineaTercerosP, 90);
        return timbreOkLineaTercerosP.isDisplayed();
    }

    /**
     * Valida nomina pendiente pago controller boolean.
     *
     * @return the boolean
     */
    public boolean validaNominaPendientePagoController() {
        boolean textOk = false;
        MetodosGenericos.waitFor(60);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(centralFrm);
        WebElement textAutorizado =
                driver.findElement(By.xpath("//*[contains(text(),'Autorizada por:')]"));
        WebElement textController =
                driver.findElement(By.xpath("//*[contains(text(),'Angelica Olmedo Leiva (Controller)')]"));
        if (textAutorizado.isDisplayed() && textController.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    /**
     * Pagar transferencias masivas.
     */
    public void pagarTransferenciasMasivas() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(arribaFrm);
        MetodosGenericos.waitClickable(driver, pagarBtnTransferenciasMasivas, 15);
        JavascriptExecutor executo = (JavascriptExecutor) driver;
        executo.executeScript("arguments[0].click();", pagarBtnTransferenciasMasivas);
        MetodosGenericos.waitFor(6);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(arribaFrm);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.waitFor(8);
        MetodosGenericos.waitClickable(driver, firmaElectronicaLineaTerceros, 20);
        JavascriptExecutor xecutor = (JavascriptExecutor) driver;
        xecutor.executeScript("arguments[0].click();", firmaElectronicaLineaTerceros);
        MetodosGenericos.waitFor(3);
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", pagoBtnTransMasivas);
        MetodosGenericos.waitFor(3);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(2);
        MetodosGenericos.ingresarPinMonoapoderadoFEA();
        MetodosGenericos.waitFor(10);
    }

    /**
     * Validar transferencia masiva boolean.
     *
     * @return the boolean
     */
    public boolean validarTransferenciaMasiva() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(arribaFrm);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.visualizarObjeto(timbreOkLineaTercerosP, 20);
        return timbreOkLineaTercerosP.isDisplayed();
    }

    /**
     * Valida pendiente pago controller boolean.
     *
     * @return the boolean
     */
    public boolean validaPendientePagoController() {
        boolean textOk = false;
        MetodosGenericos.waitFor(60);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(centralFrm);
        WebElement textOpRealizada =
                driver.findElement(By.xpath("//*[text()='Operación realizada']"));
        WebElement textPendiente =
                driver.findElement(By.xpath("//*[text()='Pendiente Pago']"));
        if (textOpRealizada.isDisplayed() && textPendiente.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    /**
     * Valida nomina masiva pendiente pago controller boolean.
     *
     * @return the boolean
     */
    public boolean validaNominaMasivaPendientePagoController() {
        boolean textOk = false;
        MetodosGenericos.waitFor(60);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(arribaFrm);
        WebElement textAprobado =
                driver.findElement(By.xpath("//*[contains(text(),'Aprobada por:')]"));
        WebElement textController =
                driver.findElement(By.xpath("//*[contains(text(),'Angelica Olmedo Leiva')]"));
        if (textAprobado.isDisplayed() && textController.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    /**
     * Aceptar nomina repetida.
     */
    public static void aceptarNominaRepetida() {
        Screen screen = new Screen();
        File file = new File("src/main/java/recursos/");
        Pattern aceptarBtn = new Pattern(file + "/aceptarBtn.PNG");
        try {
            if (screen.wait(aceptarBtn.exact()) != null) {
                screen.click(aceptarBtn);
            }
        } catch (FindFailed e) {
            System.out.println("No hay nomina duplicada");
            e.getStackTrace();
        }
    }

    /**
     * Click enviar aprobacion.
     */
    public void clickEnviarAprobacion() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", enviarAprobacionTxtBtn);
    }

    public void entrarTransPendientesDVP(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame(leftFrame);
        MetodosGenericos.waitClickable(driver, transPendientesDVP, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transPendientesDVP);
    }

    public void pagarTransPendienteDVP(String usuario){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        MetodosGenericos.waitClickable(driver,dvpPendienteChk, 5);
        JavascriptExecutor executor0 = (JavascriptExecutor) driver;
        executor0.executeScript("arguments[0].click();", dvpPendienteChk);
        switch (usuario){
            case "controller":
                MetodosGenericos.waitClickable(driver, aprobarDVPControllerBtn, 10);
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", aprobarDVPControllerBtn);
                break;
            default:
                MetodosGenericos.waitClickable(driver, firmaElectronicaRd, 10);
                JavascriptExecutor executo = (JavascriptExecutor) driver;
                executo.executeScript("arguments[0].click();", firmaElectronicaRd);
                JavascriptExecutor xecutor = (JavascriptExecutor) driver;
                xecutor.executeScript("arguments[0].click();", pagarDVPBtn);
                break;
        }
    }

    public void pagarDVPConFEA(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame("mainFrame");
        MetodosGenericos.waitClickable(driver, continuarDVPBtn, 10);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", continuarDVPBtn);
        MetodosGenericos.waitFor(3);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(2);
        MetodosGenericos.ingresarPinMonoapoderadoFEA();
    }

    public boolean validarTransDVP(){
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        MetodosGenericos.visualizarObjeto(dvpAprobadaImg, 120);
        return dvpAprobadaImg.isDisplayed();
    }

    public void entrarTransPendientesDVPController() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("leftFrame");
        MetodosGenericos.waitClickable(driver, transPendientesDVPControllerBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", transPendientesDVPControllerBtn);
    }

    public boolean validaTransDVPController() {
        boolean textOk = false;
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement textAprobado =
                driver.findElement(By.xpath("//h3[contains(text(),'¡Transferencias aprobadas y pendientes de pago!')]"));
        WebElement textController =
                driver.findElement(By.xpath("//*[contains(text(),'angelica.olmedo@security.c')]"));
        if (textAprobado.isDisplayed() && textController.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    public boolean validaTransDVPApoderado() {
        boolean textOk = false;
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement textAprobado =
                driver.findElement(By.xpath("//h3[contains(text(),'¡Transferencia aprobada! 1 de tus Transferencias continúa pendiente de pago.' )]"));
        WebElement textController =
                driver.findElement(By.xpath("//*[contains(text(),'angelica.olmedo@security.c')]"));
        if (textAprobado.isDisplayed() && textController.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    public boolean validaTransDVPApoderadoDos() {// falta modificar texto de los xpath
        boolean textOk = false;
        MetodosGenericos.waitFor(180);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        WebElement textAprobado =
                driver.findElement(By.xpath("//h3[contains(text(),'¡Transferencia procesada! tu Transferencia ha sido pagada y enviada al sistema de pago.' )]"));
        WebElement textController =
                driver.findElement(By.xpath("//*[contains(text(),'jjofre@security.cl')]"));
        if (textAprobado.isDisplayed() && textController.isDisplayed()) {
            textOk = true;
        }
        return textOk;
    }

    public boolean validarCCLV() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        MetodosGenericos.visualizarObjeto(timbreOkLineaTercerosP, 60);
        return timbreOkLineaTercerosP.isDisplayed();
    }

    public boolean validarTEFAltosMontosNominas() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        MetodosGenericos.visualizarObjeto(timbreOkLineaTercerosP, 60);
        return timbreOkLineaTercerosP.isDisplayed();
    }

    public void pagarTEFAltosMontosNominas(){
        MetodosGenericos.waitFor(10);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        MetodosGenericos.waitClickable(driver, firmaElectronicaRd, 15);
        JavascriptExecutor executor3 = (JavascriptExecutor) driver;
        executor3.executeScript("arguments[0].click();", firmaElectronicaRd);
        MetodosGenericos.waitFor(3);
        JavascriptExecutor executor4 = (JavascriptExecutor) driver;
        executor4.executeScript("arguments[0].click();", realizarTEFAltosMontosMasiva);
        MetodosGenericos.waitFor(3);
        MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(2);
        MetodosGenericos.ingresarPinMonoapoderadoFEA();
        MetodosGenericos.waitFor(3);
    }
}
