package page;

import driver.DriverContext;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.MetodosGenericos;

import java.io.File;

/**
 * The type Vale vistas.
 */
public class ValeVistas {
    private WebDriver driver;

    /**
     * Instantiates a new Vale vistas.
     */
    public ValeVistas() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "//frame[contains(@id, 'topFrame')]")
    private WebElement topFrame;

    @FindBy(xpath = "//frame[@id='leftFrame']")
    private WebElement leftFrame;

    @FindBy(xpath = "//frame[@id='mainFrame']")
    private WebElement mainFrame;

    @FindBy(xpath = "//iframe[@name='central']")
    private WebElement centralFrm;

    @FindBy(xpath = "//frame[@name='Arriba']")
    private WebElement arribaFrm;

    @FindBy(xpath = "//frame[@name='Indexa']")
    private WebElement indexaFrm;

    @FindBy(xpath = "//*[@id=\"menu-izquierdo\"]//a[contains(text(), 'Ingresar nómina')]")
    private WebElement ingresarNominaLnk;

    @FindBy(xpath = "//*[@class='btn btn-primary btn-large' and contains(text(),' Crear Nómina')]")
    private WebElement crearNominaBtn;

    @FindBy(name = "Tiempo_ExpiracionVV")
    private WebElement tiempoVigenciaVVInpt;

    @FindBy(xpath = "//input[@name='TipoIngreso' and @value='2']")
    private WebElement subirArchivoRd;

    @FindBy(xpath = "//input[@name='TipoIngreso' and @value='1']")
    private WebElement digitarManualRd;

    @FindBy(xpath = "//input[@name='Tipo_Captura' and @value='1']")
    private WebElement formatoTxtRd;

    @FindBy(xpath = "//input[@name='Tipo_Captura' and @value='2']")
    private WebElement formatoXlsRd;

    @FindBy(xpath = "//input[@name='Tipo_Captura' and @value='3']")
    private WebElement formatoXlsCDetRd;

    @FindBy(xpath = "//div[@id='btn-subir']//button[text()='Crear nómina y continuar']")
    private WebElement crearNominaContinuarBtn;

    @FindBy(xpath = "//div[@id='btn-digitar']//button[text()='Crear nómina y continuar']")
    private WebElement crearNominaContinuarManualBtn;

    @FindBy(xpath = "//a[text()='Continuar']")
    private WebElement continuarBtn;

    @FindBy(xpath = "//a[text()='Confirmar']")
    private WebElement confirmarBtn;

    @FindBy(xpath = "(//a[text()='Pagar'])[1]")
    private WebElement pagarNominaBtn;

    @FindBy(id = "clave-dinamica-radio4")
    private WebElement feaInput;

    @FindBy(id = "btnDePago")
    private WebElement realizarOperacionPagoBtn;

    @FindBy(xpath = "//a[text()=' Agregar registro']")
    private WebElement agregarRegistroVVBtn;

    @FindBy(id = "Rut")
    private WebElement rutNominaInpt;

    @FindBy(id = "Nombre")
    private WebElement nombreNominaInpt;

    @FindBy(id = "Email_Proveedor")
    private WebElement emailNominaInpt;

    @FindBy(xpath = "//input[@name='MedioAbono']")
    private WebElement medioAbonoRd;

    @FindBy(id = "Monto")
    private WebElement montoNominaInpt;

    @FindBy(id = "button1")
    private WebElement guardarCambiosBtn;

    @FindBy(xpath = "(//a[text()='Enviar a aprobación'])[2]")
    private WebElement enviarAprobacionCreadaBtn;

    @FindBy(xpath = "(//a[text()='Enviar a aprobación'])[1]")
    private WebElement enviarAprobacionBtn;

    /**
     * Click ingresar nomina.
     */
    public void clickIngresarNomina() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(leftFrame);
        MetodosGenericos.waitClickable(driver, ingresarNominaLnk, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", ingresarNominaLnk);
    }

    /**
     * Click crear nomina.
     */
    public void clickCrearNomina() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        MetodosGenericos.waitClickable(driver, crearNominaBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", crearNominaBtn);
    }

    /**
     * Completa datos nueva nomina.
     *
     * @param tipo    the tipo
     * @param formato the formato
     */
    public void completaDatosNuevaNomina(String tipo, String formato) {
        tiempoVigenciaVVInpt.sendKeys("10");
        seleccionarTipoIngreso(tipo);
        if (tipo.equals("archivo")) {
            seleccionarFormatoIngreso(formato);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", crearNominaContinuarBtn);
        }else{
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", crearNominaContinuarManualBtn);
        }
    }

    private void seleccionarFormatoIngreso(String formato) {
        switch (formato) {
            case "txt":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", formatoTxtRd);
                break;
            case "xlsSinD":
                JavascriptExecutor executor2 = (JavascriptExecutor) driver;
                executor2.executeScript("arguments[0].click();", formatoXlsRd);
                break;
            case "xlsConD":
                JavascriptExecutor executor3 = (JavascriptExecutor) driver;
                executor3.executeScript("arguments[0].click();", formatoXlsCDetRd);
                break;
            default:
                break;
        }

    }

    private void seleccionarTipoIngreso(String tipo) {
        switch (tipo) {
            case "archivo":
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", subirArchivoRd);
                break;
            case "manual":
                JavascriptExecutor executor1 = (JavascriptExecutor) driver;
                executor1.executeScript("arguments[0].click();", digitarManualRd);
                break;
            default:
                break;
        }
    }

    /**
     * Cargar archivo masivo.
     *
     * @param tipo the tipo
     */
    public void cargarArchivoMasivo(String tipo) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        switch (tipo) {
            case "txt":
                File file = new File("src/main/java/recursos/Nomina VV S.Detalle.TXT");
                driver.findElement(By.xpath("//input[@type ='File']")).
                        sendKeys(file.getAbsolutePath());
                break;
            case "xlsSinD":
                File file2 = new File("src/main/java/recursos/Nomina VV S.Detalle.xlsx");
                driver.findElement(By.xpath("//input[@type ='File']")).
                        sendKeys(file2.getAbsolutePath());
                break;
            case "xlsConD":
                File file3 = new File("src/main/java/recursos/Nomina VV C.Detalle.xlsx");
                driver.findElement(By.xpath("//input[@type ='File']")).
                        sendKeys(file3.getAbsolutePath());
                break;
        }
        MetodosGenericos.waitClickable(driver, continuarBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", continuarBtn);
    }

    /**
     * Validacion nomina cargada.
     *
     * @param usuario the usuario
     * @param tipo    the tipo
     */
    public void validacionNominaCargada(String usuario, String tipo) {
        driver.findElement(By.xpath("(//a[contains(text(),'Validar')])[1]")).click();
        MetodosGenericos.waitVisibility(driver, confirmarBtn, 5);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", confirmarBtn);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        if(usuario.equals("controller")){
            JavascriptExecutor executor1 = (JavascriptExecutor) driver;
            executor1.executeScript("arguments[0].click();", enviarAprobacionBtn);
        }else {
            JavascriptExecutor executor2 = (JavascriptExecutor) driver;
            executor2.executeScript("arguments[0].click();", pagarNominaBtn);
        }
        MetodosGenericos.waitFor(10);
        if(tipo.equals("vv")){
            WebElement confirmar1 = driver.findElement(By.xpath("(//a[text()='Confirmar'])[2]"));
            JavascriptExecutor executor3 = (JavascriptExecutor) driver;
            executor3.executeScript("arguments[0].click();", confirmar1);

        }else{
            WebElement confirmar2 = driver.findElement(By.xpath("(//a[text()='Confirmar'])[3]"));
            JavascriptExecutor executor4 = (JavascriptExecutor) driver;
            executor4.executeScript("arguments[0].click();", confirmar2);

        }
    }

    /**
     * Realizar pago con fea.
     */
    public void realizarPagoConFea() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        driver.switchTo().frame(centralFrm);
        MetodosGenericos.waitFor(20);
        MetodosGenericos.waitClickable(driver, feaInput, 25);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", feaInput);
        MetodosGenericos.waitFor(3);
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", realizarOperacionPagoBtn);
          MetodosGenericos.aprobarFirmaSikuli();
        MetodosGenericos.aprobarFirmaSikuli2();
        MetodosGenericos.waitFor(2);
        MetodosGenericos.ingresarPinMonoapoderadoFEA();
        MetodosGenericos.waitFor(10);
    }


    /**
     * Valida comprobante pago boolean.
     *
     * @return the boolean
     */
    public boolean validaNominaPendientePagoController() {
        boolean textOk = false;
        MetodosGenericos.waitFor(60);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        WebElement textAutorizado = driver.findElement(By.xpath("//*[contains(text(),'Autorizada por:')]"));
        WebElement textController = driver.findElement(By.xpath("//*[contains(text(),'Angelica Olmedo Leiva (Controller)')]"));
        if(textAutorizado.isDisplayed() || textController.isDisplayed()){
            textOk= true;
        }
        return textOk;
    }

    /**
     * Agregar registro vale vista.
     *
     * @param rut    the rut
     * @param nombre the nombre
     * @param correo the correo
     */
    public void agregarRegistroValeVista(String rut, String nombre, String correo) {
        clickAgregarRegitroNomina();
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        rut = rut.replace("-", "");
        rutNominaInpt.sendKeys(rut);
        nombreNominaInpt.sendKeys(nombre);
        emailNominaInpt.sendKeys(correo);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", medioAbonoRd);
        montoNominaInpt.sendKeys("1");
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", guardarCambiosBtn);
    }

    /**
     * Click agregar regitro nomina.
     */
    public void clickAgregarRegitroNomina() {
        MetodosGenericos.waitFor(3);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", agregarRegistroVVBtn);
    }

    /**
     * Validacion nomina creada.
     *
     * @param usuario the usuario
     */
    public void validacionNominaCreada(String usuario) {
        driver.findElement(By.xpath("(//a[contains(text(),'Validar')])[2]")).click();
        MetodosGenericos.waitVisibility(driver, confirmarBtn, 5);
        JavascriptExecutor executor0 = (JavascriptExecutor) driver;
        executor0.executeScript("arguments[0].click();", confirmarBtn);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        if(usuario.equals("controller")){
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", enviarAprobacionCreadaBtn);
        }else{
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", pagarNominaBtn);
        }

        MetodosGenericos.waitFor(10);
        WebElement confirmar = driver.findElement(By.xpath("(//a[text()='Confirmar'])[2]"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", confirmar);
    }

    /**
     * Valida nomina pendiente pago mono apoderado boolean.
     *
     * @return the boolean
     */
    public boolean validaNominaPendientePagoMonoApoderado() {
        boolean textOk = false;
        MetodosGenericos.waitFor(60);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(mainFrame);
        driver.switchTo().frame(indexaFrm);
        WebElement textAutorizado = driver.findElement(By.xpath("//*[contains(text(),'Autorizada por:')]"));
        WebElement textController = driver.findElement(By.xpath("//*[contains(text(),'Angelica Olmedo Leiva (Controller)')]"));
        if(textAutorizado.isDisplayed() || textController.isDisplayed()){
            textOk= true;
        }
        return textOk;
    }
}
