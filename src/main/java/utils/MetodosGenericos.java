package utils;

import driver.DriverContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.*;
import org.testng.Assert;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Random;


public class MetodosGenericos {
    public MetodosGenericos() {
    }

    public static boolean esperarElemento(WebElement objeto) {
        boolean existe = false;
        int intentos = 0;
        while ((!existe) & intentos < 10) {
            System.out.println(intentos);
            existe = MetodosGenericos.visualizarObjeto(objeto, 10);
            System.out.println("esperando los objetos " + existe);
            if (!existe) {
                intentos++;
            } else {
                System.out.println("Se encuentra Elemento ::" + objeto.getText());
            }
        }
        return existe;
    }

    public static boolean visualizarObjeto(WebElement elementName, int timeout) {
        try {
            System.out.println("Validando visibilidad del elemento");
            WebDriverWait wait = new WebDriverWait(DriverContext.getDriver(), timeout);
            wait.until(ExpectedConditions.visibilityOf(elementName));
            System.out.println("Es visible el elemento : " + elementName);
            return true;
        } catch (Exception e) {
            System.out.println("No es visible el elemento : " + elementName);
            return false;
        }
    }

    public static boolean isEnabled(WebElement element) throws NoSuchElementException {
        System.out.println("Esta el elemento habilitado?:" + element.getAttribute("enabled"));
        return element.getAttribute("enabled").trim().equals("true");
    }

    public static boolean validarEnable(WebElement objeto, int segundos) {
        System.out.println("Se validara que el objeto:" + objeto + " se encuentre enabled en " + segundos + " segundos.");
        int milisegundos = segundos * 1000;
        boolean res = false;

        for (int i = 0; i < 9; ++i) {
            if (MetodosGenericos.isEnabled(objeto)) {
                System.out.println("El objeto:" + objeto + " se encuentra enabled.");
                res = true;
                break;
            }

            if (i == 9) {
                System.out.println("El objeto:" + objeto + " despuÃ©s de " + segundos + " segundos no se encuentra enabled.");
                res = false;
            } else {
                try {
                    Thread.sleep((long) milisegundos);
                } catch (InterruptedException var6) {
                    Assert.fail("El Sleep del metodo validarEnable fallÃ³, el motivo:" + var6.getMessage());
                }
            }
        }
        return res;
    }

    public static void waitVisibility(WebDriver driver, WebElement element, int time) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time);
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (Exception e) {
            System.out.println(element + "not visible after " + time);
        }
    }

    public static void waitClickable(WebDriver driver, WebElement element, int time) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (Exception e) {
            System.out.println(element + "not clickable after " + time);
        }
    }

    public static void waitSelected(WebDriver driver, WebElement element, int time) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time);
            wait.until(ExpectedConditions.elementToBeSelected(element));
        } catch (Exception e) {
            System.out.println(element + "not selected after " + time);
        }
    }

    public static void waitAlert(WebDriver driver, int time) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time);
            wait.until(ExpectedConditions.alertIsPresent());
        } catch (Exception e) {
            System.out.println("Alert not showed after " + time);
        }
    }

    public static void waitInvisibility(WebDriver driver, WebElement element, int time) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time);
            wait.until(ExpectedConditions.invisibilityOf(element));
        } catch (Exception e) {
            System.out.println(element + "not hidden after " + time);
        }
    }

    public static void waitFor(int segundos) {
        try {
            Thread.sleep(segundos * 1000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    public static String devuelveNumeroTelefono() {
        String SALTCHARS = "0123456789";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    public static String devuelveTextoMinusculas() {
        String SALTCHARS = "abcdefghijklmnopqrstuwxyz";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    public static String devuelveNumeroDosDigitos() {
        String salchart = "0123456789";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 2) { // length of the random string.
            int index = (int) (rnd.nextFloat() * salchart.length());
            salt.append(salchart.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    public static String devolverSubSiguienteDia() {
        Calendar c = Calendar.getInstance();
        Date newDate = c.getTime();
        Calendar c1 = Calendar.getInstance();
        c1.setTime(newDate);
        c1.add(Calendar.DATE, 2);
        int w1 = c1.get(Calendar.DAY_OF_WEEK);
        if (w1 == Calendar.SUNDAY) {
            c1.add(Calendar.DAY_OF_WEEK, 1);
        }else if(w1 == Calendar.SATURDAY){
            c1.add(Calendar.DAY_OF_WEEK, 2);
        }
        newDate = c1.getTime();
        Format formatter = new SimpleDateFormat("dd/MM/yyyy");
        String s = formatter.format(newDate);
        System.out.println(s);
        return s;
    }

    public static String devolverDiaSiguiente() {
        Calendar c = Calendar.getInstance();
        Date newDate = c.getTime();
        Calendar c1 = Calendar.getInstance();
        c1.setTime(newDate);
        c1.add(Calendar.DATE, 1);
        int w1 = c1.get(Calendar.DAY_OF_WEEK);
        if (w1 == Calendar.SUNDAY) {
            c1.add(Calendar.DAY_OF_WEEK, 1);
        }else if(w1 == Calendar.SATURDAY){
            c1.add(Calendar.DAY_OF_WEEK, 2);
        }
        newDate = c1.getTime();
        Format formatter = new SimpleDateFormat("dd/MM/yyyy");
        String s = formatter.format(newDate);
        System.out.println(s);
        return s;
    }

    public static void teclearPinMonoapoderado() throws AWTException {
        Robot robot = new Robot();
        MetodosGenericos.waitFor(3);
        robot.keyPress(KeyEvent.VK_1);
        robot.keyRelease(KeyEvent.VK_1);
        robot.keyPress(KeyEvent.VK_6);
        robot.keyRelease(KeyEvent.VK_6);
        robot.keyPress(KeyEvent.VK_8);
        robot.keyRelease(KeyEvent.VK_8);
        robot.keyPress(KeyEvent.VK_3);
        robot.keyRelease(KeyEvent.VK_3);
        robot.keyPress(KeyEvent.VK_9);
        robot.keyRelease(KeyEvent.VK_9);
        robot.keyPress(KeyEvent.VK_2);
        robot.keyRelease(KeyEvent.VK_2);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        waitFor(10);
    }

    public static String devolver15DiasDespues(){
        Calendar c = Calendar.getInstance();
        Date newDate = c.getTime();
        Calendar c1 = Calendar.getInstance();
        c1.setTime(newDate);
        c1.add(Calendar.DATE, 15);
        int w1 = c1.get(Calendar.DAY_OF_WEEK);
        if (w1 == Calendar.SUNDAY) {
            c1.add(Calendar.DAY_OF_WEEK, 1);
        }else if(w1 == Calendar.SATURDAY){
            c1.add(Calendar.DAY_OF_WEEK, 2);
        }
        newDate = c1.getTime();
        Format formatter = new SimpleDateFormat("dd/MM/yyyy");
        String s = formatter.format(newDate);
        System.out.println(s);
        return s;
    }

    public static void teclearPinApoderadoUno()throws AWTException {
        Robot robot = new Robot();
        MetodosGenericos.waitFor(3);
        robot.keyPress(KeyEvent.VK_1);
        robot.keyRelease(KeyEvent.VK_1);
        robot.keyPress(KeyEvent.VK_2);
        robot.keyRelease(KeyEvent.VK_2);
        robot.keyPress(KeyEvent.VK_2);
        robot.keyRelease(KeyEvent.VK_2);
        robot.keyPress(KeyEvent.VK_3);
        robot.keyRelease(KeyEvent.VK_3);
        robot.keyPress(KeyEvent.VK_2);
        robot.keyRelease(KeyEvent.VK_2);
        robot.keyPress(KeyEvent.VK_0);
        robot.keyRelease(KeyEvent.VK_0);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        waitFor(10);
    }


    public static void aprobarFirmaSikuli() {
        Screen screen = new Screen();
        File file = new File("src/main/java/recursos/");
        Pattern btnAprobar = new Pattern(file + "/btnAprobarFirma.PNG");
        try {
            if (screen.wait(btnAprobar.exact()) != null) {
                screen.click(btnAprobar);
                screen.mouseMove(0,20);
            }
        } catch (FindFailed e) {
            System.out.println("No se encuentra boton aprobar firma");
            e.getStackTrace();
        }
    }

    public static void ingresarPinMonoapoderadoFEA() {
        Screen screen = new Screen();
        File file = new File("src/main/java/recursos/");
        Pattern pinInput = new Pattern(file + "/pinInput.PNG");
        Pattern verificarBtn = new Pattern(file + "/verificarBtn.PNG");
        Pattern tituloVerificarPin = new Pattern(file + "/introduzcaPinUsuario.PNG");
        try {
            if (screen.wait(tituloVerificarPin.exact()) != null) {
                screen.type(pinInput, "168392");
                screen.click(verificarBtn);
            }
        } catch (FindFailed e) {
            System.out.println("No se pide pin de verificacion");
            e.getStackTrace();
        }
    }

    public static void aprobarFirmaSikuli2() {
        Screen screen = new Screen();
        File file = new File("src/main/java/recursos/");
        Pattern btnAprobar2 = new Pattern(file + "/btnAprobarFirma2.PNG");
        try {
            if (screen.wait(btnAprobar2.exact()) != null) {
                screen.click(btnAprobar2);
                screen.mouseMove(0,20);
            }
        } catch (FindFailed e) {
            System.out.println("No se encuentra boton aprobar firma");
            e.getStackTrace();
        }
    }
}
