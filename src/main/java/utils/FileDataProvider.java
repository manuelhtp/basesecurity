package utils;

import java.util.Iterator;
import java.util.Collections;
import java.util.Random;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FileDataProvider {
    private static final Logger logger;

    protected static List<String> readLinesFromFile(final String path, final boolean randomly, final int numberOfLines) {
        List<String> lines = new ArrayList<String>();
        try {
            lines = Files.readAllLines(Paths.get(path, new String[0]));
        }
        catch (IOException e) {
           System.out.println(-1);// FileDataProvider.logger.error("There was an error reading the file {}", (Object)path);
        }
        if (randomly) {
            Collections.shuffle(lines, new Random(System.nanoTime()));
        }
        if (numberOfLines > 0) {
            return (lines.size() < numberOfLines) ? lines : lines.subList(0, numberOfLines);
        }
        return lines;
    }

    public static String asString(final String path) {
        String data = "";
        try {
            data = new String(Files.readAllBytes(Paths.get(path, new String[0])));
        }
        catch (IOException e) {
         FileDataProvider.logger.error("There was an error reading the file {}", (Object)path);
        }
        return data;
    }

    public static Iterator<Object[]> asRawLines(final String path, final boolean randomly, final int numberOfLines) {
        final List<Object[]> data = new ArrayList<Object[]>();
        readLinesFromFile(path, randomly, numberOfLines).forEach(t -> data.add(new Object[] { t }));
        return data.iterator();
    }

    public static Object[][] asSeparatedValues(final String path, final String separator, final boolean randomly, final int numberOfLines) {
        return asSeparatedValues(path, separator, randomly, numberOfLines, 0);
    }

    public static Object[][] asSeparatedValues(final String path, final String separator, final boolean randomly, final int numberOfLines, final int linesToExclude) {
        final Iterator<Object[]> iterator = asRawLines(path, randomly, numberOfLines);
        final List<String> lines = new ArrayList<String>();
        for (int i = 0; i < linesToExclude; ++i) {
            iterator.next();
        }
        while (iterator.hasNext()) {
            final String line = (String)iterator.next()[0];
            if (line.startsWith("#")) {
                continue;
            }
            lines.add(line);
        }
        Object[][] data;
        if (lines.isEmpty()) {
            data = new Object[0][];
        }
        else {
            data = new Object[lines.size()][lines.get(0).split(separator).length];
            for (int j = 0; j < lines.size(); ++j) {
                final Object[] line_aux = lines.get(j).split(separator);
                for (int k = 0; k < line_aux.length; ++k) {
                    data[j][k] = line_aux[k];
                }
            }
        }
        return data;
    }

   static {
        logger = LoggerFactory.getLogger((Class) FileDataProvider.class);
    }
}