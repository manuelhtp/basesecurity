package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class CaptureScreenShot {

    public static String captureScreen(WebDriver driver, String screenName) throws IOException {
        TakesScreenshot screen = (TakesScreenshot) driver;
        File src = screen.getScreenshotAs(OutputType.FILE);
        String dest = "./report/" + screenName + ".png";
        File target = new File(dest);
        FileUtils.copyFile(src, target);
        return dest;
    }
}